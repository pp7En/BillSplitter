# BillSplitter

This Java application is based on [PHP-BillSplitter](https://codeberg.org/pp7En/PHP-BillSplitter) but written completely in Java. It is much easier to enhance the functionality than the PHP version. I use widely spread tools like Vaadin and Spring Boot. In the JUnit tests I use Selenium and Testcontainers. However, I still need to improve that part a bit so that no local Firefox is necessary.

I wrote this application to track of household expenses and who paid how much and when. Of course you can also use BillSplitter for a vacation together with friends.

* Invoices are assigned to groups and in these groups can be full members (who can also log in) or guests (these cannot log in).
* It is also possible to generate a public link. Everyone with this link can be automatically logged in. In the background a full account is created for this reason. The credentials are coded in the link as a base64 string and sent along as an anchor. This has the advantage that the credentials don't leave the browser and do not appear in any logs.
* New full members can be invited via an invitation code of the group. The group admin can confirm or decline this invitation.
* Screenshots are in [this](https://codeberg.org/pp7En/BillSplitter/screenshots/) folder.

## Prepare

* Rename `docker/.env.example` to `docker/.env`. This file contains the variables which overwrite the values in application.yml. Please modify to your environment.

```
mvn clean package -Pproduction -DskipTests
```

At the first start an administrator account will be created. You can find the credentials in the logs. Please change the password and the email address.

```
************************************************
FIRST RUN
E-Mail:   u6ygnf43@example.com
Password: w8jgeieuv0f7wrwzffob511kxlpjv5
************************************************
```

## System Requirements

* PostgreSQL database
* [Java JDK 21 (e.g. Amazon Corretto 21)](https://github.com/corretto/corretto-21/releases)
* [Maven](https://maven.apache.org/)
* [Docker](https://docs.docker.com/get-docker/)

## Contributing Code

If you have any suggestions for improving this software, please send them as a pull request.

## License

[GPLv3](https://codeberg.org/pp7En/BillSplitter/src/branch/main/LICENSE)

## Donations

[<img src="https://codeberg.org/pp7En/BillSplitter/raw/branch/main/buymeacoffee.png" alt="Buy Me A Coffee" width="214"/>](https://www.buymeacoffee.com/warpcoredev)
