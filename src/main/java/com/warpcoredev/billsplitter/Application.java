package com.warpcoredev.billsplitter;

import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.server.AppShellSettings;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.theme.Theme;
import com.warpcoredev.billsplitter.utils.RedirectionFilter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * The entry point of the Spring Boot application.
 */
@SpringBootApplication(scanBasePackages={"com.warpcoredev.billsplitter"})
@Theme(value = "billsplitter")
@EnableScheduling
@EnableAsync
@PWA (name = "BillSplitter", shortName = "BillSplitter")
public class Application implements AppShellConfigurator {

    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public FilterRegistrationBean<RedirectionFilter> loggingFilter() {
        final FilterRegistrationBean<RedirectionFilter> registrationBean = new FilterRegistrationBean<>();

        registrationBean.setFilter(new RedirectionFilter());
        registrationBean.addUrlPatterns("/");

        return registrationBean;
    }

    @Override
    public void configurePage(final AppShellSettings settings) {
        settings.addFavIcon("icon", "favicon.png", "192x192");
        settings.addLink("shortcut icon", "favicon.ico");
    }
}
