package com.warpcoredev.billsplitter;


import com.warpcoredev.billsplitter.data.entity.LoginAttempt;
import com.warpcoredev.billsplitter.data.service.LoginAttemptService;
import com.warpcoredev.billsplitter.data.service.UserService;

import java.time.Duration;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import static com.warpcoredev.billsplitter.utils.SharedConsts.LIMIT_TIME_RANGE_CLEAN_UP;


@Component
@Slf4j
@AllArgsConstructor
public class ScheduledTasks {

    private final LoginAttemptService loginAttemptService;
    private final UserService userService;

    @Scheduled (cron = "0 0 */1 * * *")
    public void cleanupBlockingTable() {
        log.info("Cleanup blocking table");

        this.loginAttemptService.getAllLoginAttempts().forEach(entry -> {
            if (this.isInTimeRange(entry)) {
                this.reduceOrDeleteLoginAttempt(entry);
            }
        });
    }

    @Scheduled (cron = "0 0 */1 * * *")
    public void cleanupUserTable() {
        log.info("Cleanup user table");

        this.userService.getUsersWithoutFirstLoginIn24Hours()
                .forEach(this.userService::delete);
    }

    private boolean isInTimeRange(final LoginAttempt loginAttempt) {
        final LocalDateTime now = LocalDateTime.now();
        final Duration duration = Duration.between(now, loginAttempt.getDatetime()).abs();

        // 6 hours
        return duration.toMinutes() > LIMIT_TIME_RANGE_CLEAN_UP;
    }

    private void reduceOrDeleteLoginAttempt(final LoginAttempt entry) {
        this.loginAttemptService.deleteOrUpdate(entry);
    }
}
