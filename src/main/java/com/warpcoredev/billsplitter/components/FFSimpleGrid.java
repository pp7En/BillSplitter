package com.warpcoredev.billsplitter.components;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.warpcoredev.billsplitter.utils.SharedStyles;

/**
* FFTable is just a workaround for missing grid compatibility on Firefox for Android.
*/
public class FFSimpleGrid extends HorizontalLayout {

    public static final String MARGIN_75_EM = "0.75em";
    public static final int NUMBER_OF_COLUMNS = 3;
    private final Div divLeft = new Div();
    private final Div divMiddle = new Div();
    private final Div divRight = new Div();

    public FFSimpleGrid() {
        this.divLeft.setWidthFull();
        this.divMiddle.setWidthFull();
        this.setWidthFull();
    }
    public FFSimpleGrid(final String column1, final String column2) {
        this();
        this.divLeft.add(new Span(column1));

        this.divRight.add(new Span(column2));
        this.divRight.setWidthFull();
        this.divRight.addClassNames(SharedStyles.TEXT_ALIGN_RIGHT);

        this.add(this.divLeft, this.divRight);
    }

    public FFSimpleGrid(final String column1, final String column2, final Button delete) {
        this();
        final Paragraph p1 = new Paragraph(column1);
        p1.getStyle().setMarginTop(MARGIN_75_EM);
        p1.getStyle().setMarginBottom(MARGIN_75_EM);

        final Paragraph p2 = new Paragraph(column2);
        p2.getStyle().setMarginTop(MARGIN_75_EM);
        p2.getStyle().setMarginBottom(MARGIN_75_EM);

        this.divLeft.add(p1);

        this.divMiddle.add(p2);
        this.divMiddle.addClassNames(SharedStyles.TEXT_ALIGN_RIGHT);

        this.divRight.add(delete);

        this.add(this.divLeft, this.divMiddle, this.divRight);
    }

    public void enableHeaderPaddingForButton() {
        this.divRight.getStyle().set("padding-right", "3.6em");
    }

    public void enableHeaderStyleByColumns(final int columns) {
        this.getStyle().set("gap", "0");

        this.addHeaderStyle(this.divLeft);
        this.addHeaderStyle(this.divRight);

        if (columns == NUMBER_OF_COLUMNS) {
            this.addHeaderStyle(this.divMiddle);
        }
    }

    private void addHeaderStyle(final Div div) {
        div.getStyle().set("font-weight", "bold");
        div.getStyle().set("border-bottom", "1px solid lightgray");
    }
}
