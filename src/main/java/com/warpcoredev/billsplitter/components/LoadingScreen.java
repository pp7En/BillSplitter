package com.warpcoredev.billsplitter.components;

import com.vaadin.flow.component.ClientCallable;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;

import jakarta.annotation.PostConstruct;

@CssImport("./themes/billsplitter/lds-dual-ring.css")
public class LoadingScreen extends Div {

    private static final String JS_SCRIPT = """
        var img = new Image();
        img.onload = function() {
            console.log('Background image loaded!');
            $0.$server.backgroundImageLoaded();
        };
        img.onerror = function() {
            console.log('Failed to load background image.');
            $0.$server.backgroundImageLoaded();
        };
        img.src = 'VAADIN/themes/billsplitter/images/background.jpg';
        """;

    @PostConstruct
    private void initUI() {
        final Div div = new Div();
        div.addClassName("lds-dual-ring");

        this.addClassNames("loading");
        this.setId("loading-screen");
        this.add(div);

        UI.getCurrent().getPage().executeJs(JS_SCRIPT, this.getElement());
    }

    @ClientCallable
    public void backgroundImageLoaded() {
        this.setVisible(false);
    }
}
