package com.warpcoredev.billsplitter.components.appnav;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;

import lombok.Getter;

@Getter
public class RefreshAppMenuEvent extends ComponentEvent<Component> {
    private final Long groupId;

    /**
     * Creates a new event using the given source and indicator whether the
     * event originated from the client side or the server side.
     *
     * @param source     the source component
     * @param fromClient <code>true</code> if the event originated from the client
     *                   side, <code>false</code> otherwise
     */
    public RefreshAppMenuEvent(final Component source, final boolean fromClient, final Long groupId) {
        super(source, fromClient);
        this.groupId = groupId;
    }
}
