package com.warpcoredev.billsplitter.components.chartjs;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.shared.ui.LoadMode;
import com.warpcoredev.billsplitter.data.dto.StatisticDto;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import software.xdev.chartjs.model.charts.AbstractChart;
import software.xdev.chartjs.model.charts.BarChart;
import software.xdev.chartjs.model.data.BarData;
import software.xdev.chartjs.model.dataset.BarDataset;
import software.xdev.chartjs.model.options.BarOptions;
import software.xdev.chartjs.model.options.LegendOptions;

public class ChartJS extends Canvas {

    private static final String BASE = "/lib/chartjs/";
    private static final String PATH_CHARTJS = BASE + "chart.min.js";
    private static final String BAR_CHART_COLOR = "#4caf50";

    private final String id = UUID.randomUUID().toString();

    private transient AbstractChart<?, ?, ?> chart;

    public ChartJS() {
        this.setId(this.id);
        UI.getCurrent().getPage().addJavaScript(PATH_CHARTJS, LoadMode.EAGER);
    }

    public void setData(final AbstractChart<?, ?, ?> chartJs) {
        this.chart = chartJs;
    }

    public BarChart generateStatisticChart(final List<StatisticDto> list) {
        final List<Number> data = new ArrayList<>();
        final List<String> labels = new ArrayList<>();

        for (final StatisticDto dto : list) {
            data.add(dto.getSum());
            labels.add(dto.getDateMonth());
        }

        final BarDataset dataset = new BarDataset();
        dataset.setData(data);
        dataset.setBackgroundColor(BAR_CHART_COLOR);

        final BarData barData = new BarData();
        barData.setLabels(labels);
        barData.setDatasets(List.of(dataset));

        final LegendOptions legendOptions = new LegendOptions();
        legendOptions.setDisplay(false);

        final BarOptions options = new BarOptions();
        options.setResponsive(true);
        options.getPlugins().setLegend(legendOptions);
        options.setAnimation(false);

        final BarChart barChart = new BarChart();
        barChart.setData(barData);
        barChart.setOptions(options);

        return barChart;
    }

    @Override
    protected void onAttach(final AttachEvent attachEvent) {
        super.onAttach(attachEvent);
        final String jsToExec = """
            const ctx = document.getElementById('%s');
            const data = %s
            new Chart(ctx, data);
            """.formatted(this.id, this.chart.toJson());

        UI.getCurrent().getPage().executeJs(jsToExec);
    }
}
