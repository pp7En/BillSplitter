package com.warpcoredev.billsplitter.components.groupcomponents;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.BigDecimalField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.theme.lumo.LumoUtility;
import com.warpcoredev.billsplitter.data.PaymentStatus;
import com.warpcoredev.billsplitter.data.entity.Bill;
import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.entity.Payment;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.utils.HelperClass;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedStyles;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import static com.warpcoredev.billsplitter.utils.I18NKeys.BILLFORM_ADJUSTMENT_REQUIRED;
import static com.warpcoredev.billsplitter.utils.SharedConsts.BILL_CALCULATOR_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.BILL_DESCRIPTION;
import static com.warpcoredev.billsplitter.utils.SharedConsts.BILL_PAYMENT_GRID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.BILL_PAYMENT_GRID_CHECKBOX;
import static com.warpcoredev.billsplitter.utils.SharedConsts.BILL_SUBMIT_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.BILL_TOTAL_PAYABLE;
import static com.warpcoredev.billsplitter.utils.SharedConsts.BILL_USER_WHO_PAID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.PAYMENT_GRID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class BillForm extends VerticalLayout {

    private static final int SCALE = 10;

    private final BigDecimalField billTotalPayable = new BigDecimalField();
    private final DatePicker billDate = new DatePicker();
    private final ComboBox<String> billDescription = new ComboBox<>();
    private final Button saveButton = new Button();
    private final Select<User> userWhoPaid = new Select<>();
    private final Div paymentDiv = new Div();
    private final Calculator calculator = new Calculator();
    private final HorizontalLayout hlTotalPayableAndCalculatorBtnArea = new HorizontalLayout();
    private final Paragraph adjustmentRequired = new Paragraph();

    private final DecimalFormat decimalFormat = HelperClass.decimalFormat(
            this.getTranslation(I18NKeys.CURRENCY_DECIMAL_SEPARATOR),
            this.getTranslation(I18NKeys.CURRENCY_GROUPING_SEPARATOR));

    @Setter
    private User user;
    @Setter
    private Group group;

    private List<User> groupMembers;

    public Div initUI() {
        final Div div = new Div();

        this.billDate.setLabel(this.getTranslation(I18NKeys.BILLFORM_DATE));
        this.billDate.setI18n(this.getI18nDatePickerFormat());
        this.billDate.addValueChangeListener(ev ->
                this.refreshGridContent());

        this.billDescription.setLabel(this.getTranslation(I18NKeys.BILLFORM_DESCRIPTION));
        this.billDescription.addValueChangeListener(ev ->
                this.refreshGridContent());
        this.billDescription.addCustomValueSetListener(ev -> {
            this.refreshGridContent();
            this.billDescription.setValue(ev.getDetail());
        });
        this.billDescription.setId(BILL_DESCRIPTION);
        this.billDescription.setAllowCustomValue(true);

        this.billTotalPayable.setLabel(this.getTranslation(I18NKeys.BILLFORM_PAID));
        this.billTotalPayable.setValueChangeMode(ValueChangeMode.EAGER);
        this.billTotalPayable.addValueChangeListener(ev ->
                this.refreshGridContent());

        this.billTotalPayable.addBlurListener(ev -> this.formatBillTotalPayableField());

        final Div currency = new Div();
        currency.setText(this.getTranslation(I18NKeys.CURRENCY));
        this.billTotalPayable.setSuffixComponent(currency);
        this.billTotalPayable.setWidthFull();
        this.billTotalPayable.setId(BILL_TOTAL_PAYABLE);

        final Image img = new Image("images/calculator.png", "");
        img.addClassName(SharedStyles.BUTTON_CALC);

        final Button btnCalc = new Button(img);
        btnCalc.setId(BILL_CALCULATOR_BTN);
        btnCalc.addClassNames(SharedStyles.BUTTON_GREEN, SharedStyles.BUTTON_PARENT_CLASS);
        btnCalc.addClickListener(e -> this.calculator.open());
        this.calculator.getApplyBtn().addClickListener(e -> this.applyCalcResult());
        this.hlTotalPayableAndCalculatorBtnArea.setWidthFull();
        this.hlTotalPayableAndCalculatorBtnArea.setAlignItems(Alignment.BASELINE);
        this.hlTotalPayableAndCalculatorBtnArea.add(this.billTotalPayable, btnCalc);

        final H4 headerWhoPaid = new H4(this.getTranslation(I18NKeys.BILLFORM_WHO_PAID));

        this.userWhoPaid.setItemLabelGenerator(User::getName);
        this.userWhoPaid.setId(BILL_USER_WHO_PAID);

        final H4 headerWhoHasToPay = new H4(this.getTranslation(I18NKeys.BILLFORM_WHOHASTOPAY));

        this.adjustmentRequired.addClassName(LumoUtility.FontSize.XSMALL);
        this.adjustmentRequired.setVisible(false);

        this.saveButton.setText(this.getTranslation(I18NKeys.BILLFORM_BUTTON_SAVE));
        this.saveButton.setEnabled(false);
        this.saveButton.addClassNames(SharedStyles.BUTTON_GREEN, SharedStyles.BUTTON_PARENT_CLASS);
        this.saveButton.setId(BILL_SUBMIT_BTN);

        this.paymentDiv.setId(BILL_PAYMENT_GRID);

        div.add(
                this.billDate,
                this.billDescription,
                this.hlTotalPayableAndCalculatorBtnArea,
                headerWhoPaid,
                this.userWhoPaid,
                headerWhoHasToPay,
                this.paymentDiv,
                this.adjustmentRequired,
                this.saveButton
        );

        div.getChildren().forEach(comp -> comp.addClassName(SharedStyles.FULL_WIDTH));

        return div;
    }

    private void formatBillTotalPayableField() {
        final BigDecimal value = this.billTotalPayable.getValue();
        if (value != null) {
            this.formatBillTotalPayableField(value);
        }
    }

    private void formatBillTotalPayableField(final BigDecimal value) {
        final String formattedValue = this.decimalFormat.format(value);
        this.billTotalPayable.setValue(new BigDecimal(formattedValue.replace(",", ".")));
    }

    private void applyCalcResult() {
        this.calculator.close();
        BigDecimal result;
        try {
            result = this.calculator.getResult();
        } catch (final NumberFormatException ex) {
            result = BigDecimal.valueOf(0.00);
            this.billTotalPayable.setInvalid(true);
            this.billTotalPayable.setErrorMessage(this.getTranslation(I18NKeys.BILLFORM_CALCULATOR_ERROR));
        }
        this.formatBillTotalPayableField(result);
    }

    private void refreshGridContent() {
        final double totalPayable = (this.billTotalPayable.getValue() == null)
                ? 0.0
                : this.billTotalPayable.getValue().doubleValue();
        final BigDecimal value = BigDecimal.valueOf(totalPayable);

        final String description = this.billDescription.getValue();
        final int lengthDescription = (description == null) ? 0 : description.length();

        final boolean dateIsInvalid = this.billDate.isInvalid()
                || this.billDate.isEmpty()
                || this.billDate.getValue() == null;

        final long selectedCount = this.paymentDiv.getChildren()
                .filter(PaymentGrid.class::isInstance)
                .map(e -> (PaymentGrid) e)
                .filter(pg -> pg.getCheckbox().getValue())
                .count();

        final int validCount = (int) Math.max(selectedCount, 1);
        final BigDecimal haveToPay = value.divide(BigDecimal.valueOf(validCount), 2, RoundingMode.DOWN);

        this.adjustmentRequired.setVisible(this.isAdjustmentNeeded(value, (int) selectedCount));

        this.paymentDiv.getChildren()
                .filter(PaymentGrid.class::isInstance)
                .map(e -> (PaymentGrid) e)
                .forEach(pg -> {
                    final double amount = pg.getCheckbox().getValue() ? haveToPay.doubleValue() : 0.0;
                    pg.setAmount(amount);
                });

        this.validationForSaveButton(totalPayable, (int) selectedCount, lengthDescription, dateIsInvalid);
    }

    private void validationForSaveButton(final double amount, final int countCheckbox, final int lengthDesc,
                                         final boolean dateIsInvalid) {
        final boolean invalid = amount == 0 || countCheckbox == 0 || lengthDesc == 0 || dateIsInvalid;

        this.saveButton.setEnabled(!invalid);
    }

    public void setValues(final List<User> listGroupMembers, final List<String> listOfMostCommonDescriptions) {
        this.billDate.setValue(LocalDate.now());
        this.billDescription.setItems(listOfMostCommonDescriptions);
        this.billTotalPayable.clear();

        this.userWhoPaid.setItems(listGroupMembers);
        this.userWhoPaid.setValue(this.user);

        this.paymentDiv.removeAll();

        listGroupMembers.forEach(member -> {
            final PaymentGrid paymentGrid = new PaymentGrid(
                    Payment.builder()
                            .user(member)
                            .group(this.group)
                            .build());
            paymentGrid.setClassName(PAYMENT_GRID);
            paymentGrid.setValue(true, member.getName(), 0.0);
            paymentGrid.getCheckbox().addClickListener(ev -> this.refreshGridContent());
            paymentGrid.getCheckbox().addClassName(BILL_PAYMENT_GRID_CHECKBOX);
            this.paymentDiv.add(paymentGrid);
        });
    }

    public Set<Payment> getListOfPayments() {
        final LocalDate currentBillDate = this.billDate.getValue();
        final User currentCollector = this.userWhoPaid.getValue();
        final Group currentGroup = this.group;

        final Bill bill = Bill.builder()
                .group(currentGroup)
                .totalPayable(this.billTotalPayable.getValue())
                .description(this.billDescription.getValue())
                .collector(currentCollector)
                .paidDate(currentBillDate)
                .build();

        final Set<Payment> listPayments = new HashSet<>();

        final List<PaymentGrid> paymentGrids = this.paymentDiv.getChildren()
                .filter(PaymentGrid.class::isInstance)
                .map(PaymentGrid.class::cast)
                .filter(pg -> pg.getPayment().getHaveToPay().compareTo(BigDecimal.ZERO) != 0)
                .toList();

        if (paymentGrids.isEmpty()) {
            return listPayments;
        }

        final long totalPaymentCount = paymentGrids.size();
        final BigDecimal firstPaymentAmount = paymentGrids.getFirst().getPayment().getHaveToPay();

        paymentGrids.forEach(pg -> {
            final Payment payment = pg.getPayment();
            payment.setPaidDay(currentBillDate);
            payment.setBill(bill);
            payment.setGroup(currentGroup);

            final BigDecimal paidAmount;
            if (payment.getUser().equals(currentCollector)) {
                paidAmount = firstPaymentAmount.multiply(BigDecimal.valueOf(totalPaymentCount));
                payment.setStatus(PaymentStatus.CONFIRMED);
            } else {
                paidAmount = BigDecimal.ZERO;
                payment.setStatus(PaymentStatus.NO_PAYMENT);
            }

            payment.setPaid(paidAmount);
            payment.setBalance(paidAmount.subtract(pg.getPayment().getHaveToPay()));

            listPayments.add(payment);
        });

        return listPayments;
    }

    public Button getSaveBtn() {
        return this.saveButton;
    }

    public void clearFields() {
        this.billDate.setValue(LocalDate.now());
        this.billDescription.clear();
        this.billTotalPayable.clear();
        this.userWhoPaid.setValue(this.user);
        this.paymentDiv.getChildren().forEach(payment -> {
            if (payment instanceof final PaymentGrid pg) {
                pg.getCheckbox().setValue(true);
            }
        });
        this.saveButton.setEnabled(false);
    }

    private DatePicker.DatePickerI18n getI18nDatePickerFormat() {
        final DatePicker.DatePickerI18n i18n = new DatePicker.DatePickerI18n();
        i18n.setMonthNames(List.of(
                this.getTranslation(I18NKeys.DATEPICKER_MONTH_JANUARY),
                this.getTranslation(I18NKeys.DATEPICKER_MONTH_FEBRUARY),
                this.getTranslation(I18NKeys.DATEPICKER_MONTH_MARCH),
                this.getTranslation(I18NKeys.DATEPICKER_MONTH_APRIL),
                this.getTranslation(I18NKeys.DATEPICKER_MONTH_MAY),
                this.getTranslation(I18NKeys.DATEPICKER_MONTH_JUNE),
                this.getTranslation(I18NKeys.DATEPICKER_MONTH_JULY),
                this.getTranslation(I18NKeys.DATEPICKER_MONTH_AUGUST),
                this.getTranslation(I18NKeys.DATEPICKER_MONTH_SEPTEMBER),
                this.getTranslation(I18NKeys.DATEPICKER_MONTH_OCTOBER),
                this.getTranslation(I18NKeys.DATEPICKER_MONTH_NOVEMBER),
                this.getTranslation(I18NKeys.DATEPICKER_MONTH_DECEMBER)
        ));

        i18n.setWeekdays(List.of(
                this.getTranslation(I18NKeys.DATEPICKER_WEEKDAY_SUNDAY),
                this.getTranslation(I18NKeys.DATEPICKER_WEEKDAY_MONDAY),
                this.getTranslation(I18NKeys.DATEPICKER_WEEKDAY_TUESDAY),
                this.getTranslation(I18NKeys.DATEPICKER_WEEKDAY_WEDNESDAY),
                this.getTranslation(I18NKeys.DATEPICKER_WEEKDAY_THURSDAY),
                this.getTranslation(I18NKeys.DATEPICKER_WEEKDAY_FRIDAY),
                this.getTranslation(I18NKeys.DATEPICKER_WEEKDAY_SATURDAY)
        ));

        i18n.setWeekdaysShort(List.of(
                this.getTranslation(I18NKeys.DATEPICKER_WEEKDAY_SHORT_SUNDAY),
                this.getTranslation(I18NKeys.DATEPICKER_WEEKDAY_SHORT_MONDAY),
                this.getTranslation(I18NKeys.DATEPICKER_WEEKDAY_SHORT_TUESDAY),
                this.getTranslation(I18NKeys.DATEPICKER_WEEKDAY_SHORT_WEDNESDAY),
                this.getTranslation(I18NKeys.DATEPICKER_WEEKDAY_SHORT_THURSDAY),
                this.getTranslation(I18NKeys.DATEPICKER_WEEKDAY_SHORT_FRIDAY),
                this.getTranslation(I18NKeys.DATEPICKER_WEEKDAY_SHORT_SATURDAY)
        ));

        i18n.setToday(this.getTranslation(I18NKeys.DATEPICKER_TODAY));
        i18n.setCancel(this.getTranslation(I18NKeys.DATEPICKER_CANCEL));
        i18n.setFirstDayOfWeek(Integer.parseInt(this.getTranslation(I18NKeys.DATEPICKER_FIRST_DAY_OF_WEEK)));
        i18n.setDateFormat(this.getTranslation(I18NKeys.DATE_FORMAT_LONG));

        return i18n;
    }

    private boolean isAdjustmentNeeded(final BigDecimal totalAmount, final int numberOfPeople) {
        if (numberOfPeople == 0 || totalAmount.compareTo(BigDecimal.ZERO) == 0.0) {
            return false;
        }
        this.adjustmentRequired.setText(
                this.getTranslation(BILLFORM_ADJUSTMENT_REQUIRED)
                        .replace("%NUMBER_OF_PEOPLE%", String.valueOf(numberOfPeople))
        );

        // Convert the total amount and number of people to BigDecimal for precise calculations
        final BigDecimal paymentCount = BigDecimal.valueOf(numberOfPeople);

        // Calculate the amount per person with rounding down to avoid precision issues
        BigDecimal payablePerPerson = totalAmount.divide(paymentCount, SCALE, RoundingMode.DOWN);

        // Strip trailing zeros to accurately count the number of decimal places
        payablePerPerson = payablePerPerson.stripTrailingZeros();

        // Check the number of decimal places in the amount per person
        final int decimalPlaces = payablePerPerson.scale();

        // Return false if the amount per person has exactly two decimal places
        // Return true if the amount per person has more than two decimal places
        return decimalPlaces > 2;
    }
}
