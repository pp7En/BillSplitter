package com.warpcoredev.billsplitter.components.groupcomponents;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedStyles;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

import static com.warpcoredev.billsplitter.utils.SharedConsts.BILL_CALCULATOR_RESULT_TEXTFIELD;
import static com.warpcoredev.billsplitter.utils.SharedStyles.CALC_RESULT_TEXTFIELD;


/**
 * Inspired from <a href="https://github.com/elohmrow/simple-vaadin-calculator">
 *     github.com/elohmrow/simple-vaadin-calculator</a>.
 */
@SuppressWarnings("checkstyle:all")
public class Calculator extends Dialog {

    private static final String BTN_HEIGHT = "40px";
    private static final String BTN_WIDTH_DOUBLE = "145px";
    private static final String BTN_WIDTH_SINGLE = "70px";

    public static final String KEY_COMMA = ",";
    public static final String KEY_POINT = ".";
    public static final String KEY_BACK = "back";
    public static final String KEY_MULTIPLY = "×";
    public static final String KEY_DIVIDE = "÷";
    public static final String KEY_EQUALS = "=";
    public static final String KEY_CLEAR = "C";
    public static final String KEY_ADD = "+";
    public static final String KEY_SUBTRACT = "−";
    public static final String PREFIX_CALCULATOR_KEY = "calculator-key-";

    private static final String ERROR = "Error";

    private final HorizontalLayout horizontalLayout = new HorizontalLayout();
    private final VerticalLayout verticalLayout = new VerticalLayout();
    private final TextField result = new TextField();
    private final Button closeBtn = new Button(this.getTranslation(I18NKeys.BUTTON_ABORT));

    @Getter
    private final Button applyBtn = new Button(this.getTranslation(I18NKeys.BUTTON_APPLY));

    private final String[][] calcButtons = {
            {KEY_CLEAR, KEY_BACK},
            {"7", "8", "9", KEY_MULTIPLY},
            {"4", "5", "6", KEY_DIVIDE},
            {"1", "2", "3", KEY_ADD},
            {"DECIMAL_SEPARATOR", "0", KEY_EQUALS, KEY_SUBTRACT}
    };

    private final String decimalSeparator = this.getTranslation(I18NKeys.CURRENCY_DECIMAL_SEPARATOR);

    private enum Function { EQUALS, CLEAR, ADD, SUBTRACT, MULTIPLY, DIVIDE }

    private String resultValue = "";

    public Calculator() {
        // Replace placeholder with language specific character
        this.calcButtons[4][0] = this.decimalSeparator;

        this.initUI();
    }

    private void initUI() {
        this.result.setId(BILL_CALCULATOR_RESULT_TEXTFIELD);
        this.result.addClassName(CALC_RESULT_TEXTFIELD);
        this.result.setHeight(50, Unit.PIXELS);
        this.result.setWidthFull();
        this.result.addThemeVariants(TextFieldVariant.LUMO_ALIGN_RIGHT);
        this.result.setValue(this.resultValue);
        this.result.setValueChangeMode(ValueChangeMode.EAGER);
        this.result.setReadOnly(true);

        final HorizontalLayout hlRow = new HorizontalLayout();
        hlRow.add(this.result);
        hlRow.setWidthFull();

        this.verticalLayout.add(hlRow);
        this.verticalLayout.addClassNames(SharedStyles.CALC_HL, SharedStyles.NO_PADDING);

        HorizontalLayout row;
        for (final String[] strArray : this.calcButtons) {
            row = new HorizontalLayout();
            row.addClassName(SharedStyles.CALC_HL);
            for (final String str : strArray) {
                final Button button;
                if (str.equals(KEY_BACK)) {
                    final Image img = new Image("images/calculator-key-e.png", "");
                    img.setWidth("2em");
                    img.getStyle().set("margin-top", "2px");

                    button = new Button(img);
                    button.addThemeVariants(ButtonVariant.LUMO_ICON);
                } else {
                    button = new Button(str);
                }
                final String string = generateButtonId(str);
                button.setId(string);
                button.addThemeVariants(ButtonVariant.LUMO_ICON);
                button.setHeight(BTN_HEIGHT);
                button.setWidth(this.getWidth(str));

                button.addClickListener(e -> this.btnPressed(str));

                if (this.isOperator(str)) {
                    button.addClassNames(
                            SharedStyles.CALC_BTN_PARENT,
                            SharedStyles.CALC_BTN_OPERATOR,
                            SharedStyles.FONT_BOLD);
                } else {
                    button.addClassNames(SharedStyles.CALC_BTN_PARENT, SharedStyles.CALC_BTN_NUMBER);
                }
                row.add(button);
            }
            this.verticalLayout.add(row);
        }
        this.add(this.verticalLayout);

        this.applyBtn.addClassNames(SharedStyles.BUTTON_PARENT_CLASS,
                SharedStyles.BUTTON_GREEN,
                SharedStyles.HALF_WIDTH);

        this.closeBtn.addClassNames(SharedStyles.BUTTON_PARENT_CLASS,
                SharedStyles.BUTTON_RED,
                SharedStyles.HALF_WIDTH);

        this.closeBtn.addClickListener(e -> this.close());

        this.horizontalLayout.addClassNames(SharedStyles.CALC_HL,
                SharedStyles.MARGIN_TOP_10);

        this.horizontalLayout.add(this.applyBtn, this.closeBtn);

        this.add(this.horizontalLayout);
    }

    public static String generateButtonId(final String str) {
        return PREFIX_CALCULATOR_KEY + switch (str) {
            case KEY_ADD -> Function.ADD.name().toLowerCase();
            case KEY_SUBTRACT -> Function.SUBTRACT.name().toLowerCase();
            case KEY_MULTIPLY -> Function.MULTIPLY.name().toLowerCase();
            case KEY_DIVIDE -> Function.DIVIDE.name().toLowerCase();
            case KEY_EQUALS -> Function.EQUALS.name().toLowerCase();
            case KEY_CLEAR -> Function.CLEAR.name().toLowerCase();
            case KEY_COMMA, KEY_POINT -> "decimal-separator";
            default -> str;
        };
    }

    private void btnPressed(final String key) {
        this.applyBtn.setEnabled(true);
        // First character must be a number
        if (this.resultValue.isEmpty() && this.pressedKeyIsNotNumber(key)) {
            return;
        }
        // Prevent all non-number keys from being pressed more than once
        if (this.resultValue.length() > 1 && this.operatorAlreadyExist(key)) {
            return;
        }

        // Reset on error
        if (this.resultValue.equals(ERROR)) {
            this.result.clear();
            this.resultValue = "";
        }

        if (Character.isDigit(key.charAt(0)) || key.equals(KEY_COMMA) || key.equals(KEY_POINT)) {
            // Handle numeric and decimal keys
            this.resultValue += key.equals(KEY_COMMA) ? KEY_POINT : key;
        } else if (key.equals(KEY_BACK) && !this.resultValue.isEmpty()) {
            // Handle backspace
            this.resultValue = this.resultValue.substring(0, this.resultValue.length() - 1);
        } else {
            // Handle function keys
            final Function functionKey = this.getFunction(key);
            if (functionKey != null) {
                this.handleFunctionKey(functionKey);
            }
        }

        this.result.setValue(this.resultValue.replace(KEY_POINT, this.decimalSeparator));
    }

    private void handleFunctionKey(final Function functionKey) {
        switch (functionKey) {
            case EQUALS -> this.handleEqualsKey();
            case CLEAR -> this.resultValue = "";
            default -> this.resultValue += this.getOperatorSymbol(functionKey);
        }
    }

    private void handleEqualsKey() {
        try {
            final BigDecimal res = this.evaluateExpression(this.resultValue);
            this.resultValue = res.toString();
        } catch (final IllegalArgumentException | ArithmeticException ex) {
            this.resultValue = ERROR;
            this.applyBtn.setEnabled(false);
        }
    }

    private boolean operatorAlreadyExist(final String key) {
        final String lastCharInResultValue = "" + this.resultValue.charAt(this.resultValue.length() - 1);
        final boolean last = this.pressedKeyIsNotNumber(lastCharInResultValue);
        final boolean current = this.pressedKeyIsNotNumber(key);
        return last && current;
    }

    private boolean pressedKeyIsNotNumber(final String value) {
        return switch (value) {
            case KEY_ADD, KEY_SUBTRACT, KEY_MULTIPLY, KEY_DIVIDE, KEY_POINT, KEY_COMMA, KEY_EQUALS -> true;
            default -> false;
        };
    }

    private BigDecimal evaluateExpression(final String expression) {
        final List<String> tokens = this.tokenize(expression);
        final List<String> postfix = this.convertToPostfix(tokens);
        return this.evaluatePostfix(postfix);
    }

    private List<String> tokenize(final String expression) {
        final List<String> tokens = new ArrayList<>();
        final StringBuilder numberBuffer = new StringBuilder();

        for (final char c : expression.toCharArray()) {
            if (Character.isDigit(c) || c == KEY_POINT.charAt(0)) {
                numberBuffer.append(c);
            } else {
                if (!numberBuffer.isEmpty()) {
                    tokens.add(numberBuffer.toString());
                    numberBuffer.setLength(0);
                }
                tokens.add(String.valueOf(c));
            }
        }

        if (!numberBuffer.isEmpty()) {
            tokens.add(numberBuffer.toString());
        }

        return tokens;
    }

    private List<String> convertToPostfix(final List<String> tokens) {
        final List<String> postfix = new ArrayList<>();
        final List<String> operators = new ArrayList<>();

        for (final String token : tokens) {
            if (this.isNumeric(token)) {
                postfix.add(token);
            } else if (this.isOperator(token)) {
                while (!operators.isEmpty() && this.precedence(token) <= this.precedence(operators.getLast())) {
                    postfix.add(operators.removeLast());
                }
                operators.add(token);
            }
        }

        while (!operators.isEmpty()) {
            postfix.add(operators.removeLast());
        }

        return postfix;
    }

    private BigDecimal evaluatePostfix(final List<String> postfix) {
        final List<BigDecimal> stack = new ArrayList<>();

        for (final String token : postfix) {
            if (this.isNumeric(token)) {
                stack.add(new BigDecimal(token));
            } else if (this.isOperator(token)) {
                if (stack.size() < 2) {
                    throw new IllegalArgumentException("More than one operator in a row");
                }
                final BigDecimal b = stack.removeLast();
                final BigDecimal a = stack.removeLast();
                stack.add(this.applyOperator(a, b, token));
            }
        }

        if (stack.size() != 1) {
            throw new IllegalArgumentException("Ungültiger Ausdruck");
        }

        return stack.getFirst();
    }

    private boolean isNumeric(final String token) {
        try {
            new BigDecimal(token);
            return true;
        } catch (final NumberFormatException e) {
            return false;
        }
    }

    private int precedence(final String operator) {
        return switch (operator) {
            case KEY_ADD, KEY_SUBTRACT -> 1;
            case KEY_MULTIPLY, KEY_DIVIDE -> 2;
            default -> 0;
        };
    }

    private BigDecimal applyOperator(final BigDecimal a, final BigDecimal b, final String operator) {
        final BigDecimal res = switch (operator) {
            case KEY_ADD -> a.add(b);
            case KEY_SUBTRACT -> a.subtract(b);
            case KEY_MULTIPLY -> a.multiply(b);
            case KEY_DIVIDE -> {
                if (b.equals(BigDecimal.ZERO)) {
                    throw new ArithmeticException("Division by zero");
                }
                yield a.divide(b, 2, RoundingMode.HALF_UP);
            }
            default -> throw new IllegalArgumentException("Invalid operator");
        };
        return res.setScale(2, RoundingMode.HALF_UP);
    }

    private String getWidth(final String s) {
        return switch (s) {
            case KEY_CLEAR, KEY_BACK -> BTN_WIDTH_DOUBLE;
            default -> BTN_WIDTH_SINGLE;
        };
    }

    private boolean isOperator(final String s) {
        return switch (s) {
            case KEY_ADD, KEY_SUBTRACT, KEY_MULTIPLY, KEY_DIVIDE, KEY_EQUALS, KEY_CLEAR, KEY_BACK -> true;
            default -> false;
        };
    }

    private String getOperatorSymbol(final Function function) {
        return switch (function) {
            case ADD -> KEY_ADD;
            case SUBTRACT -> KEY_SUBTRACT;
            case MULTIPLY -> KEY_MULTIPLY;
            case DIVIDE -> KEY_DIVIDE;
            default -> "";
        };
    }

    private Function getFunction(final String key) {
        return switch (key) {
            case KEY_EQUALS -> Function.EQUALS;
            case KEY_CLEAR -> Function.CLEAR;
            case KEY_ADD -> Function.ADD;
            case KEY_SUBTRACT -> Function.SUBTRACT;
            case KEY_MULTIPLY -> Function.MULTIPLY;
            case KEY_DIVIDE -> Function.DIVIDE;
            default -> null;
        };
    }

    public BigDecimal getResult() throws NumberFormatException {
        String value = this.result.getValue();
        value = value.replace(this.decimalSeparator, KEY_POINT);
        return new BigDecimal(value);
    }
}
