package com.warpcoredev.billsplitter.components.groupcomponents;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.warpcoredev.billsplitter.data.dto.GroupJoinRequestDto;
import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.service.GroupService;
import com.warpcoredev.billsplitter.data.service.MappingService;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedStyles;

import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_JOIN_REQUEST_ELEMENT;

@Data
@EqualsAndHashCode(callSuper = false)
public class GroupJoinArea extends VerticalLayout {

    // SERVICES -------------------------------------------------------------------------------------------------------
    private final MappingService mappingService;
    private final GroupService groupService;

    private final H4 header = new H4(this.getTranslation(I18NKeys.ADD_GROUP_JOIN_HEADER));

    private static final String USER_EMAIL_FORMAT = "%s (%s)";

    private List<GroupJoinRequestDto> list;
    private List<GroupJoinRequestDto> joinRequests;
    private Group selectedGroup = new Group();

    public GroupJoinArea(final MappingService mappingService,
                         final GroupService groupService) {
        this.mappingService = mappingService;
        this.groupService = groupService;
    }

    public VerticalLayout initUI() {
        this.joinRequests = this.groupService.getJoinRequests(this.selectedGroup);
        if (this.joinRequests.isEmpty()) {
            this.removeAll();
        } else {
            this.setValues();
            ComponentUtil.addListener(this, GroupJoinArea.GroupJoinChangedEvent.class, e -> {
                this.joinRequests = this.groupService.getJoinRequests(this.selectedGroup);
                this.setValues();
            });
        }
        return this;
    }

    public void setValues() {
        this.list = this.groupService.getJoinRequests(this.selectedGroup);
        this.removeAll();

        if (this.list.isEmpty()) {
            this.removeClassName(SharedStyles.GROUP_JOIN_AREA);
            this.setVisible(false);
            return;
        }

        this.header.setWidthFull();
        this.header.getStyle().setMarginTop("0");
        this.setVisible(true);
        this.add(this.header);
        this.addClassName(SharedStyles.GROUP_JOIN_AREA);

        this.list.forEach(user -> {
            final HorizontalLayout horizontalLayout = new HorizontalLayout();
            horizontalLayout.addClassName(GROUP_JOIN_REQUEST_ELEMENT);

            final VerticalLayout vlUser = new VerticalLayout();
            final Paragraph paragraph = new Paragraph(String.format(USER_EMAIL_FORMAT,
                    user.getUser().getName(),
                    user.getUser().getEmail()));
            paragraph.setWidthFull();
            paragraph.addClassName(SharedStyles.TEXT_ALIGN_LEFT);
            vlUser.addClassNames(SharedStyles.NO_PADDING);
            vlUser.add(paragraph);

            final VerticalLayout vlButtons = new VerticalLayout();
            final Button btnAllow = new Button(this.getTranslation(I18NKeys.ADD_GROUP_JOIN_BTN_ALLOW));
            btnAllow.addClassNames(SharedStyles.BUTTON_GREEN, SharedStyles.BUTTON_PARENT_CLASS);
            btnAllow.setWidthFull();
            btnAllow.addClickListener(event -> this.allowRequest(user));

            final Button btnReject = new Button(this.getTranslation(I18NKeys.ADD_GROUP_JOIN_BTN_REJECT));
            btnReject.addClassNames(SharedStyles.BUTTON_RED, SharedStyles.BUTTON_PARENT_CLASS);
            btnReject.setWidthFull();
            btnReject.addClickListener(event -> this.rejectRequest(user));
            vlButtons.addClassNames(SharedStyles.NO_PADDING, SharedStyles.NO_GAP);
            vlButtons.add(btnAllow, btnReject);

            horizontalLayout.add(vlUser, vlButtons);
            horizontalLayout.setWidthFull();
            horizontalLayout.setAlignItems(Alignment.START);
            horizontalLayout.setJustifyContentMode(JustifyContentMode.BETWEEN);

            this.add(horizontalLayout);
        });
    }

    private void allowRequest(final GroupJoinRequestDto request) {
        this.list.remove(request);
        this.mappingService.allowRequest(request.getMapping());
        this.setValues();
        ComponentUtil.fireEvent(this, new GroupJoinChangedEvent(this, false));
        UI.getCurrent().refreshCurrentRoute(true);
    }

    private void rejectRequest(final GroupJoinRequestDto request) {
        this.list.remove(request);
        this.mappingService.rejectRequest(request.getMapping());
        this.setValues();
        ComponentUtil.fireEvent(this, new GroupJoinChangedEvent(this, false));
    }

    public static class GroupJoinChangedEvent extends ComponentEvent<GroupJoinArea> {
        public GroupJoinChangedEvent(final GroupJoinArea source, final boolean fromClient) {
            super(source, fromClient);
        }
    }
}
