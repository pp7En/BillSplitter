package com.warpcoredev.billsplitter.components.groupcomponents;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.warpcoredev.billsplitter.components.appnav.RefreshAppMenuEvent;
import com.warpcoredev.billsplitter.components.groupcomponents.dialog.DlgCreateUserWithoutLogin;
import com.warpcoredev.billsplitter.components.groupcomponents.dialog.DlgHistory;
import com.warpcoredev.billsplitter.components.groupcomponents.dialog.DlgLeaveGroup;
import com.warpcoredev.billsplitter.components.groupcomponents.dialog.DlgMembersDetails;
import com.warpcoredev.billsplitter.components.groupcomponents.dialog.DlgSettlementTable;
import com.warpcoredev.billsplitter.components.groupcomponents.dialog.DlgShareGroup;
import com.warpcoredev.billsplitter.components.groupcomponents.dialog.DlgStatistic;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.BSNotification;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.NotificationType;
import com.warpcoredev.billsplitter.data.Status;
import com.warpcoredev.billsplitter.data.dto.StatisticDto;
import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.entity.Mapping;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.data.service.BillService;
import com.warpcoredev.billsplitter.data.service.GroupService;
import com.warpcoredev.billsplitter.data.service.MappingService;
import com.warpcoredev.billsplitter.data.service.PaymentService;
import com.warpcoredev.billsplitter.data.service.PdfService;
import com.warpcoredev.billsplitter.data.service.UserService;
import com.warpcoredev.billsplitter.utils.HelperClass;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedConsts;
import com.warpcoredev.billsplitter.utils.SharedStyles;

import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static com.warpcoredev.billsplitter.utils.SharedConsts.ERROR_WHILE_FETCHING_DATA;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_GUEST_USER_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_HISTORY_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_LEAVE_GROUP_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_MEMBER_TABLE_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_OPEN_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_SETTLEMENT_TABLE_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_SHARE_GROUP_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_STATISTIC_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.URL_AUTOLOGIN;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode (callSuper = false)
public class GroupMenuArea extends VerticalLayout {

    // Services ---------------------------------------------------------------------------------------------------
    private BillService billService;
    private PaymentService paymentService;
    private UserService userService;
    private MappingService mappingService;
    private GroupService groupService;
    private PdfService pdfService;

    // Option Buttons ---------------------------------------------------------------------------------------------
    private final Button openOptions = new Button(this.getTranslation(I18NKeys.BILLFORM_OPTIONS_OPTIONS));
    private final Button settlementTable = new Button(this.getTranslation(I18NKeys.BILLFORM_OPTIONS_SETTLEMENT_TABLE));
    private final Button membersTable = new Button(this.getTranslation(I18NKeys.BILLFORM_OPTIONS_GROUP_DETAILS));
    private final Button history = new Button(this.getTranslation(I18NKeys.BILLFORM_OPTIONS_HISTORY));
    private final Button statistic = new Button(this.getTranslation(I18NKeys.BILLFORM_OPTIONS_STATISTIC));
    private final Button createUser = new Button(this.getTranslation(I18NKeys.BILLFORM_OPTIONS_CREATE_USER));
    private final Button leaveGroup = new Button(this.getTranslation(I18NKeys.BILLFORM_OPTIONS_LEAVE_GROUP));
    private final Button shareGroup = new Button(this.getTranslation(I18NKeys.BILLFORM_OPTIONS_GUEST_LOGIN));
    private final Set<Component> setOptionButtons = new LinkedHashSet<>();
    // ------------------------------------------------------------------------------------------------------------
    private final Paragraph groupCode = new Paragraph();
    private final Div divOption = new Div();
    private Group selectedGroup;
    private User user;
    private List<User> groupMembers;
    private boolean isAuthenticatedUser;
    private boolean isGroupAdmin;

    public Div initUI(final boolean isGuest) {
        this.openOptions.setId(GROUP_OPTIONS_OPEN_BTN);
        this.settlementTable.setId(GROUP_OPTIONS_SETTLEMENT_TABLE_BTN);
        this.membersTable.setId(GROUP_OPTIONS_MEMBER_TABLE_BTN);
        this.createUser.setId(GROUP_OPTIONS_GUEST_USER_BTN);
        this.shareGroup.setId(GROUP_OPTIONS_SHARE_GROUP_BTN);
        this.leaveGroup.setId(GROUP_OPTIONS_LEAVE_GROUP_BTN);
        this.history.setId(GROUP_OPTIONS_HISTORY_BTN);
        this.statistic.setId(GROUP_OPTIONS_STATISTIC_BTN);

        this.groupCode.addClassNames(SharedStyles.FONT_XS, SharedStyles.FONT_BOLD);

        this.setOptionButtons.add(this.settlementTable);
        this.setOptionButtons.add(this.membersTable);
        this.setOptionButtons.add(this.history);
        this.setOptionButtons.add(this.statistic);
        if (!isGuest) {
            this.setOptionButtons.add(this.createUser);
            this.setOptionButtons.add(this.leaveGroup);
            this.setOptionButtons.add(this.shareGroup);
            this.setOptionButtons.add(this.groupCode);
        }
        this.setOptionButtons.forEach(e -> e.setVisible(false));

        this.divOption.add(this.openOptions);
        this.setOptionButtons.forEach(this.divOption::add);

        this.divOption.addClassName(SharedStyles.BORDER);
        this.divOption
                .getChildren()
                .forEach(comp -> {
                    if (comp.getElement().getTag().equals(SharedConsts.VAADIN_BUTTON)) {
                        comp.addClassNames(SharedStyles.BUTTON_GROUP_OPTIONS);
                    }
                    comp.addClassNames(SharedStyles.FULL_WIDTH);
                });
        this.divOption.setWidthFull();

        this.openOptions.addClickListener(this::clickHandlerOpenOptions);

        this.settlementTable.addClickListener(e -> this.handleSettlementTable());
        this.membersTable.addClickListener(e -> this.handleMembersTable());
        this.history.addClickListener(e -> this.handleHistory());
        this.statistic.addClickListener(e -> this.handleStatistic(e));

        if (this.isAuthenticatedUser && this.isGroupAdmin) {
            this.createUser.setVisible(true);
            this.leaveGroup.setVisible(true);
        } else {
            this.createUser.setVisible(false);
            this.leaveGroup.setVisible(false);
        }

        this.createUser.addClickListener(e -> this.handleCreateUser());
        this.leaveGroup.addClickListener(e -> this.handleLeaveGroup());
        this.shareGroup.addClickListener(e -> this.handleShareGroup());

        return this.divOption;
    }

    private void handleSettlementTable() {
        final DlgSettlementTable dlgSettlementTable = new DlgSettlementTable(
                this.billService,
                this.paymentService);
        dlgSettlementTable.setValues(this.selectedGroup, this.groupService.getSettlementData(this.selectedGroup));
        this.closeMenu();
    }

    private void handleMembersTable() {
        final DlgMembersDetails dlgMembersDetails = new DlgMembersDetails(this.userService);
        dlgMembersDetails.setValues(this.paymentService.getPaymentsForUsers(this.groupMembers, this.selectedGroup));
        this.closeMenu();
    }

    private void handleHistory() {
        final DlgHistory dlgHistory = new DlgHistory(
                this.billService,
                this.groupService,
                this.pdfService);
        dlgHistory.initUI(this.selectedGroup);
        this.closeMenu();
    }

    private void handleStatistic(final ClickEvent<Button> clickEvent) {
        final DlgStatistic dlgStatistic = new DlgStatistic();
        final UI ui = clickEvent.getSource().getUI().orElseThrow();

        // Prevents an IllegalStateException “Push not enabled” in the UI tests,
        // because Push does not currently work in Selenium.
        if (ui.getPushConfiguration().getPushMode().isEnabled()) {
            final CompletableFuture<List<StatisticDto>> future =
                    this.groupService.getStatisticDataAsync(this.selectedGroup);
            future.thenAccept(statisticData -> ui.access(() -> {
                dlgStatistic.setValues(statisticData);
                ui.push();
            })).exceptionally(ex -> {
                ui.access(() -> Notification.show(
                        ERROR_WHILE_FETCHING_DATA
                                + ex.getMessage()));
                ui.push();
                return null;
            });
        } else {
            // If Push is not activated, the data will not be fetched in async and the UI will be blocked.
            final List<StatisticDto> statisticData = this.groupService.getStatisticDataSync(this.selectedGroup);
            dlgStatistic.setValues(statisticData);
        }
        this.closeMenu();
    }

    private void handleCreateUser() {
        final DlgCreateUserWithoutLogin createUserWithoutLogin = new DlgCreateUserWithoutLogin(
                this.mappingService,
                this.userService);
        createUserWithoutLogin.setGroup(this.selectedGroup);
        createUserWithoutLogin.initUI();
        createUserWithoutLogin.getBtnCreate().addClickListener(ev -> {
            createUserWithoutLogin.createUser();
            createUserWithoutLogin.close();
        });
        this.closeMenu();
    }

    private void handleLeaveGroup() {
        final DlgLeaveGroup dlgLeaveGroup = new DlgLeaveGroup();
        final boolean accountBalanced = this.paymentService.isAccountBalanced(this.user,
                this.selectedGroup);

        dlgLeaveGroup.setValues(accountBalanced, this.isGroupAdmin, this.groupMembers, this.user);

        dlgLeaveGroup.getConfirmBtn().addClickListener(ev ->
                this.handleLeaveGroupListener(dlgLeaveGroup, this.isGroupAdmin));

        this.closeMenu();
    }

    private void handleShareGroup() {
        final DlgShareGroup dlgShareGroup = new DlgShareGroup();
        dlgShareGroup.initUI();

        final boolean isSharedGroup = this.selectedGroup.getCode() != null;
        if (isSharedGroup) {
            final String encodedString = HelperClass.createEncodedLoginString(
                    this.selectedGroup.getId(), this.selectedGroup.getCode());
            dlgShareGroup.setAnchorPublicLink(HelperClass.getURL() + URL_AUTOLOGIN + encodedString);
        }

        dlgShareGroup.getCheckbox().setValue(isSharedGroup);
        dlgShareGroup.getConfirmButton().setEnabled(this.validateGroupCodeState(
                dlgShareGroup.getCheckbox().getValue(),
                this.selectedGroup)
        );
        dlgShareGroup.getConfirmButton().addClickListener(ev -> {
            try {
                this.handlePublicLinkListener(dlgShareGroup);
            } catch (final NoSuchAlgorithmException ex) {
                BSNotification.show(ex.getMessage(), NotificationType.ERROR);
            }
        });
        dlgShareGroup.getCheckbox().addValueChangeListener(ev ->
                dlgShareGroup.getConfirmButton().setEnabled(
                    this.validateGroupCodeState(
                        ev.getValue(), this.selectedGroup)
        ));
        this.closeMenu();
    }

    public void setGroup(final Group group) {
        this.selectedGroup = group;
        this.groupMembers = this.groupService.getGroupMember(group);

        final String strGroupCode = group.getCode() == null
                                  ? this.getTranslation(I18NKeys.SHARE_STATUS_DISABLED)
                                  : group.getCode();

        this.groupCode.setText(
                this.getTranslation(I18NKeys.BILLFORM_OPTIONS_GROUP_CODE)
                        + ": "
                        + strGroupCode);
    }

    private boolean validateGroupCodeState(final Boolean checkbox, final Group group) {
        // Disable button if same state
        return checkbox && group.getCode() == null || !checkbox && group.getCode() != null;
    }

    private void clickHandlerOpenOptions(final ClickEvent<Button> ev) {
        final boolean isVisible = this.setOptionButtons
                .stream()
                .findFirst()
                .orElseThrow()
                .isVisible();

        // if button visible, then hide it
        this.setOptionButtons.forEach(e -> e.setVisible(!isVisible));
    }

    public void closeMenu() {
        this.setOptionButtons.forEach(e -> e.setVisible(false));
    }

    private void handlePublicLinkListener(final DlgShareGroup dlgShareGroup) throws NoSuchAlgorithmException {
        final boolean checkboxIsEnabled = dlgShareGroup.getCheckbox().getValue() != null
                && dlgShareGroup.getCheckbox().getValue();
        if (checkboxIsEnabled && this.selectedGroup.getCode() != null) {
            // Prevents new code if already exists
            dlgShareGroup.getConfirmButton().setEnabled(false);
            return;
        }
        dlgShareGroup.getConfirmButton().setEnabled(true);

        if (checkboxIsEnabled) {
            final String randomCode = HelperClass.getRandomCode(32);
            final String textGroupCode = this.getTranslation(I18NKeys.BILLFORM_OPTIONS_GROUP_CODE) + ": " + randomCode;

            this.selectedGroup.setCode(randomCode);

            final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
            final String encode = bCryptPasswordEncoder.encode(randomCode);

            final User guestUser = this.userService.findGuestUser(this.selectedGroup);
            guestUser.setEnabled(Status.CONFIRMED);
            guestUser.setHashedPassword(encode);
            this.userService.saveEntity(guestUser);

            this.mappingService.createMappingEntryIfNotExist(Mapping.builder()
                    .uid(guestUser)
                    .gid(this.selectedGroup)
                    .isAdmin(false)
                    .status(Status.CONFIRMED)
                    .build());

            // Only if authenticatedUser is GroupOwner
            this.groupCode.setText(textGroupCode);

            final String encodedString = HelperClass.createEncodedLoginString(this.selectedGroup.getId(), randomCode);
            dlgShareGroup.setAnchorPublicLink(HelperClass.getURL() + URL_AUTOLOGIN + encodedString);
        } else {
            final User guestUser = this.userService.findGuestUser(this.selectedGroup);
            guestUser.setHashedPassword(null);
            guestUser.setEnabled(Status.DISABLED);

            this.userService.disableGuestUserPassword(guestUser);
            this.selectedGroup.setCode(null);

            this.groupCode.setText("");
            dlgShareGroup.setAnchorPublicLink(null);
        }

        this.groupService.saveEntity(this.selectedGroup);
        dlgShareGroup.getConfirmButton().setEnabled(false);
        dlgShareGroup.fireNotification();
    }

    private void handleLeaveGroupListener(final DlgLeaveGroup dlgLeaveGroup, final boolean groupAdmin) {
        if (groupAdmin) {
            if (!dlgLeaveGroup.isEmptyListGroupUser()) {
                if (dlgLeaveGroup.getSelectedNewGroupAdmin() == null) {
                    dlgLeaveGroup.fireNotification();
                    return;
                }
                this.selectedGroup.setOwner(dlgLeaveGroup.getSelectedNewGroupAdmin());
                this.groupService.saveGroupWithNewGroupAdmin(this.selectedGroup);
                this.groupService.leaveGroup(this.user, this.selectedGroup);
            } else {
                this.groupService.leaveAndDeleteGroup(this.selectedGroup);
            }
        } else {
            this.groupService.leaveGroup(this.user, this.selectedGroup);
        }

        final Group group = this.groupService.getGroups(this.user).stream().findFirst().orElseThrow();
        UI.getCurrent().navigate(SharedConsts.URL_GROUPS2 + group.getId());
        ComponentUtil.fireEvent(UI.getCurrent(), new RefreshAppMenuEvent(this, false, group.getId()));
        BSNotification.show(
                this.getTranslation(I18NKeys.NOTIFICATIONS_CHANGES_SAVED),
                NotificationType.SUCCESS);

        dlgLeaveGroup.closeDlg();
    }
}
