package com.warpcoredev.billsplitter.components.groupcomponents;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.textfield.BigDecimalField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.warpcoredev.billsplitter.data.entity.Payment;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedStyles;

import java.math.BigDecimal;
import java.math.RoundingMode;
import lombok.Getter;

@Tag(Tag.DIV)
public class PaymentGrid extends FlexLayout {

    @Getter
    private final Checkbox checkbox = new Checkbox();
    private final Div divName = new Div();
    private final Div divAmount = new Div();
    private final BigDecimalField bigDecimalField = new BigDecimalField();
    @Getter
    private final Payment payment;

    public PaymentGrid(final Payment payment) {
        this.payment = payment;

        this.bigDecimalField.setReadOnly(true);
        this.bigDecimalField.setSuffixComponent(new Span(this.getTranslation(I18NKeys.CURRENCY)));
        this.bigDecimalField.addThemeVariants(TextFieldVariant.LUMO_ALIGN_RIGHT);

        this.divAmount.add(this.bigDecimalField);

        this.add(
                this.checkbox,
                this.divName,
                this.divAmount
        );

        this.divName.addClassNames(SharedStyles.TEXT_ALIGN_LEFT);

        this.divAmount.addClassName(SharedStyles.TEXT_ALIGN_RIGHT);

        this.setAlignItems(Alignment.BASELINE);
        this.setFlexGrow(1, this.divName, this.divAmount);
        this.addClassNames(SharedStyles.MARGIN_AUTO, SharedStyles.PAYMENT_BOX);
    }

    public void setValue(final boolean status, final String name, final double amount) {
        this.checkbox.setValue(status);
        this.divName.setText(name);
        this.setAmount(amount);
    }

    public String getName() {
        return this.divName.getText();
    }

    public void setAmount(final double value) {
        BigDecimal bigDecimal = BigDecimal.valueOf(value);
        bigDecimal = bigDecimal.setScale(2, RoundingMode.UP);

        this.bigDecimalField.setValue(bigDecimal);
        this.payment.setHaveToPay(bigDecimal);
    }

}
