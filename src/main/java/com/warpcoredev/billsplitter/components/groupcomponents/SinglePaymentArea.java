package com.warpcoredev.billsplitter.components.groupcomponents;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.BSNotification;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.NotificationType;
import com.warpcoredev.billsplitter.data.PaymentStatus;
import com.warpcoredev.billsplitter.data.entity.Bill;
import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.entity.Payment;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.data.service.BillService;
import com.warpcoredev.billsplitter.data.service.PaymentService;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedStyles;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import static com.warpcoredev.billsplitter.utils.I18NKeys.SINGLE_PAYMENT_FROM;
import static com.warpcoredev.billsplitter.utils.SharedConsts.CURRENCY_FIELDS_MIN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.SINGLE_PAYMENT_FROM_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.SINGLE_PAYMENT_SAVE_BTN_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.SINGLE_PAYMENT_TOTAL_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.SINGLE_PAYMENT_TO_ID;

@Slf4j
public class SinglePaymentArea extends Div {

    private final Select<User> fromUser = new Select<>();
    private final Select<User> toUser = new Select<>();
    private final NumberField billTotalPayable = new NumberField();
    private final Button saveButton = new Button(this.getTranslation(I18NKeys.BILLFORM_BUTTON_SAVE));

    @Setter
    private BillService billService;

    @Setter
    private PaymentService paymentService;

    private Group group;

    public SinglePaymentArea() {
        final Paragraph pageDescription = new Paragraph(this.getTranslation(I18NKeys.SINGLE_PAYMENT_DESC));
        pageDescription.setWidthFull();
        this.billTotalPayable.setWidthFull();
        this.fromUser.setWidthFull();
        this.toUser.setWidthFull();
        this.saveButton.setWidthFull();

        this.billTotalPayable.setLabel(this.getTranslation(I18NKeys.BILLFORM_PAID));
        final Div currency = new Div();
        currency.setText(this.getTranslation(I18NKeys.CURRENCY));
        this.billTotalPayable.setId(SINGLE_PAYMENT_TOTAL_ID);
        this.billTotalPayable.setSuffixComponent(currency);
        this.billTotalPayable.setMin(CURRENCY_FIELDS_MIN);
        this.billTotalPayable.setMax(Double.MAX_VALUE);
        this.billTotalPayable.setValueChangeMode(ValueChangeMode.EAGER);
        this.billTotalPayable.addValueChangeListener(ev -> this.validationForSaveButton());

        this.fromUser.setId(SINGLE_PAYMENT_FROM_ID);
        this.fromUser.setItemLabelGenerator(User::getName);
        this.fromUser.setLabel(this.getTranslation(SINGLE_PAYMENT_FROM));
        this.fromUser.addValueChangeListener(ev -> this.validationForSaveButton());

        this.toUser.setId(SINGLE_PAYMENT_TO_ID);
        this.toUser.setItemLabelGenerator(User::getName);
        this.toUser.setLabel(this.getTranslation(I18NKeys.SINGLE_PAYMENT_TO));
        this.toUser.addValueChangeListener(ev -> this.validationForSaveButton());

        this.saveButton.setId(SINGLE_PAYMENT_SAVE_BTN_ID);
        this.saveButton.setEnabled(false);
        this.saveButton.addClassNames(SharedStyles.BUTTON_GREEN, SharedStyles.BUTTON_PARENT_CLASS);
        this.saveButton.addClickListener(ev -> this.saveBill());

        this.add(
                pageDescription,
                this.billTotalPayable,
                this.fromUser,
                this.toUser,
                this.saveButton
        );
    }

    private void saveBill() {
        final LocalDate now = LocalDate.now();
        final Bill bill = Bill.builder()
                .group(this.group)
                .description(PaymentStatus.SINGLE_PAYMENT.name())
                .collector(this.toUser.getValue())
                .paidDate(now)
                .totalPayable(BigDecimal.valueOf(this.billTotalPayable.getValue()))
                .build();

        final Bill savedEntity = this.billService.saveEntity(bill);

        final List<Payment> payments = new ArrayList<>();
        payments.add(Payment.builder()
                .bill(savedEntity)
                .balance(BigDecimal.valueOf(this.billTotalPayable.getValue()).negate())
                .haveToPay(BigDecimal.valueOf(this.billTotalPayable.getValue()))
                .paid(BigDecimal.ZERO)
                .paidDay(now)
                .group(this.group)
                .user(this.toUser.getValue())
                .status(PaymentStatus.CONFIRMED)
                .build());

        payments.add(Payment.builder()
                .bill(savedEntity)
                .balance(BigDecimal.valueOf(this.billTotalPayable.getValue()))
                .haveToPay(BigDecimal.ZERO)
                .paid(BigDecimal.valueOf(this.billTotalPayable.getValue()))
                .paidDay(now)
                .group(this.group)
                .user(this.fromUser.getValue())
                .status(PaymentStatus.NO_PAYMENT)
                .build());

        this.paymentService.savePayments(payments);

        this.clearFields();
        BSNotification.show(this.getTranslation(I18NKeys.SAVED_SUCCESSFULLY), NotificationType.SUCCESS);
    }

    public void setValues(final Group newGroup, final List<User> groupMembers) {
        this.group = newGroup;
        this.fromUser.setItems(groupMembers);
        this.toUser.setItems(groupMembers);
    }

    private void clearFields() {
        this.billTotalPayable.clear();
        this.fromUser.clear();
        this.toUser.clear();
    }

    private void validationForSaveButton() {
        final boolean fromUserIsInvalid = this.fromUser.getValue() != null
                && this.fromUser.getValue().equals(this.toUser.getValue());

        final boolean toUserIsInvalid = this.toUser.getValue() != null
                && this.toUser.getValue().equals(this.fromUser.getValue());

        final boolean invalid = this.billTotalPayable.isInvalid()
                || fromUserIsInvalid
                || this.fromUser.isEmpty()
                || toUserIsInvalid
                || this.toUser.isEmpty()
                || this.toUser.getValue().equals(this.fromUser.getValue());


        this.fromUser.setInvalid(fromUserIsInvalid);
        this.toUser.setInvalid(toUserIsInvalid);

        final boolean moneyIsEmpty = this.billTotalPayable.isEmpty();
        this.billTotalPayable.setInvalid(moneyIsEmpty);

        this.saveButton.setEnabled(!invalid && !moneyIsEmpty);
    }
}
