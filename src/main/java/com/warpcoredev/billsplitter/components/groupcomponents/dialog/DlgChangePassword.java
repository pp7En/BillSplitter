package com.warpcoredev.billsplitter.components.groupcomponents.dialog;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.textfield.PasswordField;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.BSNotification;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.NotificationType;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.data.service.UserService;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedStyles;

public class DlgChangePassword extends Dialog {

    private final UserService userService;
    private final User authenticatedUser;

    private final Button btnApply = new Button(this.getTranslation(I18NKeys.BUTTON_APPLY));
    private final Button closeBtn = new Button(new Icon("lumo", "cross"), e -> this.close());

    private final PasswordField oldPassword = new PasswordField(
            this.getTranslation(I18NKeys.SETTINGS_PASSWORD_OLD));

    private final PasswordField newPassword1 = new PasswordField(
            this.getTranslation(I18NKeys.SETTINGS_PASSWORD_NEW));

    private final PasswordField newPassword2 = new PasswordField(
            this.getTranslation(I18NKeys.SETTINGS_PASSWORD_NEW_REPEAT));


    public DlgChangePassword(final UserService userService, final User authenticatedUser) {
        this.userService = userService;
        this.authenticatedUser = authenticatedUser;
    }

    public void initUI() {
        this.oldPassword.setWidthFull();
        this.newPassword1.setWidthFull();
        this.newPassword2.setWidthFull();

        this.add(this.oldPassword, this.newPassword1, this.newPassword2);

        this.btnApply.addClickListener(this::checkPassword);
        this.btnApply.addClassNames(SharedStyles.BUTTON_GREEN, SharedStyles.BUTTON_PARENT_CLASS);

        this.setHeaderTitle(this.getTranslation(I18NKeys.SETTINGS_CHANGE_PASSWORD));
        this.closeBtn.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        this.getHeader().add(this.closeBtn);

        this.getFooter().add(this.btnApply);
        this.setWidthFull();
        this.addClassName(SharedStyles.DIALOG_MAX_WIDTH);
        this.open();
    }

    private void checkPassword(final ClickEvent<Button> buttonClickEvent) {
        final boolean passwordCorrect = this.userService.checkCurrentPassword(
                this.authenticatedUser,
                this.oldPassword.getValue());

        if (!passwordCorrect) {
            BSNotification.show(
                    this.getTranslation(I18NKeys.SETTINGS_PASSWORD_WRONG),
                    NotificationType.ERROR);
            return;
        }

        if (!this.newPassword1.getValue().equals(this.newPassword2.getValue())) {
            BSNotification.show(
                    this.getTranslation(I18NKeys.SETTINGS_PASSWORD_DO_NOT_MATCH),
                    NotificationType.ERROR);
        }

        this.userService.setNewPassword(
                this.authenticatedUser,
                this.newPassword1.getValue());
        BSNotification.show(this.getTranslation(I18NKeys.DONE), NotificationType.SUCCESS);
        this.close();
    }
}
