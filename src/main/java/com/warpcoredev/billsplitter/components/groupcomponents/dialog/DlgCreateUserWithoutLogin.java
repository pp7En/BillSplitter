package com.warpcoredev.billsplitter.components.groupcomponents.dialog;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.textfield.TextField;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.BSNotification;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.NotificationType;
import com.warpcoredev.billsplitter.data.Role;
import com.warpcoredev.billsplitter.data.Status;
import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.entity.Mapping;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.data.service.MappingService;
import com.warpcoredev.billsplitter.data.service.UserService;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedStyles;

import lombok.Getter;
import lombok.Setter;

import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_GUEST_USER_SUBMIT;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_GUEST_USER_TXT;

public class DlgCreateUserWithoutLogin extends Dialog {

    private final MappingService mappingService;
    private final UserService userService;

    private final Paragraph description = new Paragraph(this.getTranslation(I18NKeys.CREATE_USER_WITHOUT_LOGIN_DESC));
    private final TextField textField = new TextField();
    @Getter
    private final Button btnCreate = new Button(this.getTranslation(I18NKeys.CREATE_USER_WITHOUT_LOGIN_BTN_CREATE));
    private final Button closeBtn = new Button(new Icon("lumo", "cross"), e -> this.close());
    @Setter
    private Group group;

    public DlgCreateUserWithoutLogin(final MappingService mappingService, final UserService userService) {
        this.mappingService = mappingService;
        this.userService = userService;
    }

    public void initUI() {
        this.textField.setLabel(this.getTranslation(I18NKeys.CREATE_USER_WITHOUT_LOGIN_USERNAME));
        this.textField.setWidthFull();
        this.textField.setId(GROUP_OPTIONS_GUEST_USER_TXT);

        this.btnCreate.addClassNames(SharedStyles.BUTTON_GREEN, SharedStyles.BUTTON_PARENT_CLASS);
        this.btnCreate.setId(GROUP_OPTIONS_GUEST_USER_SUBMIT);

        this.setHeaderTitle(this.getTranslation(I18NKeys.CREATE_USER_WITHOUT_LOGIN_HEADER));
        this.closeBtn.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        this.getHeader().add(this.closeBtn);

        this.add(this.description, this.textField, this.btnCreate);
        this.setWidthFull();
        this.addClassName(SharedStyles.DIALOG_MAX_WIDTH);
        this.open();
    }

    public void createUser() {
        final User user = User.builder()
                .name(this.textField.getValue())
                .enabled(Status.ENABLED)
                .role(Role.USER_WITHOUT_LOGIN)
                .build();

        final Mapping mapping = Mapping.builder()
                .uid(user)
                .gid(this.group)
                .isAdmin(false)
                .status(Status.CONFIRMED)
                .build();

        this.userService.createUser(user);
        this.mappingService.createMappingEntryIfNotExist(mapping);

        BSNotification.show(
                this.getTranslation(I18NKeys.CREATE_USER_WITHOUT_LOGIN_CREATED_SUCCESS),
                NotificationType.SUCCESS);

        this.close();
        UI.getCurrent().refreshCurrentRoute(true);
    }
}

