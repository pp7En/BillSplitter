package com.warpcoredev.billsplitter.components.groupcomponents.dialog;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.textfield.PasswordField;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.BSNotification;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.NotificationType;
import com.warpcoredev.billsplitter.data.Status;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.data.service.PaymentService;
import com.warpcoredev.billsplitter.data.service.UserService;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedStyles;

import lombok.RequiredArgsConstructor;

import static com.warpcoredev.billsplitter.utils.SharedConsts.DLG_DELETE_ACCOUNT_BTN_APPLY;
import static com.warpcoredev.billsplitter.utils.SharedConsts.DLG_DELETE_ACCOUNT_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.DLG_DELETE_ACCOUNT_PASSWORD_FIELD;

@RequiredArgsConstructor
public class DlgDeleteAccount extends Dialog {

    private final UserService userService;
    private final User authenticatedUser;
    private final PaymentService paymentService;
    private final Button btnApply = new Button(this.getTranslation(I18NKeys.BUTTON_APPLY));
    private final Button closeBtn = new Button(new Icon("lumo", "cross"), e -> this.close());
    private final Paragraph paragraphAccountNotBalanced = new Paragraph(
            this.getTranslation(I18NKeys.SETTINGS_DELETE_ACCOUNT_ACCOUNT_NOT_BALANCED));
    private final PasswordField password = new PasswordField(this.getTranslation(I18NKeys.SETTINGS_PASSWORD));
    private final Paragraph description = new Paragraph(this.getTranslation(I18NKeys.SETTINGS_DELETE_ACCOUNT_DESC));

    public void initUI() {
        this.setHeaderTitle(this.getTranslation(I18NKeys.SETTINGS_DELETE_ACCOUNT));
        this.getHeader().add(this.closeBtn);

        if (!this.paymentService.isAccountBalanced(this.authenticatedUser)) {
            this.add(this.paragraphAccountNotBalanced);
        } else {
            this.description.setWidthFull();
            this.password.setWidthFull();
            this.password.setId(DLG_DELETE_ACCOUNT_PASSWORD_FIELD);

            this.add(this.description, this.password);

            this.btnApply.addClassNames(SharedStyles.BUTTON_GREEN, SharedStyles.BUTTON_PARENT_CLASS);
            this.btnApply.setId(DLG_DELETE_ACCOUNT_BTN_APPLY);
            this.btnApply.addClickListener(e -> this.checkPassword());
            this.getFooter().add(this.btnApply);
        }

        this.setWidthFull();
        this.addClassName(SharedStyles.DIALOG_MAX_WIDTH);
        this.setId(DLG_DELETE_ACCOUNT_ID);
        this.open();
    }

    private void checkPassword() {
        final boolean passwordCorrect = this.userService.checkCurrentPassword(
                this.authenticatedUser,
                this.password.getValue());

        if (!passwordCorrect) {
            BSNotification.show(
                    this.getTranslation(I18NKeys.SETTINGS_PASSWORD_WRONG),
                    NotificationType.ERROR);
            return;
        }

        final User user = this.userService.findUserByMailAddress(this.authenticatedUser.getEmail());
        user.setHashedPassword(null);
        user.setEmail(null);
        user.setEnabled(Status.DISABLED);
        user.setNotifications(Status.DISABLED);
        user.setValidationCode(null);
        user.setValidationCodeValidUntil(null);
        this.userService.saveEntity(user);

        final Dialog dlg = new Dialog();

        final Paragraph accDeleteDesc = new Paragraph(
                this.getTranslation(I18NKeys.SETTINGS_DELETE_ACCOUNT_CONFIRM_DESC));

        final Button button = new Button(this.getTranslation(I18NKeys.DIALOG_CLOSE));
        button.addClickListener(e -> {
            UI.getCurrent().getSession().close();
            UI.getCurrent().getPage().setLocation("/");
        });

        dlg.add(accDeleteDesc, button);
        dlg.open();
    }


}
