package com.warpcoredev.billsplitter.components.groupcomponents.dialog;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.CallbackDataProvider;
import com.vaadin.flow.data.provider.Query;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.LocalDateRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.dom.Style;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.theme.lumo.LumoIcon;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.BSNotification;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.NotificationType;
import com.warpcoredev.billsplitter.data.dto.HistoryDto;
import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.service.BillService;
import com.warpcoredev.billsplitter.data.service.GroupService;
import com.warpcoredev.billsplitter.data.service.PdfService;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedStyles;

import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import static com.warpcoredev.billsplitter.data.PaymentStatus.SINGLE_PAYMENT;
import static com.warpcoredev.billsplitter.utils.SharedConsts.BUTTON_DOWNLOAD_PDF;
import static com.warpcoredev.billsplitter.utils.SharedConsts.DATE_FORMATTER_HUMAN_READABLE_SHORT;
import static com.warpcoredev.billsplitter.utils.SharedConsts.HISTORY_DIALOG_SEARCH_FIELD;
import static com.warpcoredev.billsplitter.utils.SharedConsts.ID_DOWNLOAD_PDF_ANCHOR;
import static com.warpcoredev.billsplitter.utils.SharedConsts.JS_CLICK_DOWNLOAD_PDF;

@Slf4j
@RequiredArgsConstructor
public class DlgHistory extends Dialog {

    private static final int PAGE_SIZE = 20;
    private static final float PDF_ICON_WIDTH = 1.25f;

    private final BillService billService;
    private final GroupService groupService;
    private final PdfService pdfService;

    private final Button closeBtn = new Button(new Icon("lumo", "cross"), e -> this.close());
    private final Button saveBtn = new Button(this.getTranslation(I18NKeys.DIALOG_SAVE));
    private final Grid<HistoryDto> grid = new Grid<>(HistoryDto.class, false);
    private final TextField searchField = new TextField();
    private final Button pdfButton = new Button();
    private final Image pdfIcon = new Image("images/pdf.png", "");

    private final Anchor downloadLink = new Anchor("");

    private CallbackDataProvider<HistoryDto, Void> dataProvider;

    private List<HistoryDto> listToDelete;

    public void initUI(final Group group) {
        this.searchField.setId(HISTORY_DIALOG_SEARCH_FIELD);

        this.listToDelete = new ArrayList<>();
        this.configureGridColumns();
        this.configureGridProperties(group);
        this.configureSearchField();
        this.configureHeader();
        this.configureFooter(group);
        this.configureLayout();
    }

    private void configureGridColumns() {
        this.grid.addColumn(new LocalDateRenderer<>(
                        e -> e.getBill().getPaidDate(),
                        () -> DATE_FORMATTER_HUMAN_READABLE_SHORT))
                .setAutoWidth(true)
                .setHeader(this.getTranslation(I18NKeys.HISTORY_DATE));

        this.grid.addColumn(e -> {
            final String desc = e.getBill().getDescription();
            return desc.equals(SINGLE_PAYMENT.name())
                    ? this.getTranslation(I18NKeys.SINGLE_PAYMENT_PAYMENT_DETAILS)
                    : desc;
        }).setHeader(this.getTranslation(I18NKeys.HISTORY_DESC));

        this.grid.setPartNameGenerator(dto -> this.isMarkedForDeletion(dto) ? "history-delete" : null);

        this.grid.addColumn(new ComponentRenderer<>(Button::new, (button, dto) -> {
            button.addThemeVariants(ButtonVariant.LUMO_ICON, ButtonVariant.LUMO_ERROR, ButtonVariant.LUMO_TERTIARY);
            button.addClickListener(e -> this.toggleRemoveFromGrid(dto));
            button.setIcon(this.isMarkedForDeletion(dto) ? LumoIcon.UNDO.create() : VaadinIcon.TRASH.create());
        })).setAutoWidth(true).setTextAlign(ColumnTextAlign.END);

        this.grid.setItemDetailsRenderer(this.createHistoryDetailFormLayout());
        this.grid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES, GridVariant.LUMO_WRAP_CELL_CONTENT);
        this.grid.setPageSize(PAGE_SIZE);
    }

    private void configureGridProperties(final Group group) {
        this.dataProvider = new CallbackDataProvider<>(
                query -> {
                    final int offset = query.getOffset();
                    final int limit = query.getLimit();
                    return this.groupService
                            .getHistoryData(group, this.searchField.getValue().trim(), offset, limit)
                            .stream();
                },
                query -> this.groupService.countTotalHistoryData(group, this.searchField.getValue().trim())
        );

        this.grid.setDataProvider(this.dataProvider);
    }

    private void configureSearchField() {
        this.searchField.addValueChangeListener(e -> this.dataProvider.refreshAll());
        this.searchField.setWidthFull();
        this.searchField.setPlaceholder(this.getTranslation(I18NKeys.HISTORY_SEARCH));
        this.searchField.setPrefixComponent(new Icon(VaadinIcon.SEARCH));
        this.searchField.setValueChangeMode(ValueChangeMode.EAGER);
        this.add(this.searchField, this.grid);
    }

    private void configureHeader() {
        this.setHeaderTitle(this.getTranslation(I18NKeys.HISTORY_HEADER));
        this.closeBtn.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        this.getHeader().add(this.closeBtn);
    }

    private void configureFooter(final Group group) {
        this.saveBtn.addClassNames(SharedStyles.BUTTON_GREEN, SharedStyles.BUTTON_PARENT_CLASS);
        this.saveBtn.setEnabled(false);
        this.saveBtn.addClickListener(this::handleSave);

        this.pdfIcon.setWidth(PDF_ICON_WIDTH, Unit.EM);
        this.pdfIcon.getStyle().set("vertical-align", "middle");
        this.pdfButton.setId(BUTTON_DOWNLOAD_PDF);
        this.pdfButton.addClassNames(SharedStyles.BUTTON_PARENT_CLASS, SharedStyles.BUTTON_GREEN);
        this.pdfButton.setIcon(this.pdfIcon);
        this.pdfButton.addClickListener(event -> this.handlePdfDownload(group));

        final int itemCount = this.dataProvider.size(new Query<>());
        this.pdfButton.setEnabled(itemCount > 0);

        this.getFooter().add(this.downloadLink, this.pdfButton, this.saveBtn);
    }

    private void handlePdfDownload(final Group group) {
        final StreamResource resource = this.pdfService.getPdf(UI.getCurrent().getLocale(), group);
        if (resource == null) {
            return;
        }
        this.downloadLink.getElement().setAttribute("download", true);
        this.downloadLink.setId(ID_DOWNLOAD_PDF_ANCHOR);
        this.downloadLink.setHref(resource);
        this.downloadLink.getStyle().setDisplay(Style.Display.NONE);
        UI.getCurrent().getPage().executeJs(JS_CLICK_DOWNLOAD_PDF);
    }

    private void configureLayout() {
        this.setWidthFull();
        this.addClassName(SharedStyles.DIALOG_MAX_WIDTH);
        this.open();
    }

    private boolean isMarkedForDeletion(final HistoryDto item) {
        return this.listToDelete
                .stream()
                .anyMatch(dto -> dto.getBill().getId().equals(item.getBill().getId()));
    }

    private ComponentRenderer<HistoryDetailsFormLayout, HistoryDto> createHistoryDetailFormLayout() {
        return new ComponentRenderer<>(HistoryDetailsFormLayout::new,
                HistoryDetailsFormLayout::setHistoryDto);
    }

    private void toggleRemoveFromGrid(final HistoryDto item) {
        if (this.isMarkedForDeletion(item)) {
            this.listToDelete.removeIf(dto -> dto.getBill().getId().equals(item.getBill().getId()));
        } else {
            this.listToDelete.add(item);
        }

        this.saveBtn.setEnabled(!this.listToDelete.isEmpty());
        this.grid.getDataProvider().refreshAll();
    }

    private void handleSave(final ClickEvent<Button> event) {
        this.billService.deleteEntity(this.listToDelete);

        this.close();

        BSNotification.show(
                this.getTranslation(I18NKeys.SAVED_SUCCESSFULLY),
                NotificationType.SUCCESS);
    }
}
