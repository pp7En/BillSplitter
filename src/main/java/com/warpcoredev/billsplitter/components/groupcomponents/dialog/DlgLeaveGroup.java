package com.warpcoredev.billsplitter.components.groupcomponents.dialog;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.select.Select;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.BSNotification;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.NotificationType;
import com.warpcoredev.billsplitter.data.Role;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedStyles;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

import static com.warpcoredev.billsplitter.utils.SharedConsts.VIEW_LEAVE_GROUP_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.VIEW_LEAVE_GROUP_SELECT_NEW_ADMIN;

public class DlgLeaveGroup extends Dialog {
    private final Paragraph pGroupAdmin = new Paragraph(this.getTranslation(I18NKeys.LEAVE_GROUP_GROUP_ADMIN));
    private final Paragraph pNotBalanced = new Paragraph(this.getTranslation(I18NKeys.LEAVE_GROUP_NOT_BALANCED));
    private final Paragraph pInfotext = new Paragraph(this.getTranslation(I18NKeys.LEAVE_GROUP_INFOTEXT));
    private final Select<User> selectNewGroupAdmin = new Select<>();
    @Getter
    private final Button confirmBtn = new Button(this.getTranslation(I18NKeys.LEAVE_GROUP_CONFIRM_BTN));
    private final Button closeBtn = new Button(new Icon("lumo", "cross"), e -> this.close());

    private List<User> groupMembers = new ArrayList<>();

    public void setValues(final boolean isAccountBalanced, final boolean isAdmin, final List<User> listGroupMembers,
                          final User user) {
        this.groupMembers = listGroupMembers;
        this.groupMembers.remove(user);
        this.confirmBtn.setEnabled(false);
        this.confirmBtn.addClassNames(SharedStyles.BUTTON_GREEN, SharedStyles.BUTTON_PARENT_CLASS);

        this.confirmBtn.setId(VIEW_LEAVE_GROUP_BTN);
        this.selectNewGroupAdmin.setId(VIEW_LEAVE_GROUP_SELECT_NEW_ADMIN);

        if (!isAccountBalanced) {
            this.add(this.pNotBalanced);
        } else {
            final List<User> listGroupMembersWithoutGuests = listGroupMembers
                    .stream()
                    .filter(e -> e.getRole().equals(Role.USER) || e.getRole().equals(Role.ADMIN))
                    .toList();
            if (isAdmin && !listGroupMembersWithoutGuests.isEmpty()) {
                this.add(this.pGroupAdmin, this.selectNewGroupAdmin);
                this.createSelectElement();
            } else {
                this.confirmBtn.setEnabled(true);
            }
            this.add(this.pInfotext, this.confirmBtn);
        }

        this.setHeaderTitle(this.getTranslation(I18NKeys.LEAVE_GROUP_HEADER));
        this.closeBtn.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        this.getHeader().add(this.closeBtn);

        this.setWidthFull();
        this.addClassName(SharedStyles.DIALOG_MAX_WIDTH);
        this.open();
    }

    private void createSelectElement() {
        this.selectNewGroupAdmin.setLabel(this.getTranslation(I18NKeys.LEAVE_GROUP_NEW_GROUP_ADMIN));
        this.selectNewGroupAdmin.setItemLabelGenerator(User::getName);
        this.selectNewGroupAdmin.setItems(this.groupMembers);
        this.selectNewGroupAdmin.setWidthFull();
        this.selectNewGroupAdmin.setRequiredIndicatorVisible(true);
        this.selectNewGroupAdmin.addValueChangeListener(ev -> {
            if (null != ev.getValue()) {
                this.confirmBtn.setEnabled(true);
            }
        });
    }

    public User getSelectedNewGroupAdmin() {
        return this.selectNewGroupAdmin.getValue();
    }

    public void fireNotification() {
        BSNotification.show(
                this.getTranslation(I18NKeys.LEAVE_GROUP_NEW_GROUP_ADMIN),
                NotificationType.SUCCESS);
    }

    public void closeDlg() {
        this.close();
    }

    public boolean isEmptyListGroupUser() {
        return this.groupMembers.isEmpty();
    }

}
