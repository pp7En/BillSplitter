package com.warpcoredev.billsplitter.components.groupcomponents.dialog;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.Icon;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedConsts;
import com.warpcoredev.billsplitter.utils.SharedStyles;

import lombok.Getter;

public class DlgMailSuccessful extends Dialog {
    private final Paragraph message = new Paragraph(this.getTranslation(I18NKeys.EMAIL_SIGNUP_SUCCESSFUL_MESSAGE));
    @Getter
    private final Button closeBtn = new Button(new Icon("lumo", "cross"), e -> this.close());


    public void initUI() {
        this.setHeaderTitle(this.getTranslation(I18NKeys.EMAIL_SIGNUP_SUCCESSFUL_HEADER));
        this.closeBtn.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        this.getHeader().add(this.closeBtn);

        this.add(this.message);
        this.setWidthFull();
        this.addClassName(SharedStyles.DIALOG_MAX_WIDTH);
        this.open();

        this.closeBtn.addClickListener(e -> UI.getCurrent().navigate(SharedConsts.URL_LOGIN2));
    }

}
