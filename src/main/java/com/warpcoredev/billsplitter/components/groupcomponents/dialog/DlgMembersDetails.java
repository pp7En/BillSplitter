package com.warpcoredev.billsplitter.components.groupcomponents.dialog;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.warpcoredev.billsplitter.components.FFSimpleGrid;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.BSNotification;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.NotificationType;
import com.warpcoredev.billsplitter.data.dto.GroupMemberDto;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.data.service.UserService;
import com.warpcoredev.billsplitter.utils.HelperClass;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedStyles;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;

import static com.warpcoredev.billsplitter.utils.SharedStyles.MEMBER_TABLE_LINE;

@RequiredArgsConstructor
public class DlgMembersDetails extends Dialog {
    public static final int COLUMNS = 3;

    private final Div content = new Div();
    private final Button closeBtn = new Button(new Icon("lumo", "cross"), e -> this.close());
    private final Button saveBtn = new Button(this.getTranslation(I18NKeys.DIALOG_SAVE));
    private final DecimalFormat currencyFormatter = HelperClass.currencyFormat(
            this.getTranslation(I18NKeys.CURRENCY_FORMAT),
            this.getTranslation(I18NKeys.CURRENCY_DECIMAL_SEPARATOR),
            this.getTranslation(I18NKeys.CURRENCY_GROUPING_SEPARATOR));

    private List<GroupMemberDto> list = new ArrayList<>();
    private List<User> listToDelete;

    private final UserService userService;

    @PostConstruct
    private void initUI() {
        this.addClassName(SharedStyles.NO_PADDING);
    }

    public void setValues(final List<GroupMemberDto> listGroupMemberDto) {
        this.list = listGroupMemberDto;
        this.listToDelete = new ArrayList<>();


        final FFSimpleGrid header = new FFSimpleGrid(this.getTranslation(I18NKeys.MEMBERS_TABLE_USER),
                                                     this.getTranslation(I18NKeys.MEMBERS_TABLE_BALANCE));
        header.enableHeaderPaddingForButton();
        header.enableHeaderStyleByColumns(COLUMNS);
        this.add(header);

        this.content.setWidthFull();
        this.add(this.content);

        this.insertGrid();

        this.saveBtn.addClassNames(SharedStyles.BUTTON_GREEN, SharedStyles.BUTTON_PARENT_CLASS);
        this.saveBtn.setEnabled(false);
        this.saveBtn.addClickListener(this::handleSave);

        this.setHeaderTitle(this.getTranslation(I18NKeys.MEMBERS_TABLE_HEADER));
        this.closeBtn.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        this.getHeader().add(this.closeBtn);

        this.getFooter().add(this.saveBtn);
        this.setWidthFull();
        this.addClassName(SharedStyles.DIALOG_MAX_WIDTH);
        this.open();
    }

    private void insertGrid() {
        this.content.removeAll();

        this.list.forEach(item -> {
            final BigDecimal balance;
            if (item.getBalance() == null) {
                balance = BigDecimal.valueOf(0.0);
            } else {
                balance = item.getBalance();
            }

            final Button delete = new Button();
            delete.addThemeVariants(ButtonVariant.LUMO_ICON,
                    ButtonVariant.LUMO_ERROR,
                    ButtonVariant.LUMO_TERTIARY);
            delete.addClickListener(e -> this.deleteUser(item));
            delete.setIcon(new Icon(VaadinIcon.TRASH));

            final FFSimpleGrid member = new FFSimpleGrid(
                    item.getUser().getName(),
                    this.currencyFormatter.format(balance),
                    delete);

            member.addClassName(MEMBER_TABLE_LINE);

            this.content.add(member);
        });
    }

    private void handleSave(final ClickEvent<Button> event) {
        this.userService.disableUsers(this.listToDelete);

        this.close();

        BSNotification.show(
                this.getTranslation(I18NKeys.SAVED_SUCCESSFULLY),
                NotificationType.SUCCESS);
    }

    private void deleteUser(final GroupMemberDto groupMemberDto) {
        this.saveBtn.setEnabled(true);
        this.list.remove(groupMemberDto);
        this.listToDelete.add(groupMemberDto.getUser());
        this.insertGrid();
    }
}
