package com.warpcoredev.billsplitter.components.groupcomponents.dialog;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.warpcoredev.billsplitter.data.entity.Mapping;
import com.warpcoredev.billsplitter.data.service.MappingService;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedStyles;

import java.util.List;

import static com.warpcoredev.billsplitter.utils.SharedConsts.AWAITING_GROUP_JOIN_REQUEST_VL;
import static com.warpcoredev.billsplitter.utils.SharedConsts.REJECTED_GROUP_JOIN_REQUEST_VL;

public class DlgNotification extends Dialog {

    private final Button closeBtn = new Button(new Icon("lumo", "cross"), e -> this.close());

    private final MappingService mappingService;
    private final List<Mapping> awaitingGroupJoinRequests;
    private final List<Mapping> rejectedGroupJoinRequests;

    public DlgNotification(final MappingService mappingService,
                           final List<Mapping> awaitingGroupJoinRequests,
                           final List<Mapping> rejectedGroupJoinRequests) {
        this.mappingService = mappingService;
        this.awaitingGroupJoinRequests = awaitingGroupJoinRequests;
        this.rejectedGroupJoinRequests = rejectedGroupJoinRequests;
    }

    public void initUI() {
        this.setHeaderTitle(this.getTranslation(I18NKeys.ADD_GROUP_NOTIFICATIONS_HEADER));
        this.closeBtn.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        this.getHeader().add(this.closeBtn);

        final Div divAwaitingRequests = new Div();
        final Paragraph headerAwaiting = new Paragraph(this.getTranslation(I18NKeys.ADD_GROUP_NOTIFICATIONS_AWAITING));
        divAwaitingRequests.add(headerAwaiting);
        this.awaitingGroupJoinRequests.forEach(e -> {
            final HorizontalLayout hl = this.createElement(e);
            hl.setClassName(AWAITING_GROUP_JOIN_REQUEST_VL);
            divAwaitingRequests.add(hl);
        });

        if (!this.awaitingGroupJoinRequests.isEmpty()) {
            this.add(divAwaitingRequests);
        }

        final Div divRejectedRequests = new Div();
        final Paragraph headerRejected = new Paragraph(this.getTranslation(I18NKeys.ADD_GROUP_NOTIFICATIONS_REJECTED));
        divRejectedRequests.add(headerRejected);
        this.rejectedGroupJoinRequests.forEach(e -> {
            final HorizontalLayout hl = this.createElement(e);
            hl.setClassName(REJECTED_GROUP_JOIN_REQUEST_VL);
            divRejectedRequests.add(hl);
        });

        if (!this.rejectedGroupJoinRequests.isEmpty()) {
            this.add(divRejectedRequests);
        }

        this.setWidthFull();
        this.addClassName(SharedStyles.DIALOG_MAX_WIDTH);
        this.open();
    }

    private HorizontalLayout createElement(final Mapping mappingEntry) {
        final HorizontalLayout hl = new HorizontalLayout();
        final Paragraph name = new Paragraph(mappingEntry.getGid().getName());
        name.setWidthFull();
        final Button button = new Button(new Icon(VaadinIcon.TRASH), this::delete);
        button.addThemeVariants(ButtonVariant.LUMO_ICON,
                ButtonVariant.LUMO_ERROR,
                ButtonVariant.LUMO_TERTIARY);
        button.addClickListener(ev -> {
            this.mappingService.removeMapping(mappingEntry);
            this.close();
            UI.getCurrent().refreshCurrentRoute(true);
        });
        hl.add(button, name);
        return hl;
    }

    private void delete(final ClickEvent<Button> event) {
    }
}
