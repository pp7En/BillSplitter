package com.warpcoredev.billsplitter.components.groupcomponents.dialog;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.dom.Style;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.BSNotification;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.NotificationType;
import com.warpcoredev.billsplitter.data.PaymentStatus;
import com.warpcoredev.billsplitter.data.dto.SettlementDto;
import com.warpcoredev.billsplitter.data.entity.Bill;
import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.entity.Payment;
import com.warpcoredev.billsplitter.data.service.BillService;
import com.warpcoredev.billsplitter.data.service.PaymentService;
import com.warpcoredev.billsplitter.utils.HelperClass;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedStyles;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static com.warpcoredev.billsplitter.utils.SharedStyles.SETTLEMENT_TABLE_LINE;

public class DlgSettlementTable extends Dialog {

    private final PaymentService paymentService;
    private final BillService billService;

    private final Button closeBtn = new Button(new Icon("lumo", "cross"), e -> this.close());
    private final DecimalFormat currencyFormatter = HelperClass.currencyFormat(
            this.getTranslation(I18NKeys.CURRENCY_FORMAT),
            this.getTranslation(I18NKeys.CURRENCY_DECIMAL_SEPARATOR),
            this.getTranslation(I18NKeys.CURRENCY_GROUPING_SEPARATOR));

    public DlgSettlementTable(final BillService billService,
                              final PaymentService paymentService) {
        this.billService = billService;
        this.paymentService = paymentService;
    }

    public void setValues(final Group group, final List<SettlementDto> list) {
        final List<SettlementDto> filtered = list.stream()
                .toList();

        if (filtered.isEmpty()) {
            this.add(new Paragraph(this.getTranslation(I18NKeys.SETTLEMENT_TABLE_EVERYTHING_BALANCED)));
        } else {
            filtered.forEach(settlementDto -> {
                final HorizontalLayout horizontalLayout = new HorizontalLayout();
                horizontalLayout.addClassName(SETTLEMENT_TABLE_LINE);
                final Paragraph user = new Paragraph();
                user.setText(
                        String.join(
                                " ",
                                settlementDto.getFrom().getName(),
                                this.getTranslation(I18NKeys.SETTLEMENT_TABLE_OWES),
                                settlementDto.getTo().getName()
                        )
                );
                user.setWidthFull();

                final Paragraph money = new Paragraph();
                money.setText(this.currencyFormatter.format(settlementDto.getMoney()));
                money.setWidthFull();
                money.getStyle().setTextAlign(Style.TextAlign.RIGHT);

                final Button paid = new Button(this.getTranslation(I18NKeys.SETTLEMENT_TABLE_PAID));
                paid.addClassNames(SharedStyles.BUTTON_PARENT_CLASS, SharedStyles.BUTTON_BLUE);
                paid.addClickListener(ev -> this.paid(group, settlementDto));

                horizontalLayout.add(user, money, paid);
                horizontalLayout.setWidthFull();
                horizontalLayout.setAlignItems(FlexComponent.Alignment.STRETCH);
                horizontalLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.BETWEEN);

                this.add(horizontalLayout);
            });
        }

        this.setHeaderTitle(this.getTranslation(I18NKeys.SETTLEMENT_TABLE_HEADER));
        this.closeBtn.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        this.getHeader().add(this.closeBtn);

        this.setWidthFull();
        this.addClassName(SharedStyles.DIALOG_MAX_WIDTH);

        this.open();
    }


    private void paid(final Group group, final SettlementDto data) {
        final LocalDate date = LocalDate.now();
        final Bill balancing = Bill.builder()
                .collector(data.getFrom())
                .paidDate(date)
                .group(group)
                .totalPayable(data.getMoney())
                .description(this.getTranslation(I18NKeys.BALANCING_PAYMENT))
                .build();
        final Bill bill = this.billService.saveEntity(balancing);

        final List<Payment> paymentList = new ArrayList<>();
        paymentList.add(
            Payment.builder()
                    .user(data.getFrom())
                    .group(group)
                    .bill(bill)
                    .haveToPay(BigDecimal.ZERO)
                    .paid(data.getMoney())
                    .paidDay(date)
                    .status(PaymentStatus.CONFIRMED)
                    .balance(data.getMoney())
                    .build());
        paymentList.add(
                Payment.builder()
                        .user(data.getTo())
                        .group(group)
                        .bill(bill)
                        .haveToPay(data.getMoney())
                        .paid(BigDecimal.ZERO)
                        .paidDay(date)
                        .status(PaymentStatus.NO_PAYMENT)
                        .balance(data.getMoney().negate())
                        .build());

        this.paymentService.savePayments(paymentList);
        BSNotification.show(this.getTranslation(I18NKeys.SAVED_SUCCESSFULLY), NotificationType.SUCCESS);
        this.close();
    }
}
