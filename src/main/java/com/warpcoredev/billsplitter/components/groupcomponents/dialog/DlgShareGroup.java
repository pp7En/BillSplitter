package com.warpcoredev.billsplitter.components.groupcomponents.dialog;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.BSNotification;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.NotificationType;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedStyles;

import lombok.Getter;

import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_SHARE_GROUP_CBX;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_SHARE_GROUP_LINK;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_SHARE_GROUP_SUBMIT;

public class DlgShareGroup extends Dialog {
    private final Paragraph desc = new Paragraph(this.getTranslation(I18NKeys.SHARE_DESC));
    @Getter
    private final Checkbox checkbox = new Checkbox(this.getTranslation(I18NKeys.SHARE_LABEL));

    private final VerticalLayout verticalLayout = new VerticalLayout();
    private final Anchor publicLink = new Anchor();
    private final Button confirmBtn = new Button(this.getTranslation(I18NKeys.SHARE_CONFIRM_BUTTON));
    private final Button closeBtn = new Button(new Icon("lumo", "cross"), e -> this.close());

    public void initUI() {
        this.desc.setWidthFull();
        this.checkbox.setWidthFull();
        this.checkbox.setId(GROUP_OPTIONS_SHARE_GROUP_CBX);

        this.publicLink.setWidthFull();
        this.publicLink.setId(GROUP_OPTIONS_SHARE_GROUP_LINK);
        this.publicLink.addClassName(SharedStyles.LINE_BREAKS_ANYWHERE);
        this.verticalLayout.add(this.publicLink);

        this.confirmBtn.addClassNames(SharedStyles.BUTTON_GREEN, SharedStyles.BUTTON_PARENT_CLASS);
        this.confirmBtn.setId(GROUP_OPTIONS_SHARE_GROUP_SUBMIT);

        this.setHeaderTitle(this.getTranslation(I18NKeys.SHARE_HEADER));
        this.closeBtn.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        this.getHeader().add(this.closeBtn);

        this.add(this.desc, this.checkbox, this.confirmBtn, this.verticalLayout);
        this.setWidthFull();
        this.addClassName(SharedStyles.DIALOG_MAX_WIDTH);
        this.open();
    }

    public Button getConfirmButton() {
        return this.confirmBtn;
    }

    public void setAnchorPublicLink(final String encodedUrl) {
        if (encodedUrl == null) {
            this.publicLink.setText("");
            this.publicLink.removeHref();
            return;
        }
        this.publicLink.setText(encodedUrl);
        this.publicLink.setHref(encodedUrl);
    }

    public void fireNotification() {
        BSNotification.show(
                this.getTranslation(I18NKeys.SAVED_SUCCESSFULLY),
                NotificationType.SUCCESS);
    }
}
