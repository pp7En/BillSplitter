package com.warpcoredev.billsplitter.components.groupcomponents.dialog;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.Icon;
import com.warpcoredev.billsplitter.components.FFSimpleGrid;
import com.warpcoredev.billsplitter.components.chartjs.ChartJS;
import com.warpcoredev.billsplitter.data.dto.StatisticDto;
import com.warpcoredev.billsplitter.utils.HelperClass;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedStyles;

import java.text.DecimalFormat;
import java.util.List;
import software.xdev.chartjs.model.charts.BarChart;

import static com.warpcoredev.billsplitter.utils.SharedConsts.STATISTIC_DIALOG_ID;

public class DlgStatistic extends Dialog {

    private final DecimalFormat currencyFormatter = HelperClass.currencyFormat(
            this.getTranslation(I18NKeys.CURRENCY_FORMAT),
            this.getTranslation(I18NKeys.CURRENCY_DECIMAL_SEPARATOR),
            this.getTranslation(I18NKeys.CURRENCY_GROUPING_SEPARATOR));
    private final Paragraph pleaseWait = new Paragraph();

    public DlgStatistic() {
        super();
        this.setHeaderTitle(this.getTranslation(I18NKeys.HISTORY_HEADER));
        this.pleaseWait.setText(this.getTranslation(I18NKeys.PLEASE_WAIT));

        final Button closeBtn = new Button(new Icon("lumo", "cross"), e -> this.close());
        closeBtn.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

        this.getHeader().add(closeBtn);
        this.add(this.pleaseWait);
        this.setWidthFull();
        this.addClassNames(SharedStyles.DIALOG_MAX_WIDTH);
        this.open();
    }

    public void setValues(final List<StatisticDto> list) {
        this.pleaseWait.setVisible(false);
        final Div div = new Div();
        div.setId(STATISTIC_DIALOG_ID);

        final ChartJS chartJS = new ChartJS();
        div.add(chartJS);

        final BarChart barChart = chartJS.generateStatisticChart(list);
        chartJS.setData(barChart);

        final FFSimpleGrid header = new FFSimpleGrid(this.getTranslation(I18NKeys.STATISTIC_COLUMN_DATE),
                                                    this.getTranslation(I18NKeys.STATISTIC_COLUMN_SUM));
        header.enableHeaderStyleByColumns(2);
        this.add(div, header);

        list.reversed().forEach(item -> this.add(
                new FFSimpleGrid(
                        item.getDateMonth(),
                        this.currencyFormatter.format(item.getSum()))));
    }
}
