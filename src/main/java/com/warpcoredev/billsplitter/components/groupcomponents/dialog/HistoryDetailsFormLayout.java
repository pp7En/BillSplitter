package com.warpcoredev.billsplitter.components.groupcomponents.dialog;

import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.warpcoredev.billsplitter.components.FFSimpleGrid;
import com.warpcoredev.billsplitter.data.dto.HistoryDto;
import com.warpcoredev.billsplitter.utils.HelperClass;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedStyles;

import java.text.DecimalFormat;
import java.util.Comparator;

import static com.warpcoredev.billsplitter.utils.SharedConsts.DASH;

public class HistoryDetailsFormLayout extends FormLayout {

    private final TextField paidBy = new TextField(this.getTranslation(I18NKeys.HISTORY_PAID_BY));
    private final TextField total = new TextField(this.getTranslation(I18NKeys.HISTORY_TOTAL));
    private final VerticalLayout vlParts = new VerticalLayout();
    private final DecimalFormat currencyFormatter = HelperClass.currencyFormat(
            this.getTranslation(I18NKeys.CURRENCY_FORMAT),
            this.getTranslation(I18NKeys.CURRENCY_DECIMAL_SEPARATOR),
            this.getTranslation(I18NKeys.CURRENCY_GROUPING_SEPARATOR));


    public HistoryDetailsFormLayout() {
        this.initUI();
    }

    private void initUI() {
        this.paidBy.setReadOnly(true);
        this.total.setReadOnly(true);
        this.vlParts.setWidthFull();
        this.vlParts.addClassName(SharedStyles.NO_GAP);
        this.vlParts.getStyle().setPadding("0.25em 0.25em 3em 0.25em");

        this.add(this.paidBy, this.total, this.vlParts);

        this.setResponsiveSteps(new ResponsiveStep("0", 2));
        this.setColspan(this.paidBy, 1);
        this.setColspan(this.total, 1);
        this.setColspan(this.vlParts, 2);
    }

    public void setHistoryDto(final HistoryDto dto) {
        this.paidBy.setValue(
                dto.getBill().getCollector().getName());

        this.total.setValue(
                this.currencyFormatter.format(
                        dto.getBill().getTotalPayable()));

        if (dto.getBill().getDescription().equals(this.getTranslation(I18NKeys.BALANCING_PAYMENT))) {
            this.total.setValue(DASH);
        }

        final H5 header = new H5(this.getTranslation(I18NKeys.DETAILS));
        header.setWidthFull();
        header.addClassNames(SharedStyles.INNER_BORDER);
        header.getStyle().setMarginTop("1em");
        header.getStyle().setColor("#5e6a7a");
        this.vlParts.add(header);

        dto.getPayments().stream()
                .sorted(Comparator.comparing(payment -> payment.getUser().getName()))
                .forEach(payment -> {
                    final FFSimpleGrid member = new FFSimpleGrid(
                        payment.getUser().getName(),
                        this.currencyFormatter.format(payment.getHaveToPay()));
                    member.addClassName(SharedStyles.INNER_BORDER);
                    member.getStyle().setColor("#5e6a7a");
                    this.vlParts.add(member);
                });

    }
}
