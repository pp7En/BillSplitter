package com.warpcoredev.billsplitter.components.groupcomponents.notifications;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.warpcoredev.billsplitter.utils.I18NKeys;

import lombok.Builder;

@Builder
@Tag(Tag.DIV)
public class BSNotification extends Notification {

    public static final int DURATION = 2_000;
    private final Notification notification = Notification.show(
            this.getTranslation(I18NKeys.CREATE_USER_WITHOUT_LOGIN_CREATED_SUCCESS));
    private NotificationType type;

    public static Notification show(final String text, final NotificationType type) {
        final Notification notification = new Notification(text, DURATION, Position.MIDDLE);
        NotificationVariant variant = NotificationVariant.LUMO_PRIMARY;

        if (type.equals(NotificationType.SUCCESS)) {
            variant = NotificationVariant.LUMO_SUCCESS;
        } else if (type.equals(NotificationType.ERROR)) {
            variant = NotificationVariant.LUMO_ERROR;
        }

        notification.addThemeVariants(variant);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.open();
        return notification;
    }
}
