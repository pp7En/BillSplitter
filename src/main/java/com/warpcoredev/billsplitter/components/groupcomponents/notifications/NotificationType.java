package com.warpcoredev.billsplitter.components.groupcomponents.notifications;

import lombok.experimental.FieldNameConstants;

@FieldNameConstants (onlyExplicitlyIncluded = true)
public enum NotificationType {

    @FieldNameConstants.Include
    SUCCESS,

    @FieldNameConstants.Include
    ERROR,

    @FieldNameConstants.Include
    INFO;

}
