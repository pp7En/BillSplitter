package com.warpcoredev.billsplitter.data;

import lombok.experimental.FieldNameConstants;

@FieldNameConstants (onlyExplicitlyIncluded = true)
public enum PaymentStatus {

    @FieldNameConstants.Include
    NO_PAYMENT,

    @FieldNameConstants.Include
    REQUEST_SEND,

    @FieldNameConstants.Include
    REJECTED,

    @FieldNameConstants.Include
    CONFIRMED,

    @FieldNameConstants.Include
    SINGLE_PAYMENT;
}
