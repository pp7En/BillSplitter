package com.warpcoredev.billsplitter.data;

import lombok.experimental.FieldNameConstants;

@FieldNameConstants (onlyExplicitlyIncluded = true)
public enum Role {

    @FieldNameConstants.Include
    UNKNOWN,

    @FieldNameConstants.Include
    USER,

    @FieldNameConstants.Include
    GUEST,

    @FieldNameConstants.Include
    FOR_PUBLIC_SHARE,

    @FieldNameConstants.Include
    USER_WITHOUT_LOGIN,

    @FieldNameConstants.Include
    ADMIN;

}
