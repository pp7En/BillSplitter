package com.warpcoredev.billsplitter.data;

import lombok.experimental.FieldNameConstants;

@FieldNameConstants (onlyExplicitlyIncluded = true)
public enum Status {

    @FieldNameConstants.Include
    DISABLED,

    @FieldNameConstants.Include
    ENABLED,

    @FieldNameConstants.Include
    REQUESTED,

    @FieldNameConstants.Include
    CONFIRMED,

    @FieldNameConstants.Include
    REJECTED;
}
