package com.warpcoredev.billsplitter.data.dto;

import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.entity.Mapping;
import com.warpcoredev.billsplitter.data.entity.User;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GroupJoinRequestDto implements Serializable {
    private User user;
    private Group group;
    private Mapping mapping;
}
