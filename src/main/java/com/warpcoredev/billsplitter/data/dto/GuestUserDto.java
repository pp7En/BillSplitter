package com.warpcoredev.billsplitter.data.dto;

import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.entity.User;

import java.io.Serializable;

public record GuestUserDto(User user, Group group) implements Serializable {

}
