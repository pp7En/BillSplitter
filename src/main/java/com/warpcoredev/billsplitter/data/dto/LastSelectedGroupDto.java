package com.warpcoredev.billsplitter.data.dto;

import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.entity.Setting;

import java.io.Serializable;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LastSelectedGroupDto implements Serializable {
    private Group group;
    private Setting setting;
}
