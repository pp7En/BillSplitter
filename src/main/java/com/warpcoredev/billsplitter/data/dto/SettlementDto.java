package com.warpcoredev.billsplitter.data.dto;

import com.warpcoredev.billsplitter.data.entity.User;

import java.io.Serializable;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SettlementDto implements Serializable {
    private User from;
    private User to;
    private BigDecimal money;
}
