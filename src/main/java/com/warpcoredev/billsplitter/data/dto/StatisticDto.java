package com.warpcoredev.billsplitter.data.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StatisticDto implements Serializable {
    private String dateMonth;
    private BigDecimal sum;
}
