package com.warpcoredev.billsplitter.data.dto;

import com.warpcoredev.billsplitter.data.entity.User;

import java.io.Serializable;
import java.math.BigDecimal;

public record UserPaymentsDto(User user, BigDecimal value) implements Serializable {

}
