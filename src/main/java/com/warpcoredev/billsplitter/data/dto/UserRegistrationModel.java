package com.warpcoredev.billsplitter.data.dto;

import java.io.Serializable;
import lombok.Data;

@Data
public class UserRegistrationModel implements Serializable {
    private String name;
    private String email;
    private String password;
    private String passwordConfirmation;
}
