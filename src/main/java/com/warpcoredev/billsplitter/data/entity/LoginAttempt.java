package com.warpcoredev.billsplitter.data.entity;

import java.time.LocalDateTime;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table (name = "login_attempt")
public class LoginAttempt {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    // can be ip address or username
    private String object;

    private LocalDateTime datetime;

    private Integer counter;

    private Boolean notified;
}
