package com.warpcoredev.billsplitter.data.entity;

import com.warpcoredev.billsplitter.data.Status;

import java.io.Serializable;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "mapping")
public class Mapping implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "uid")
    @NotNull
    private User uid;

    @ManyToOne
    @JoinColumn(name = "gid")
    @NotNull
    private Group gid;

    @NotNull
    private boolean isAdmin;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status;
}
