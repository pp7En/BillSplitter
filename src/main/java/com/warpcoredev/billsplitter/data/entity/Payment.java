package com.warpcoredev.billsplitter.data.entity;

import com.warpcoredev.billsplitter.data.PaymentStatus;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "payments")
public class Payment implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @NotNull
    private User user;

    @ManyToOne
    @JoinColumn(name = "group_id")
    @NotNull
    private Group group;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "bill_id")
    private Bill bill;

    @NotNull
    private BigDecimal haveToPay;

    @NotNull
    private BigDecimal paid;

    @NotNull
    private BigDecimal balance;

    @NotNull
    private LocalDate paidDay;

    @NotNull
    @Enumerated(EnumType.STRING)
    private PaymentStatus status;

}
