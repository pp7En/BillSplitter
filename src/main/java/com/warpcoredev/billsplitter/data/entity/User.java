package com.warpcoredev.billsplitter.data.entity;

import com.warpcoredev.billsplitter.data.Role;
import com.warpcoredev.billsplitter.data.Status;

import java.io.Serializable;
import java.time.LocalDateTime;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "users")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String name;

    private String email;

    @Column(name = "pass_hash")
    private String hashedPassword;

    @Enumerated(EnumType.STRING)
    private Status enabled;

    @Column(name = "validation_code")
    private String validationCode;

    @Column(name = "last_login")
    private long lastLogin;

    private LocalDateTime validationCodeValidUntil;

    @Enumerated(EnumType.STRING)
    private Status notifications;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role;

}
