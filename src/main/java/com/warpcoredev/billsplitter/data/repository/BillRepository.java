package com.warpcoredev.billsplitter.data.repository;

import com.warpcoredev.billsplitter.data.entity.Bill;
import com.warpcoredev.billsplitter.data.entity.Group;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface BillRepository extends CrudRepository<Bill, Long>, Serializable {

    @Query (value = """
            SELECT b
            FROM Bill b
            WHERE b.group = :group
            ORDER BY b.paidDate DESC, b.id
            """)
    List<Bill> getBillsOfGroup(@Param ("group") Group group);

    @Query(value = """
            SELECT b
            FROM Bill b
            WHERE b.group = :group
              AND LOWER(b.description) LIKE LOWER(CONCAT('%', :searchTerm, '%'))
            ORDER BY b.paidDate DESC, b.id DESC
            """)
    List<Bill> getBillsOfGroupWithPagination(@Param("group") Group group,
                                             @Param("searchTerm") String searchTerm,
                                             Pageable pageable);

    @Query(value = """
            SELECT COUNT(b)
            FROM Bill b
            WHERE b.group = :group
              AND LOWER(b.description) LIKE LOWER(CONCAT('%', :searchTerm, '%'))
            """)
    int countBillsOfGroup(@Param("group") Group group,
                          @Param("searchTerm") String searchTerm);

    @Query(value = """
            SELECT b.description
            FROM Bill b
            WHERE b.group = :group
              AND b.description != 'SINGLE_PAYMENT'
              AND b.description != :balancingPayment
            GROUP BY b.description
            ORDER BY COUNT(*) DESC
            LIMIT 10
            """)
    List<String> getMostCommonDescriptions(@Param("group") Group group,
                                           @Param("balancingPayment") String balancingPayment);
}
