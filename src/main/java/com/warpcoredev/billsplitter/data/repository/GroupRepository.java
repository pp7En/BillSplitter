package com.warpcoredev.billsplitter.data.repository;

import com.warpcoredev.billsplitter.data.Role;
import com.warpcoredev.billsplitter.data.Status;
import com.warpcoredev.billsplitter.data.dto.GroupJoinRequestDto;
import com.warpcoredev.billsplitter.data.dto.GroupMemberDto;
import com.warpcoredev.billsplitter.data.dto.SettlementDataDto;
import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.entity.User;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends CrudRepository<Group, Long>, Serializable {

    @Query(value = """
           SELECT g
           FROM Group g
           INNER JOIN Mapping m ON m.gid.id = g.id
           WHERE m.uid = :user
             AND m.status = :status
           """)
    List<Group> getGroups(@Param("user") User user,
                          @Param("status") Status status);

    @Query(value = """
           SELECT g
           FROM Group g
           INNER JOIN Mapping m ON m.gid.id = g.id
           WHERE m.uid = :user
             AND m.status = :status
             AND g.id = :groupId
           """)
    Optional<Group> getGroupById(@Param("user") User user,
                                 @Param("status") Status status,
                                 @Param("groupId") Long groupId);

    @Query(value = """
           SELECT g
           FROM Group g
           WHERE g.id = :groupId
           """)
    Group getGroupById(@Param("groupId") Long groupId);

    @Query(value = """
           SELECT u
           FROM User u
           INNER JOIN Mapping m ON m.uid.id = u.id
           WHERE m.gid = :group
             AND u.role <> :role
             AND m.status = :confirmed
             AND u.enabled = :enabled
           ORDER BY u.name
           """)
    List<User> getGroupMemberWithoutGuests(@Param("group") Group group,
                                           @Param("role") Role role,
                                           @Param("confirmed") Status confirmed,
                                           @Param("enabled") Status status);

    @Query(value = """
           SELECT new com.warpcoredev.billsplitter.data.dto.SettlementDataDto(
             u,
             SUM(p.balance)
           )
           FROM Payment p
           INNER JOIN User u ON u.id = p.user.id
           WHERE p.group = :group
           GROUP BY u
           """)
    List<SettlementDataDto> getSettlementData(@Param("group") Group group);

    @Query(value = """
           SELECT new com.warpcoredev.billsplitter.data.dto.GroupMemberDto(
             p.user,
             COUNT(p.paid),
             SUM(p.balance)
           )
           FROM Payment p
           WHERE p.group = :group
           GROUP BY p.user
           """)
    List<GroupMemberDto> getMembersData(@Param("group") Group group);

    @Query(value = """
           SELECT new com.warpcoredev.billsplitter.data.dto.GroupJoinRequestDto(u, g, m)
           FROM Group g
           INNER JOIN Mapping m ON m.gid.id = g.id
           INNER JOIN User u ON u.id = m.uid.id
           WHERE m.gid = :group
             AND m.status = :status
           """)
    List<GroupJoinRequestDto> getJoinRequests(@Param("group") Group group,
                                              @Param("status") Status status);

    @Query(value = """
           SELECT g
           FROM Group g
           WHERE g.code = :code
           """)
    Group findGroupByInviteCode(@Param("code") String code);

    @Query(value = """
           SELECT g
           FROM Group g
           WHERE g.owner = :user
           """)
    List<Group> getGroupByOwner(@Param("user") User user);

}
