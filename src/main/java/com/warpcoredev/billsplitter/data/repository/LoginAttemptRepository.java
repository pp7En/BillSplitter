package com.warpcoredev.billsplitter.data.repository;

import com.warpcoredev.billsplitter.data.entity.LoginAttempt;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginAttemptRepository extends CrudRepository<LoginAttempt, Long>, Serializable {

    @Query (value = """
            SELECT c
            FROM LoginAttempt c
            """)
    List<LoginAttempt> findAll();

    @Query (value = """
            SELECT c
            FROM LoginAttempt c
            WHERE c.object = :exp
            """)
    Optional<LoginAttempt> findByObject(@Param("exp") String expression);

}
