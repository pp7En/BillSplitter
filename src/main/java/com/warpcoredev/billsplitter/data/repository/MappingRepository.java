package com.warpcoredev.billsplitter.data.repository;

import com.warpcoredev.billsplitter.data.Status;
import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.entity.Mapping;
import com.warpcoredev.billsplitter.data.entity.User;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MappingRepository extends CrudRepository<Mapping, Long>, Serializable {

    @Query(value = """
           SELECT m
           FROM Mapping m
           WHERE m.gid = :group AND m.uid = :user
           """)
    Mapping getMapping(@Param("user") User user,
                       @Param("group") Group group);

    @Query(value = """
           SELECT m
           FROM Mapping m
           INNER JOIN Group g ON g.id = m.gid.id
           WHERE m.uid = :user
             AND m.status = :status
           """)
    List<Mapping> getStatusGroupJoinRequests(@Param("user") User user,
                                             @Param("status") Status status);

    @Modifying
    @Query(value = """
           DELETE FROM Mapping m
           WHERE m.uid = :user
             AND m.gid = :group
           """)
    void deleteByUidGid(@Param("user") User user,
                        @Param("group") Group group);

    @Modifying
    @Query(value = """
           DELETE FROM Mapping m
           WHERE m.gid = :group
           """)
    void deleteByGid(@Param("group") Group group);
}
