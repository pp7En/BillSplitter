package com.warpcoredev.billsplitter.data.repository;

import com.warpcoredev.billsplitter.data.dto.GroupMemberDto;
import com.warpcoredev.billsplitter.data.entity.Bill;
import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.entity.Payment;
import com.warpcoredev.billsplitter.data.entity.User;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepository extends CrudRepository<Payment, Long>, Serializable {

    @Query(value = """
           SELECT SUM(p.balance)
           FROM Payment p
           WHERE p.user = :user
           """)
    Double getUserBalance(@Param("user") User user);

    @Query(value = """
           SELECT SUM(p.balance)
           FROM Payment p
           WHERE p.user = :user AND p.group = :group
           """)
    Double getUserBalance(@Param("user") User user,
                          @Param("group") Group group);

    @Query(value = """
           SELECT new com.warpcoredev.billsplitter.data.dto.GroupMemberDto(
             u,
             (SELECT COUNT(p.paid) FROM Payment p WHERE p.user = :user AND p.group = :group),
             (SELECT SUM(p.balance) FROM Payment p WHERE p.user = :user AND p.group = :group)
           )
           FROM Mapping m
           INNER JOIN User u ON u.id = m.uid.id
           WHERE m.gid = :group
             AND m.uid = :user
           """)
    GroupMemberDto getPaymentsByUser(@Param("user") User user,
                                     @Param("group") Group group);

    @Query(value = """
           SELECT p
           FROM Payment p
           WHERE p.group = :group
           """)
    List<Payment> getPaymentsByGroup(@Param("group") Group group);

    @Query(value = """
           SELECT p
           FROM Payment p
           WHERE p.bill = :bill
           """)
    List<Payment> getPaymentsByBill(@Param("bill") Bill bill);
}
