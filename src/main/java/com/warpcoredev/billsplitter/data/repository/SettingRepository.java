package com.warpcoredev.billsplitter.data.repository;

import com.warpcoredev.billsplitter.data.entity.Setting;

import java.io.Serializable;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SettingRepository extends CrudRepository<Setting, Long>, Serializable {

    @Query (value = """
            SELECT s
            FROM Setting s
            WHERE s.name = :setting
            """)
    Setting getValue(@Param ("setting") String settingKey);

}
