package com.warpcoredev.billsplitter.data.repository;

import com.warpcoredev.billsplitter.data.Role;
import com.warpcoredev.billsplitter.data.Status;
import com.warpcoredev.billsplitter.data.dto.GuestUserDto;
import com.warpcoredev.billsplitter.data.entity.User;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long>, JpaSpecificationExecutor<User>, Serializable {

    @Query(value = """
           SELECT u
           FROM User u
           WHERE u.email = :email
           """)
    Optional<User> optFindByEmail(@Param("email") String email);

    @Query(value = """
           SELECT u
           FROM User u
           WHERE u.validationCode = :validationCode
             AND u.email = :email
           """)
    Optional<User> validateValidationCode(@Param("email") String email,
                                          @Param("validationCode") String validationCode);

    @Query(value = """
           SELECT new com.warpcoredev.billsplitter.data.dto.GuestUserDto(u, g)
           FROM User u
           INNER JOIN Mapping m ON m.uid.id = u.id
           INNER JOIN Group g ON g.id = m.gid.id
           WHERE g.id = :groupId
             AND u.enabled = :status
             AND u.role = :role
             AND g.code = :token
           """)
    Optional<GuestUserDto> findGuestAccount(@Param("groupId") String groupId,
                                            @Param("token") String token,
                                            @Param("role") Role role,
                                            @Param("status") Status status);

    @Query(value = """
           SELECT u
           FROM User u
           INNER JOIN Mapping m ON m.uid.id = u.id
           INNER JOIN Group g ON g.id = m.gid.id
           WHERE g.id = :groupId
             AND u.role = :role
           """)
    Optional<User> findGuestAccount(@Param("groupId") Long groupId,
                                    @Param("role") Role role);

    @Query(value = """
           SELECT u
           FROM User u
           WHERE u.validationCodeValidUntil <= CURRENT_TIMESTAMP
           """)
    List<User> findNonActivatedUserOlderThan();
}
