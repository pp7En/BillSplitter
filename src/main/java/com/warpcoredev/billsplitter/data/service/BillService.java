package com.warpcoredev.billsplitter.data.service;

import com.warpcoredev.billsplitter.data.dto.HistoryDto;
import com.warpcoredev.billsplitter.data.entity.Bill;
import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.repository.BillRepository;

import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;

import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class BillService implements Serializable {

    private final BillRepository billRepository;

    public void deleteEntity(final List<HistoryDto> deleteList) {
        deleteList.forEach(item -> this.billRepository.delete(item.getBill()));
    }

    public Bill saveEntity(final Bill bill) {
        return this.billRepository.save(bill);
    }

    public List<Bill> getBillsByGroup(final Group group) {
        return this.billRepository.getBillsOfGroup(group);
    }

    public List<String> getMostCommonDescriptions(final Group group, final String balancingPayment) {
        return this.billRepository.getMostCommonDescriptions(group, balancingPayment);
    }
}
