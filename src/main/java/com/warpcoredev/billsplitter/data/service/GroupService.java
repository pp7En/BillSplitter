package com.warpcoredev.billsplitter.data.service;

import com.warpcoredev.billsplitter.data.Role;
import com.warpcoredev.billsplitter.data.Status;
import com.warpcoredev.billsplitter.data.dto.GroupJoinRequestDto;
import com.warpcoredev.billsplitter.data.dto.HistoryDto;
import com.warpcoredev.billsplitter.data.dto.SettlementDataDto;
import com.warpcoredev.billsplitter.data.dto.SettlementDto;
import com.warpcoredev.billsplitter.data.dto.StatisticDto;
import com.warpcoredev.billsplitter.data.dto.UserPaymentsDto;
import com.warpcoredev.billsplitter.data.entity.Bill;
import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.entity.Mapping;
import com.warpcoredev.billsplitter.data.entity.Setting;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.data.repository.BillRepository;
import com.warpcoredev.billsplitter.data.repository.GroupRepository;
import com.warpcoredev.billsplitter.data.repository.MappingRepository;
import com.warpcoredev.billsplitter.data.repository.PaymentRepository;
import com.warpcoredev.billsplitter.data.repository.SettingRepository;
import com.warpcoredev.billsplitter.data.repository.UserRepository;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;

import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import static com.warpcoredev.billsplitter.utils.SharedConsts.DATE_FORMATTER;
import static com.warpcoredev.billsplitter.utils.SharedConsts.PREFIX_LAST_GROUP;
import static com.warpcoredev.billsplitter.utils.SharedConsts.RANGE_STATISTICS_IN_MONTHS;

@Service
@AllArgsConstructor
public class GroupService implements Serializable {

    private final GroupRepository groupRepository;
    private final BillRepository billRepository;
    private final MappingRepository mappingRepository;
    private final PaymentRepository paymentRepository;
    private final SettingRepository settingRepository;
    private final UserRepository userRepository;

    public List<Group> getGroups(final User user) {
        return this.groupRepository.getGroups(user, Status.CONFIRMED);
    }

    public Group getLastGroup(final User user) {
        final Setting setting = this.settingRepository.getValue(PREFIX_LAST_GROUP + user.getId());
        if (setting != null) {
            return this.groupRepository.getGroupById(Long.parseLong(setting.getValue()));
        }
        return null;
    }

    public Optional<Group> getGroupById(final Long id) {
        return this.groupRepository.findById(id);
    }

    public Optional<Group> getGroupById(final User user, final Long id) {
        return this.groupRepository.getGroupById(user, Status.CONFIRMED, id);
    }
    public List<User> getGroupMember(final Group group) {
        // only confirmed and enabled accounts
        return this.groupRepository.getGroupMemberWithoutGuests(
                group,
                Role.FOR_PUBLIC_SHARE,
                Status.CONFIRMED,
                Status.ENABLED);
    }

    public List<SettlementDto> getSettlementData(final Group group) {
        final List<SettlementDataDto> resultList = this.groupRepository.getSettlementData(group);
        final List<UserPaymentsDto> personsCredit = new ArrayList<>();
        final List<UserPaymentsDto> personsDebt = new ArrayList<>();

        resultList.forEach(p -> {
            final BigDecimal value = p.getBalance();
            final User user = p.getUser();
            if (value.compareTo(BigDecimal.ZERO) > 0) {
                personsCredit.add(new UserPaymentsDto(user, value));
            } else if (value.compareTo(BigDecimal.ZERO) < 0) {
                personsDebt.add(new UserPaymentsDto(user, value));
            }
        });

        return this.calcSettlementList(personsCredit, personsDebt);
    }

    private List<SettlementDto> calcSettlementList(final List<UserPaymentsDto> personsCredit,
                                                   final List<UserPaymentsDto> personsDebt) {
        final List<SettlementDto> list = new ArrayList<>();
        int indexDebt = 0;
        int indexCredit = 0;
        boolean paidOff = false;

        for (int i = 0; i < personsCredit.size(); i++) {
            if (i < personsDebt.size()
                    && personsCredit.get(i).value().compareTo(personsDebt.get(i).value().negate()) < 0) {
                list.add(
                        SettlementDto.builder()
                                .from(personsDebt.get(indexDebt).user())
                                .to(personsCredit.get(indexCredit).user())
                                .money(personsCredit.get(indexCredit).value())
                                .build()
                );
                indexCredit++;
            } else {
                while (!paidOff && indexDebt < personsDebt.size()) {
                    list.add(
                            SettlementDto.builder()
                                    .from(personsDebt.get(indexDebt).user())
                                    .to(personsCredit.get(indexCredit).user())
                                    .money(personsDebt.get(indexDebt).value().negate())
                                    .build()
                    );
                    indexDebt++;
                    if (personsCredit.get(indexCredit).value().compareTo(BigDecimal.ZERO) == 0) {
                        paidOff = true;
                    }
                }
            }
        }

        return list;
    }

    /**
     * Synchronously retrieves statistical data for a given group.
     *
     * <p><strong>Note:</strong> This method performs synchronous processing
     * and should only be used in exceptional cases. This method will block the
     * UI until the data has been processed.
     *
     * @param group the group for which to retrieve statistical data
     * @return a list of {@link StatisticDto} objects representing the aggregated statistics
     */
    public List<StatisticDto> getStatisticDataSync(final Group group) {
        return this.calculateStatistics(group);
    }

    /**
     * Asynchronously retrieves statistical data for a given group.
     *
     * <p>This method processes the bills associated with the specified group,
     * filters them based on a time range, and aggregates them into a list of
     * {@link StatisticDto} objects. The data is sorted by month. The result is
     * returned as a {@link CompletableFuture}.
     *
     * @param group the group for which to retrieve statistical data
     * @return a {@link CompletableFuture} containing a list of {@link StatisticDto}
     *         objects representing the aggregated statistics
     */
    @Async
    public CompletableFuture<List<StatisticDto>> getStatisticDataAsync(final Group group) {
        return CompletableFuture.completedFuture(this.calculateStatistics(group));
    }

    private List<StatisticDto> calculateStatistics(final Group group) {
        final List<Bill> billsOfGroup = this.billRepository.getBillsOfGroup(group);

        final LocalDate limit = LocalDate.now()
                .minusMonths(RANGE_STATISTICS_IN_MONTHS)
                .withDayOfMonth(1);

        return billsOfGroup.stream()
                .filter(e -> e.getPaidDate().isEqual(limit) || e.getPaidDate().isAfter(limit))
                .map(e -> new StatisticDto(DATE_FORMATTER.format(e.getPaidDate()), e.getTotalPayable()))
                .collect(Collectors.toMap(
                        StatisticDto::getDateMonth,
                        Function.identity(),
                        (sum1, sum2) -> new StatisticDto(sum1.getDateMonth(), sum1.getSum().add(sum2.getSum()))
                ))
                .values()
                .stream()
                .sorted(Comparator.comparing(StatisticDto::getDateMonth))
                .toList();
    }

    public void saveGroupWithNewGroupAdmin(final Group group) {
        this.groupRepository.save(group);
    }

    public void leaveGroup(final User user, final Group group) {
        final Mapping mapping = this.mappingRepository.getMapping(user, group);
        this.mappingRepository.delete(mapping);
    }

    public Group saveEntity(final Group group) {
        return this.groupRepository.save(group);
    }

    public List<GroupJoinRequestDto> getJoinRequests(final Group group) {
        return this.groupRepository.getJoinRequests(group, Status.REQUESTED);
    }

    public Group findGroupByInviteCode(final String code) {
        return this.groupRepository.findGroupByInviteCode(code);
    }

    public Group saveAndReturnGroup(final Group group) {
        return this.groupRepository.save(group);
    }

    public void leaveAndDeleteGroup(final Group group) {
        this.paymentRepository.getPaymentsByGroup(group).forEach(this.paymentRepository::delete);
        this.billRepository.getBillsOfGroup(group)
                .forEach(this.billRepository::delete);
        this.mappingRepository.deleteByGid(group);
        this.groupRepository.delete(group);
    }

    public List<HistoryDto> getHistoryData(final Group group,
                                           final String searchTerm,
                                           final int offset,
                                           final int limit) {

        return this.billRepository.getBillsOfGroupWithPagination(
                        group,
                        searchTerm.toLowerCase(),
                        PageRequest.of(offset / limit, limit)
                ).stream()
                .map(bill -> HistoryDto.builder()
                        .bill(bill)
                        .payments(this.paymentRepository.getPaymentsByBill(bill))
                        .build())
                .toList();
    }

    public int countTotalHistoryData(final Group group,
                                     final String searchTerm) {

        return this.billRepository.countBillsOfGroup(
                group,
                searchTerm.toLowerCase()
        );
    }

    public List<Group> getAllGroups() {
        final List<Group> list = new ArrayList<>();
        this.groupRepository.findAll().forEach(list::add);
        return list;
    }

    public List<Group> findGroupsByOwner(final String email) {
        final User user = this.userRepository.optFindByEmail(email).orElseThrow();
        return this.groupRepository.getGroupByOwner(user);
    }
}
