package com.warpcoredev.billsplitter.data.service;

import com.warpcoredev.billsplitter.data.entity.LoginAttempt;
import com.warpcoredev.billsplitter.data.repository.LoginAttemptRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;

import static com.warpcoredev.billsplitter.utils.SharedConsts.MAX_LOGIN_ATTEMPT;


@Service
@Slf4j
@AllArgsConstructor
public class LoginAttemptService {

    private final HttpServletRequest request;
    private final LoginAttemptRepository loginAttemptRepository;
    private final MailService mailService;

    public void loginFailed(final String ipAddress) {
        final Optional<LoginAttempt> optLoginAttempt = this.loginAttemptRepository.findByObject(ipAddress);

        if (optLoginAttempt.isEmpty()) {
            log.warn("Login attempt! First try from {}", ipAddress);
            this.createNewEntity(ipAddress);
            return;
        }

        final LoginAttempt loginAttempt = optLoginAttempt.get();
        final int newValue = loginAttempt.getCounter() + 1;
        log.warn("Login attempt! No. {} from {}", newValue, ipAddress);
        loginAttempt.setCounter(newValue);
        loginAttempt.setDatetime(LocalDateTime.now());
        this.loginAttemptRepository.save(loginAttempt);
    }

    public boolean isBlocked(final HttpServletRequest httpServletRequest) {
        final String xcHeader = httpServletRequest.getHeader("X-Real-IP");
        final String ipAddress = xcHeader == null ? httpServletRequest.getRemoteAddr() : xcHeader;

        final List<LoginAttempt> list = this.loginAttemptRepository
                .findAll()
                .stream()
                .filter(entry -> {
                    final String attemptObject = entry.getObject();
                    return attemptObject != null && attemptObject.equals(ipAddress);
                })
                .toList();

        for (final LoginAttempt loginAttempt : list) {
            if (loginAttempt.getCounter() >= MAX_LOGIN_ATTEMPT) {
                log.error("IP-Address blocked! {}", loginAttempt.getObject());
                if (!loginAttempt.getNotified()) {
                    this.mailService.sendAdminNotification(
                            "IP Address blocked!",
                            "Permanent blocked: " + ipAddress);
                    loginAttempt.setNotified(true);
                    this.loginAttemptRepository.save(loginAttempt);
                }
                return true;
            }
        }

        return false;
    }

    public List<LoginAttempt> getAllLoginAttempts() {
        return this.loginAttemptRepository.findAll();
    }

    public void deleteOrUpdate(final LoginAttempt loginAttempt) {
        final LoginAttempt object = this.loginAttemptRepository
                .findByObject(loginAttempt.getObject())
                .orElseThrow();

        if (object.getCounter() == 1) {
            log.info("Delete IP {}", object.getObject());
            this.loginAttemptRepository.delete(object);
            return;
        }
        object.setCounter(object.getCounter() - 1);
        log.info("Reduce counter for IP {}. New value: {}", object.getObject(), object.getCounter());
        this.loginAttemptRepository.save(object);
    }

    public void cleanTable() {
        this.loginAttemptRepository.deleteAll();
    }

    private void createNewEntity(final String value) {
        this.loginAttemptRepository.save(LoginAttempt.builder()
                .counter(1)
                .object(value)
                .datetime(LocalDateTime.now())
                .notified(false)
                .build());
    }
}
