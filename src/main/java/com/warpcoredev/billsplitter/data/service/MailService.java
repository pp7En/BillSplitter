package com.warpcoredev.billsplitter.data.service;

import com.vaadin.flow.i18n.DefaultI18NProvider;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.BSNotification;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.NotificationType;
import com.warpcoredev.billsplitter.data.entity.Payment;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.utils.HelperClass;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedConsts;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import static com.warpcoredev.billsplitter.utils.SharedConsts.COLON_SPACE;
import static com.warpcoredev.billsplitter.utils.SharedConsts.SPACE;
import static java.nio.charset.StandardCharsets.UTF_8;

@Service
@Slf4j
public class MailService implements Serializable {

    @Value ("${service.mailserver.ssl}")
    private Boolean mailserverUseSSL;

    @Value ("${service.url}")
    private String applicationUrl;

    @Value ("${service.mailserver.admin:admin@example.com}")
    private String admin;

    @Value ("${service.mailserver.sender}")
    private String sender;

    @Value ("${service.mailserver.hostname}")
    private String hostname;

    @Value ("${service.mailserver.port}")
    private int port;

    @Value ("${service.mailserver.username}")
    private String username;

    @Value ("${service.mailserver.password}")
    private String password;

    @Value ("${service.envName:dev}")
    private String envName;

    @Value("${service.mailserver.language}")
    private Locale locale;

    @Value("classpath:templates/registration.html")
    private transient Resource mailTemplateRegistration;

    @Value("classpath:templates/new-bill.html")
    private transient Resource mailTemplateNewBill;

    @Value("classpath:templates/restore-password.html")
    private transient Resource mailTemplateRestorePassword;

    private final transient DateTimeFormatter formatter = DateTimeFormatter.ofPattern(
            "dd. MMMM yyyy HH:mm:ss",
            Locale.GERMANY);
    private final DefaultI18NProvider billSplitterI18nProvider
            = new DefaultI18NProvider(List.of(Locale.ENGLISH, Locale.GERMAN));

    @SuppressWarnings("checkstyle:MultipleStringLiterals")
    public void sendRegistrationMail(final User user) {
        final String queryParameter = HelperClass.createEncodedRestorePasswordLink(user);
        final String signupLink = this.applicationUrl + "/login?q=" + queryParameter;

        final String modifiedSubject = this.getI18nProperty(I18NKeys.EMAIL_SUBJECT_SIGNUP);

        final String mail = Objects.requireNonNull(this.getMailTemplate(this.mailTemplateRegistration))

                .replace("%SALUTATION%",
                        this.getI18nProperty(I18NKeys.EMAIL_SALUTATION))

                .replace("%USERNAME%",
                        user.getName())

                .replace("%MESSAGE%",
                        this.getI18nProperty(I18NKeys.EMAIL_SIGNUP_MESSAGE))

                .replace("%URL%",
                        this.applicationUrl)

                .replace("%SIGNUP_LINK%",
                        signupLink)

                .replace("%APPLICATION_NAME%",
                        this.getI18nProperty(I18NKeys.APP_TITLE));

        this.sendMail(user.getEmail(), modifiedSubject, mail);
    }

    @SuppressWarnings("checkstyle:MultipleStringLiterals")
    public void sendNotificationMail(final Payment payment, final BigDecimal balance) {
        final String modifiedSubject = this.getI18nProperty(I18NKeys.EMAIL_SUBJECT_NEW_BILL);

        final String mail = Objects.requireNonNull(this.getMailTemplate(this.mailTemplateNewBill))
                .replace("%SALUTATION%",
                        this.getI18nProperty(I18NKeys.EMAIL_SALUTATION))

                .replace("%USERNAME%",
                        payment.getUser().getName())

                .replace("%MESSAGE%",
                        this.getI18nProperty(I18NKeys.EMAIL_NEW_BILL_MESSAGE))

                .replace("%GROUP%",
                        String.join(COLON_SPACE,
                                this.getI18nProperty(I18NKeys.EMAIL_NEW_BILL_GROUP),
                                payment.getGroup().getName()))

                .replace("%DESC%",
                        String.join(COLON_SPACE,
                                this.getI18nProperty(I18NKeys.EMAIL_NEW_BILL_DESC),
                                payment.getBill().getDescription()))

                .replace("%TOTAL%",
                        String.join(COLON_SPACE,
                                this.getI18nProperty(I18NKeys.EMAIL_NEW_BILL_TOTAL),
                                this.getI18nCurrency(payment.getBill().getTotalPayable())))

                .replace("%YOUR_PART%",
                        String.join(COLON_SPACE,
                                this.getI18nProperty(I18NKeys.EMAIL_NEW_BILL_YOUR_PART),
                                this.getI18nCurrency(payment.getHaveToPay())))

                .replace("%ACCOUNT_BALANCE%",
                        String.join(COLON_SPACE,
                                this.getI18nProperty(I18NKeys.EMAIL_NEW_BILL_ACCOUNT_BALANCE),
                                this.getI18nCurrency(balance)))

                .replace("%APPLICATION_NAME%",
                        this.getI18nProperty(I18NKeys.APP_TITLE));

        this.sendMail(payment.getUser().getEmail(), modifiedSubject, mail);
    }

    @SuppressWarnings("checkstyle:MultipleStringLiterals")
    public void sendPasswordResetMail(final User user) {
        final String queryParameter = HelperClass.createEncodedRestorePasswordLink(user);
        final String link = this.applicationUrl + "/restore?q=" + queryParameter;

        final LocalDateTime dateTime = user.getValidationCodeValidUntil();
        final String validUntil = String.join(SPACE,
                this.getI18nProperty(I18NKeys.EMAIL_RESTORE_PASSWORD_VALID_UNTIL),
                dateTime.format(this.formatter));

        final String mail = Objects.requireNonNull(this.getMailTemplate(this.mailTemplateRestorePassword))
                .replace("%SALUTATION%",
                        this.getI18nProperty(I18NKeys.EMAIL_SALUTATION))

                .replace("%USERNAME%",
                        user.getName())

                .replace("%MESSAGE%",
                        this.getI18nProperty(I18NKeys.EMAIL_RESTORE_PASSWORD_DESC))

                .replace("%LINK%",
                        link)

                .replace("%VALID_UNTIL%",
                        validUntil)

                .replace("%APPLICATION_NAME%",
                        this.getI18nProperty(I18NKeys.APP_TITLE));

        this.sendMail(user.getEmail(), this.getI18nProperty(I18NKeys.EMAIL_RESTORE_PASSWORD_HEADER), mail);
    }

    public void sendAdminNotification(final String subject, final String message) {
        this.sendMail(this.admin, subject, message);
    }

    @SuppressWarnings("checkstyle:EqualsAvoidNull")
    private void sendMail(final String recipient, final String subject, final String message) {
        if (!this.envName.equals(SharedConsts.MODE_PRODUCTION)) {
            log.warn("Send email notifications are disabled, because application not in production mode!");
            log.info("recipient={} subject={} message={}",
                    recipient,
                    subject,
                    message.replace("\n", " ").replace("\r", ""));

            return;
        }

        try {
            final SimpleEmail email = this.getEmail(recipient, subject, message);
            email.send();
        } catch (final EmailException ex) {
            log.error("Send mail error: {}", ex.getMessage());
            BSNotification.show(ex.getLocalizedMessage(), NotificationType.ERROR);
        }
    }

    private SimpleEmail getEmail(final String recipient,
                                 final String subject,
                                 final String message) throws EmailException {

        final SimpleEmail email = new SimpleEmail();
        email.setHostName(this.hostname);
        email.setSmtpPort(this.port);

        if (this.mailserverUseSSL) {
            email.setSSLOnConnect(true);
            email.setSSLCheckServerIdentity(true);
        }

        email.setAuthentication(this.username, this.password);
        email.setFrom(this.sender);

        email.addTo(recipient);
        email.setSubject(subject);
        email.setMsg(message);
        return email;
    }

    private String getMailTemplate(final Resource resource) {
        //noinspection LocalCanBeFinal
        try (Reader reader = new InputStreamReader(resource.getInputStream(), UTF_8)) {
            return FileCopyUtils.copyToString(reader);
        } catch (final IOException ex) {
            log.error("Error while parsing html template: {}", ex.getMessage());
            BSNotification.show(ex.getLocalizedMessage(), NotificationType.ERROR);
            return null;
        }
    }

    private String getI18nProperty(final String key) {
        return this.billSplitterI18nProvider.getTranslation(key, this.locale);
    }

    private String getI18nCurrency(final BigDecimal money) {
        final DecimalFormat currencyFormatter = HelperClass.currencyFormat(
            this.getI18nProperty(I18NKeys.CURRENCY_FORMAT),
            this.getI18nProperty(I18NKeys.CURRENCY_DECIMAL_SEPARATOR),
            this.getI18nProperty(I18NKeys.CURRENCY_GROUPING_SEPARATOR));

        return currencyFormatter.format(money);
    }
}
