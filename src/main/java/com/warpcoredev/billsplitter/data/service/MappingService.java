package com.warpcoredev.billsplitter.data.service;

import com.warpcoredev.billsplitter.data.Status;
import com.warpcoredev.billsplitter.data.entity.Mapping;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.data.repository.MappingRepository;

import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;

import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MappingService implements Serializable {

    private final MappingRepository mappingRepository;

    public void createMappingEntryIfNotExist(final Mapping mapping) {
        final Mapping mappingByIds = this.mappingRepository.getMapping(mapping.getUid(), mapping.getGid());
        if (mappingByIds == null) {
            this.mappingRepository.save(mapping);
        }
    }

    public void allowRequest(final Mapping mapping) {
        mapping.setStatus(Status.CONFIRMED);
        this.mappingRepository.save(mapping);
    }

    public void rejectRequest(final Mapping mapping) {
        mapping.setStatus(Status.REJECTED);
        this.mappingRepository.save(mapping);
    }

    public List<Mapping> getRejectedGroupJoinRequests(final User user) {
        return this.mappingRepository.getStatusGroupJoinRequests(user, Status.REJECTED);
    }

    public List<Mapping> getAwaitingGroupJoinRequests(final User user) {
        return this.mappingRepository.getStatusGroupJoinRequests(user, Status.REQUESTED);
    }

    public void saveEntity(final Mapping mapping) {
        this.mappingRepository.save(mapping);
    }

    public void removeMapping(final Mapping mapping) {
        final Mapping data = this.mappingRepository.getMapping(mapping.getUid(), mapping.getGid());
        this.mappingRepository.delete(data);
    }
}
