package com.warpcoredev.billsplitter.data.service;

import com.warpcoredev.billsplitter.data.PaymentStatus;
import com.warpcoredev.billsplitter.data.Status;
import com.warpcoredev.billsplitter.data.dto.GroupMemberDto;
import com.warpcoredev.billsplitter.data.dto.HistoryDto;
import com.warpcoredev.billsplitter.data.entity.Bill;
import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.entity.Payment;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.data.repository.BillRepository;
import com.warpcoredev.billsplitter.data.repository.PaymentRepository;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;

@Service
@Slf4j
@AllArgsConstructor
public class PaymentService implements Serializable {

    private final PaymentRepository paymentRepository;
    private final BillRepository billRepository;
    private final MailService mailService;
    private final GroupService groupService;

    public void savePayments(final Set<Payment> listPayments, final User authenticatedUser) {
        final Bill bill = listPayments.stream().findFirst().orElseThrow().getBill();

        bill.setTotalPayable(
                listPayments.stream()
                        .map(Payment::getHaveToPay)
                        .reduce(BigDecimal.ZERO, BigDecimal::add)
        );

        final Bill savedBill = this.billRepository.save(bill);

        final List<Payment> copy = new ArrayList<>();
        final List<Payment> paymentsForMail = new ArrayList<>();

        listPayments.forEach(entry -> {
            final Payment p = Payment.builder()
                    .user(entry.getUser())
                    .group(entry.getGroup())
                    .bill(savedBill)
                    .haveToPay(entry.getHaveToPay())
                    .paid(entry.getPaid())
                    .balance(entry.getBalance())
                    .paidDay(entry.getPaidDay())
                    .status(entry.getStatus())
                    .build();

            // Send notification if the user's notifications are enabled and they are not the authenticated user
            final Status notifications = entry.getUser().getNotifications();
            if (notifications != null
                    && notifications.equals(Status.ENABLED)
                    && !authenticatedUser.equals(entry.getUser())) {
                paymentsForMail.add(entry);
            }

            copy.add(p);
        });

        final boolean isCollectorInList = copy.stream()
                .anyMatch(payment -> payment.getUser().equals(savedBill.getCollector()));
        if (!isCollectorInList) {
            final Payment collectorPayment = Payment.builder()
                    .user(bill.getCollector())
                    .group(bill.getGroup())
                    .bill(savedBill)
                    .haveToPay(BigDecimal.ZERO)
                    .paid(bill.getTotalPayable())
                    .balance(bill.getTotalPayable())
                    .paidDay(bill.getPaidDate())
                    .status(PaymentStatus.CONFIRMED)
                    .build();
            copy.add(collectorPayment);
        }

        // Save all payments
        this.paymentRepository.saveAll(copy);
        if (!paymentsForMail.isEmpty()) {
            this.sendMailNotifications(paymentsForMail);
        }
    }

    public void savePayments(final List<Payment> listPayments) {
        final Bill bill = listPayments.stream().findFirst().orElseThrow().getBill();
        final Bill savedBill = this.billRepository.save(bill);

        final List<Payment> copyPaymentList = new ArrayList<>();

        listPayments.forEach(entry -> {
            final Payment p = Payment.builder()
                    .user(entry.getUser())
                    .group(entry.getGroup())
                    .bill(savedBill)
                    .haveToPay(entry.getHaveToPay())
                    .paid(entry.getPaid())
                    .balance(entry.getBalance())
                    .paidDay(entry.getPaidDay())
                    .status(entry.getStatus())
                    .build();

            copyPaymentList.add(p);
        });
        this.paymentRepository.saveAll(copyPaymentList);
    }

    private void sendMailNotifications(final List<Payment> payments) {
        final Group group = payments.getFirst().getGroup();

        payments.forEach(entry -> {
            final BigDecimal balance = this.paymentRepository.getPaymentsByUser(entry.getUser(), group).getBalance();
            this.mailService.sendNotificationMail(entry, balance);
        });
    }

    public boolean isAccountBalanced(final User user) {
        final Double userBalance = this.paymentRepository.getUserBalance(user);
        final boolean isAccountBalanced = userBalance == null || userBalance == 0;

        log.debug("User balance: {}", userBalance);
        return isAccountBalanced;
    }

    public boolean isAccountBalanced(final User user, final Group group) {
        final Double userBalance = this.paymentRepository.getUserBalance(user, group);

        final boolean isAccountBalanced = userBalance == null || userBalance == 0;

        log.debug("User balance: {} (Group={})", userBalance, group.getId());
        return isAccountBalanced;
    }

    public void deleteEntity(final List<HistoryDto> deleteList) {
        deleteList.forEach(item -> item.getPayments().forEach(this.paymentRepository::delete));
    }

    public List<GroupMemberDto> getPaymentsForUsers(final List<User> listUser, final Group group) {
        final List<GroupMemberDto> dto = new ArrayList<>();

        listUser.forEach(user -> dto.add(this.paymentRepository.getPaymentsByUser(user, group)));

        return dto;
    }
}
