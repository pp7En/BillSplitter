package com.warpcoredev.billsplitter.data.service;

import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.exceptions.PdfException;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.properties.BorderCollapsePropertyValue;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.UnitValue;
import com.vaadin.flow.i18n.I18NProvider;
import com.vaadin.flow.server.StreamResource;
import com.warpcoredev.billsplitter.data.entity.Bill;
import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.entity.Payment;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.utils.HelperClass;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedConsts;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.stereotype.Service;

import static com.warpcoredev.billsplitter.data.PaymentStatus.SINGLE_PAYMENT;
import static com.warpcoredev.billsplitter.utils.I18NKeys.PDF_FILENAME_PREFIX;
import static com.warpcoredev.billsplitter.utils.I18NKeys.PDF_HEADER;
import static com.warpcoredev.billsplitter.utils.I18NKeys.PDF_HEADER_SUB;
import static com.warpcoredev.billsplitter.utils.SharedConsts.EMPTY_SPACE;
import static com.warpcoredev.billsplitter.utils.SharedConsts.LINE_BREAK;
import static com.warpcoredev.billsplitter.utils.SharedConsts.PLACEHOLDER_DATE;
import static com.warpcoredev.billsplitter.utils.SharedConsts.PLACEHOLDER_GROUP;
import static com.warpcoredev.billsplitter.utils.SharedConsts.PLACEHOLDER_PDF_PREFIX;
import static com.warpcoredev.billsplitter.utils.SharedConsts.UNDERSCORE;

@Service
public class PdfService implements Serializable {

    private static final DeviceRgb BACKGROUND_COLOR_GREEN = new DeviceRgb(221, 255, 230);
    private static final DeviceRgb BACKGROUND_COLOR = new DeviceRgb(255, 255, 255);
    private static final DeviceRgb FONT_COLOR_NEGATIVE = new DeviceRgb(255, 0, 0);
    private static final DeviceRgb FONT_COLOR_POSITIVE = new DeviceRgb(0, 128, 0);
    private static final DeviceRgb FONT_COLOR_DEFAULT = new DeviceRgb(0, 0, 0);

    private static final float WIDTH_DATE = 75f;
    private static final float WIDTH_DESC = 150f;
    private static final float WIDTH_COLLECTOR = 75f;
    private static final float WIDTH_SUM = 55f;
    private static final float WIDTH_USER_PAYMENT = 50f;
    private static final float DESC_COLUMN_WIDTH = 10f;

    private static final int PADDING_LEFT = 5;
    private static final int PADDING_RIGHT = 5;
    private static final int MARGIN = 10;

    private static final Color HEADER_FOOTER_BORDER_COLOR = new DeviceRgb(73, 169, 81);
    private static final float HEADER_FOOTER_BORDER_WIDTH = 2f;
    private static final int HEADER_FONT_SIZE = 14;
    private static final int HEADER_SUB_FONT_SIZE = 12;

    private static final String PDF_FILE_NAME = String.join(UNDERSCORE,
            PLACEHOLDER_PDF_PREFIX, PLACEHOLDER_GROUP, PLACEHOLDER_DATE);
    private static final int NUMBERS_OF_BASE_COLUMNS = 4;

    private final BillService billService;
    private final GroupService groupService;
    private final I18NProvider i18nProvider;

    private transient PdfFont boldFont;
    private transient Table table;
    private Map<User, BigDecimal> list;
    private DecimalFormat currencyFormatter;
    private Locale currentLocale;
    private BigDecimal totalAmount;

    public PdfService(final BillService billService,
                      final GroupService groupService,
                      final I18NProvider i18nProvider) {
        this.billService = billService;
        this.groupService = groupService;
        this.i18nProvider = i18nProvider;
    }

    public StreamResource getPdf(final Locale locale, final Group group) {
        this.list = new LinkedHashMap<>();
        this.totalAmount = BigDecimal.ZERO;

        this.currentLocale = locale;
        this.currencyFormatter = HelperClass.currencyFormat(
                SharedConsts.CURRENCY_FORMAT,
                this.getTranslation(I18NKeys.CURRENCY_DECIMAL_SEPARATOR),
                this.getTranslation(I18NKeys.CURRENCY_GROUPING_SEPARATOR));

        final List<Bill> bills = this.billService.getBillsByGroup(group);
        if (bills.isEmpty()) {
            return null;
        }

        bills.sort(Comparator.comparing(Bill::getPaidDate));
        final List<User> groupMember = this.groupService.getGroupMember(group);

        return this.createPdfStreamResource(bills, groupMember);
    }

    private StreamResource createPdfStreamResource(final List<Bill> bills,
                                                   final List<User> groupMember) {
        final String filename = PDF_FILE_NAME
                .replace(PLACEHOLDER_PDF_PREFIX, this.getTranslation(PDF_FILENAME_PREFIX))
                .replace(PLACEHOLDER_GROUP, bills.getFirst().getGroup().getName())
                .replace(PLACEHOLDER_DATE, LocalDateTime.now().format(SharedConsts.DATE_FORMATTER_HUMAN_READABLE))
                + ".pdf";

        try {
            this.boldFont = PdfFontFactory.createFont(StandardFonts.HELVETICA_BOLD);
        } catch (final IOException ex) {
            throw new PdfException("Can not create StandardFonts.HELVETICA_BOLD");
        }

        return new StreamResource(filename, () -> {
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            final PdfWriter writer = new PdfWriter(baos);
            final PdfDocument pdfDoc = new PdfDocument(writer);
            pdfDoc.setDefaultPageSize(PageSize.A4.rotate());

            final Document document = new Document(pdfDoc);
            document.setMargins(MARGIN, MARGIN, MARGIN, MARGIN);

            // Add title
            final Paragraph title = new Paragraph(this.getTranslation(PDF_HEADER))
                    .setFontSize(HEADER_FONT_SIZE)
                    .setFont(this.boldFont)
                    .setTextAlignment(TextAlignment.CENTER);
            document.add(title);

            // Add date
            final Paragraph date = new Paragraph(
                        this.getTranslation(PDF_HEADER_SUB)
                                .replace(PLACEHOLDER_DATE,
                                        LocalDate.now().format(SharedConsts.DATE_FORMATTER_HUMAN_READABLE)))
                    .setFontSize(HEADER_SUB_FONT_SIZE)
                    .setTextAlignment(TextAlignment.CENTER);
            document.add(date);

            // Empty line
            document.add(new Paragraph(LINE_BREAK));

            // Determining number of columns
            final int userColumnCount = groupMember.size();
            final int columns = NUMBERS_OF_BASE_COLUMNS + userColumnCount;
            final float[] columnWidths = new float[columns];

            // Set the column widths
            for (int i = 0; i < columnWidths.length; i++) {
                if (i == 1) {
                    columnWidths[i] = DESC_COLUMN_WIDTH;
                } else {
                    columnWidths[i] = i < NUMBERS_OF_BASE_COLUMNS ? 2f : 1f;
                }
            }

            // Create table
            this.table = new Table(UnitValue.createPercentArray(columnWidths));
            this.table.setBorderCollapse(BorderCollapsePropertyValue.SEPARATE);

            // Add column headers
            this.addHeaderData(groupMember, userColumnCount);

            // Add invoice data and payments
            for (final Bill bill : bills) {
                this.addBillData(groupMember, bill);
            }

            // Last line with summary
            this.addSummery();

            // Remove all unnecessary borders
            this.setBorders();

            // Add table to document and finalize
            document.add(this.table);
            document.close();

            return new ByteArrayInputStream(baos.toByteArray());
        });
    }

    private void addSummery() {
        this.table.addCell(this.createCell(EMPTY_SPACE, WIDTH_DATE).setBorder(Border.NO_BORDER));
        this.table.addCell(this.createCell(EMPTY_SPACE, WIDTH_DESC).setBorder(Border.NO_BORDER));
        this.table.addCell(this.createCell(EMPTY_SPACE, WIDTH_COLLECTOR).setBorder(Border.NO_BORDER));

        this.table.addCell(
                this.createCell(this.currencyFormatter.format(this.totalAmount),WIDTH_SUM)
                        .setBorder(Border.NO_BORDER)
                        .setTextAlignment(TextAlignment.RIGHT)
        );

        for (final Map.Entry<User, BigDecimal> entry : this.list.entrySet()) {
            final DeviceRgb color;
            if (entry.getValue().compareTo(BigDecimal.ZERO) < 0) {
                color = FONT_COLOR_NEGATIVE;
            } else if (entry.getValue().compareTo(BigDecimal.ZERO) > 0) {
                color = FONT_COLOR_POSITIVE;
            } else {
                color = FONT_COLOR_DEFAULT;
            }
            this.table.addCell(
                    this.createMemberCell(this.currencyFormatter.format(entry.getValue()))
                            .setTextAlignment(TextAlignment.RIGHT)
                            .setBorder(Border.NO_BORDER)
                            .setFontColor(color));
        }
    }

    private void setBorders() {
        for (int i = 0; i < this.table.getNumberOfRows(); i++) {
            for (int j = 0; j < this.table.getNumberOfColumns(); j++) {
                final Cell cell = this.table.getCell(i, j);
                cell.setBorderLeft(Border.NO_BORDER);
                cell.setBorderRight(Border.NO_BORDER);

                if (i == 0) {
                    cell.setBorderTop(new SolidBorder(HEADER_FOOTER_BORDER_COLOR, HEADER_FOOTER_BORDER_WIDTH));
                }
                if (i == this.table.getNumberOfRows() - 1) {
                    cell.setBorder(Border.NO_BORDER);
                }

                if (i == this.table.getNumberOfRows() - 2) {
                    cell.setBorderBottom(new SolidBorder(HEADER_FOOTER_BORDER_COLOR, HEADER_FOOTER_BORDER_WIDTH));
                }
            }
        }
    }

    private void addHeaderData(final List<User> groupMember, final int userColumnCount) {
        this.table.addHeaderCell(this.createCell(this.getTranslation(I18NKeys.HISTORY_DATE),WIDTH_DATE)
                        .setFont(this.boldFont)
                        .setBorder(Border.NO_BORDER))
                .setTextAlignment(TextAlignment.LEFT)
                .setMinWidth(WIDTH_DATE);
        this.table.addHeaderCell(this.createCell(this.getTranslation(I18NKeys.HISTORY_DESC),WIDTH_DESC)
                        .setFont(this.boldFont)
                        .setBorder(Border.NO_BORDER)
                        .setTextAlignment(TextAlignment.LEFT))
                .setMinWidth(WIDTH_DESC);
        this.table.addHeaderCell(this.createCell(this.getTranslation(I18NKeys.HISTORY_PAID_BY),WIDTH_COLLECTOR)
                        .setFont(this.boldFont)
                        .setBorder(Border.NO_BORDER)
                        .setTextAlignment(TextAlignment.LEFT))
                .setMinWidth(WIDTH_COLLECTOR);
        this.table.addHeaderCell(this.createCell(this.getTranslation(I18NKeys.HISTORY_TOTAL), WIDTH_SUM)
                        .setFont(this.boldFont)
                        .setBorder(Border.NO_BORDER)
                        .setTextAlignment(TextAlignment.RIGHT))
                .setMinWidth(WIDTH_SUM);

        if (userColumnCount > 0) {
            // Add the user column headers
            for (int i = 0; i < userColumnCount; i++) {
                // Header for each user column
                this.table.addHeaderCell(new Cell(2, 1)
                        .add(new Paragraph(groupMember.get(i).getName()))
                        .setFont(this.boldFont)
                        .setBorder(Border.NO_BORDER)
                        .setBackgroundColor(BACKGROUND_COLOR_GREEN)
                        .setTextAlignment(TextAlignment.RIGHT)
                        .setPaddingLeft(PADDING_LEFT)
                        .setPaddingRight(PADDING_RIGHT));
            }
        }
    }

    private void addBillData(final List<User> groupMember, final Bill bill) {
        final String description;
        if (bill.getDescription().equals(SINGLE_PAYMENT.name())) {
            description = this.getTranslation(I18NKeys.SINGLE_PAYMENT_PAYMENT_DETAILS);
        } else {
            description = bill.getDescription();
        }

        this.table.addCell(this.createCell(
                bill.getPaidDate().format(SharedConsts.DATE_FORMATTER_HUMAN_READABLE),
                WIDTH_DATE));
        this.table.addCell(this.createCell(
                description,
                WIDTH_DESC));
        this.table.addCell(this.createCell(
                bill.getCollector().getName(),
                WIDTH_COLLECTOR));
        this.table.addCell(this.createCell(
                        this.currencyFormatter.format(bill.getTotalPayable()),
                        WIDTH_SUM)
                .setTextAlignment(TextAlignment.RIGHT));

        this.totalAmount = this.totalAmount.add(bill.getTotalPayable());

        if (groupMember.size() == 1) {
            final BigDecimal haveToPay = bill.getTotalPayable().negate();
            final Cell haveToPayCell = this.createMemberCell(this.currencyFormatter.format(haveToPay))
                    .setTextAlignment(TextAlignment.RIGHT)
                    .setFontColor(FONT_COLOR_NEGATIVE)
                    .setBackgroundColor(BACKGROUND_COLOR_GREEN);
            this.table.addCell(haveToPayCell);
            this.list.merge(bill.getCollector(), haveToPay, BigDecimal::add);
            return;
        }

        for (final User user : groupMember) {
            this.addPaymentData(bill, user);
        }
    }

    private void addPaymentData(final Bill bill, final User user) {
        // Find payment object for this group member
        Payment memberPayment = null;
        for (final Payment payment : bill.getPayments()) {
            if (payment.getUser().equals(user)) {
                memberPayment = payment;
                break;
            }
        }
        BigDecimal haveToPay = BigDecimal.ZERO;
        // Show the amount that the group member has to pay
        if (memberPayment != null && memberPayment.getBalance().compareTo(BigDecimal.ZERO) > 0) {
            haveToPay = memberPayment.getBalance();
            final Cell balance = this.createMemberCell(this.currencyFormatter.format(haveToPay))
                    .setTextAlignment(TextAlignment.RIGHT)
                    .setFontColor(FONT_COLOR_POSITIVE)
                    .setBackgroundColor(BACKGROUND_COLOR_GREEN);
            this.table.addCell(balance);
        } else if (memberPayment != null && memberPayment.getBalance().compareTo(BigDecimal.ZERO) < 0) {
            haveToPay = memberPayment.getBalance();
            final Cell haveToPayCell = this.createMemberCell(this.currencyFormatter.format(haveToPay))
                    .setTextAlignment(TextAlignment.RIGHT)
                    .setFontColor(FONT_COLOR_NEGATIVE)
                    .setBackgroundColor(BACKGROUND_COLOR_GREEN);
            this.table.addCell(haveToPayCell);
        } else {
            // Empty cell if no payment exists or the amount is 0
            this.table.addCell(this.createMemberCell(EMPTY_SPACE))
                    .setBackgroundColor(BACKGROUND_COLOR_GREEN);
        }
        this.list.merge(user, haveToPay, BigDecimal::add);
    }

    private Cell createCell(final String text, final float width) {
        return new Cell().add(new Paragraph(text).setWidth(width))
                .setPaddingLeft(PADDING_LEFT)
                .setPaddingRight(PADDING_RIGHT)
                .setBackgroundColor(BACKGROUND_COLOR);
    }

    private Cell createMemberCell(final String text) {
        return new Cell().add(new Paragraph(text).setWidth(WIDTH_USER_PAYMENT))
                .setPaddingLeft(PADDING_LEFT)
                .setPaddingRight(PADDING_RIGHT)
                .setBackgroundColor(BACKGROUND_COLOR_GREEN);
    }

    private String getTranslation(final String key) {
        assert this.i18nProvider != null;
        return this.i18nProvider.getTranslation(key, this.currentLocale);
    }

}
