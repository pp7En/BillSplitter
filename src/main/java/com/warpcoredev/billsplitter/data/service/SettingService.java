package com.warpcoredev.billsplitter.data.service;

import com.warpcoredev.billsplitter.data.Status;
import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.entity.Setting;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.data.repository.GroupRepository;
import com.warpcoredev.billsplitter.data.repository.SettingRepository;

import java.io.Serializable;
import java.util.Optional;
import lombok.AllArgsConstructor;

import org.springframework.stereotype.Service;

import static com.warpcoredev.billsplitter.utils.SharedConsts.PUBLIC_REGISTRATION;

@Service
@AllArgsConstructor
public class SettingService implements Serializable {

    private final SettingRepository settingRepository;
    private final GroupRepository groupRepository;

    public Setting getValue(final String type) {
        return this.settingRepository.getValue(type);
    }

    public String getValueByKey(final String type) {
        final Setting setting = this.settingRepository.getValue(type);
        final String value;
        if (setting == null) {
            value = "";
        } else {
            value = setting.getValue();
        }
        return value;
    }

    public void saveValue(final Setting setting) {
        this.settingRepository.save(setting);
    }

    public Group getLastSelectedGroup(final User user) {
        final String searchString = "LAST_GROUP_FOR_USERID_" + user.getId();

        final Setting setting = this.settingRepository.getValue(searchString);
        if (setting == null) {
            return null;
        }

        final Optional<Group> group = this.groupRepository.getGroupById(
                user,
                Status.CONFIRMED,
                Long.valueOf(setting.getValue()));

        return group.orElse(null);
    }

    public void setLastSelectedGroup(final User user, final Group group) {
        final String searchString = "LAST_GROUP_FOR_USERID_" + user.getId();

        Setting value = this.getValue(searchString);
        if (value == null) {
            value = Setting.builder()
                    .name(searchString)
                    .value(String.valueOf(group.getId()))
                    .build();
        }
        value.setValue(String.valueOf(group.getId()));

        this.settingRepository.save(value);
    }

    public void setLastSelectedGroup(final Setting setting) {
        this.settingRepository.save(setting);
    }

    public boolean publicRegistrationsEnabled() {
        final Setting setting = this.settingRepository.getValue(PUBLIC_REGISTRATION);
        return setting != null && !setting.getValue().equals("0");
    }

    public void setPublicRegistrations(final boolean value) {
        Setting setting = this.settingRepository.getValue(PUBLIC_REGISTRATION);

        if (setting == null) {
            setting = Setting.builder().name(PUBLIC_REGISTRATION).build();
        }

        setting.setValue(value ? "1" : "0");
        this.settingRepository.save(setting);
    }
}
