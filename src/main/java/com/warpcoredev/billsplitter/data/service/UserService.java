package com.warpcoredev.billsplitter.data.service;

import com.warpcoredev.billsplitter.data.Role;
import com.warpcoredev.billsplitter.data.Status;
import com.warpcoredev.billsplitter.data.dto.GuestUserDto;
import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.data.repository.UserRepository;
import com.warpcoredev.billsplitter.utils.HelperClass;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import static com.warpcoredev.billsplitter.utils.SharedConsts.VALIDATION_CODE_VALID;

@Service
@Slf4j
public class UserService implements Serializable {

    private final UserRepository userRepository;

    public UserService(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Optional<User> get(final Long id) {
        return this.userRepository.findById(id);
    }

    public User update(final User entity) {
        return this.userRepository.save(entity);
    }

    public void deleteById(final Long id) {
        this.userRepository.deleteById(id);
    }

    public Page<User> list(final Pageable pageable, final Specification<User> filter) {
        return this.userRepository.findAll(filter, pageable);
    }

    public void createUser(final User user) {
        this.userRepository.save(user);
    }

    public boolean checkCurrentPassword(final User authenticatedUser, final String string) {
        final User user = this.userRepository.optFindByEmail(authenticatedUser.getEmail()).orElseThrow();

        final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder.matches(string, user.getHashedPassword());
    }

    public void setNewPassword(final User authenticatedUser, final String string) {
        final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        final String encode = bCryptPasswordEncoder.encode(string);

        authenticatedUser.setHashedPassword(encode);

        this.userRepository.save(authenticatedUser);
    }

    public User saveEntity(final User user) {
        return this.userRepository.save(user);
    }

    public boolean emailAlreadyInUse(final String mail) {
        final Optional<User> optionalUser = this.userRepository.optFindByEmail(mail);
        if (optionalUser.isPresent()) {
            log.debug("Email address already in use ({})", mail);
        }

        return optionalUser.isPresent();
    }

    public Optional<GuestUserDto> existGuestAccount(final String group, final String token) {
        return this.userRepository.findGuestAccount(group, token, Role.FOR_PUBLIC_SHARE, Status.CONFIRMED);
    }

    public Optional<User> validateValidationCode(final String email, final String code) {
        return this.userRepository.validateValidationCode(email, code);
    }

    public User findGuestUser(final Group group) {
        Optional<User> optGuestAccount = this.userRepository.findGuestAccount(group.getId(), Role.FOR_PUBLIC_SHARE);
        if (optGuestAccount.isEmpty()) {
            log.info("Create new guest user");
            final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
            final String encode = bCryptPasswordEncoder.encode(group.getCode());
            final String email = "guest"
                                 + group.getId()
                                 + "@billsplitter.org";
            final User user = User.builder()
                    .name("ghost" + group.getId())
                    .email(email)
                    .lastLogin(HelperClass.getUnixtime())
                    .enabled(Status.CONFIRMED)
                    .hashedPassword(encode)
                    .notifications(Status.DISABLED)
                    .role(Role.FOR_PUBLIC_SHARE)
                    .build();
            this.userRepository.save(user);
            optGuestAccount = Optional.of(user);
        }
        return optGuestAccount.get();
    }

    public void disableGuestUserPassword(final User user) {
        user.setEnabled(Status.DISABLED);
        user.setHashedPassword(null);

        this.userRepository.save(user);
    }

    public void delete(final User user) {
        this.userRepository.delete(user);
    }

    public void deleteEntity(final List<User> deleteList) {
        deleteList.forEach(this.userRepository::delete);
    }

    public void disableUsers(final List<User> deleteList) {
        deleteList.forEach(item -> {
            item.setEnabled(Status.DISABLED);
            this.userRepository.save(item);
        });
    }

    public User findUserByMailAddress(final String mail) {
        return this.userRepository.optFindByEmail(mail).orElseThrow();
    }

    public Optional<User> optUserRestorePassword(final String mail) throws NoSuchAlgorithmException {
        final Optional<User> optUser = this.userRepository.optFindByEmail(mail);
        if (optUser.isPresent()) {
            final User user = optUser.get();
            final String randomCode = HelperClass.getRandomCode(50);

            user.setValidationCode(randomCode);
            user.setValidationCodeValidUntil(
                    LocalDateTime.now()
                            .plusDays(VALIDATION_CODE_VALID));

            this.userRepository.save(user);
        }
        return optUser;
    }

    public List<User> getUsersWithoutFirstLoginIn24Hours() {
        final List<User> users = this.userRepository.findNonActivatedUserOlderThan();
        log.info("Delete {} unactivated accounts", users.size());
        return users;
    }
}
