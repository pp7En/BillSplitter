package com.warpcoredev.billsplitter.security;

import com.vaadin.flow.server.VaadinServletRequest;
import com.vaadin.flow.server.VaadinServletResponse;
import com.vaadin.flow.spring.security.AuthenticationContext;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.data.repository.UserRepository;

import java.io.Serializable;
import java.util.Optional;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AuthenticatedUser implements Serializable {

    private final UserRepository userRepository;
    private final transient AuthenticationContext authenticationContext;

    private Optional<Authentication> getAuthentication() {
        final SecurityContext context = SecurityContextHolder.getContext();
        return Optional.ofNullable(context.getAuthentication())
                .filter(authentication -> !(authentication instanceof AnonymousAuthenticationToken));
    }

    public Optional<User> get() {
        return this.getAuthentication().map(authentication ->
                this.userRepository.optFindByEmail(authentication.getName()).orElseThrow());
    }

    public void logout() {
        this.authenticationContext.logout();
        this.clearCookies();
    }

    private static final String JWT_HEADER_AND_PAYLOAD_COOKIE_NAME = "jwt.headerAndPayload";

    private static final String JWT_SIGNATURE_COOKIE_NAME = "jwt.signature";

    private void clearCookies() {
        this.clearCookie(JWT_HEADER_AND_PAYLOAD_COOKIE_NAME);
        this.clearCookie(JWT_SIGNATURE_COOKIE_NAME);
    }

    private void clearCookie(final String cookieName) {
        final HttpServletRequest request = VaadinServletRequest.getCurrent().getHttpServletRequest();
        final HttpServletResponse response = VaadinServletResponse.getCurrent().getHttpServletResponse();

        final Cookie cookie = new Cookie(cookieName, null);
        cookie.setPath(this.getRequestContextPath(request));
        cookie.setMaxAge(0);
        cookie.setSecure(request.isSecure());
        cookie.setHttpOnly(true);
        response.addCookie(cookie);
    }

    private String getRequestContextPath(final HttpServletRequest request) {
        final String contextPath = request.getContextPath();
        return "".equals(contextPath) ? "/" : contextPath;
    }

}
