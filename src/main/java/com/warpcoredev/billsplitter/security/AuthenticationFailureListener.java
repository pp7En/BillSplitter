package com.warpcoredev.billsplitter.security;

import com.warpcoredev.billsplitter.data.service.LoginAttemptService;

import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class AuthenticationFailureListener implements
        ApplicationListener<AuthenticationFailureBadCredentialsEvent> {

    private HttpServletRequest request;
    private LoginAttemptService loginAttemptService;

    @SneakyThrows
    @Override
    public void onApplicationEvent(final AuthenticationFailureBadCredentialsEvent e) {
        // e.g. block user
        // final String email = String.valueOf(e.getAuthentication().getPrincipal());

        final String xcHeader = this.request.getHeader("X-Real-IP");

        if (xcHeader != null) {
            this.loginAttemptService.loginFailed(xcHeader);
        } else {
            this.loginAttemptService.loginFailed(this.request.getRemoteAddr());
        }
    }
}
