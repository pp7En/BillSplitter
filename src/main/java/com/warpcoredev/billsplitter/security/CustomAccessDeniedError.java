package com.warpcoredev.billsplitter.security;

import com.vaadin.flow.router.AccessDeniedException;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.ErrorParameter;
import com.vaadin.flow.router.RouteAccessDeniedError;
import com.vaadin.flow.server.HttpStatusCode;

public class CustomAccessDeniedError extends RouteAccessDeniedError {
    @Override
    public int setErrorParameter(final BeforeEnterEvent event,
                                 final ErrorParameter<AccessDeniedException> parameter) {
        this.getElement().setText("Your IP address has been permanently blocked.");
        return HttpStatusCode.UNAUTHORIZED.getCode();
    }
}
