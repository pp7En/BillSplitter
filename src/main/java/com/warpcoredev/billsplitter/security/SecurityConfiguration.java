package com.warpcoredev.billsplitter.security;

import com.vaadin.flow.spring.security.VaadinWebSecurity;
import com.warpcoredev.billsplitter.views.login.LoginView;

import javax.sql.DataSource;
import lombok.RequiredArgsConstructor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@EnableWebSecurity
@Configuration
@RequiredArgsConstructor
public class SecurityConfiguration extends VaadinWebSecurity {

    // two weeks
    public static final int TOKEN_VALIDITY_SECONDS = 60 * 60 * 24 * 14;
    public static final String SESSION_REMEMBER_ME_COOKIE = "SESSION_REMEMBER_ME_COOKIE";

    private final SecurityProperties securityProperties;
    private final DataSource dataSource;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {

        http.authorizeHttpRequests(
                authorize -> authorize.requestMatchers(
                        new AntPathRequestMatcher("/images/*.png"),
                        new AntPathRequestMatcher("/images/*.jpg"),
                        new AntPathRequestMatcher("/line-awesome/**/*.svg"),
                        new AntPathRequestMatcher("/login")
                ).permitAll());

        http.rememberMe(rememberMe -> rememberMe
                .key(this.securityProperties.getKey())
                .alwaysRemember(true)
                .useSecureCookie(true)
                .rememberMeCookieName(SESSION_REMEMBER_ME_COOKIE)
                .tokenValiditySeconds(TOKEN_VALIDITY_SECONDS)
                .tokenRepository(this.persistentTokenRepository()));

        super.configure(http);
        this.setLoginView(http, LoginView.class);
    }

    private PersistentTokenRepository persistentTokenRepository() {
        final JdbcTokenRepositoryImpl tokenRepository = new JdbcTokenRepositoryImpl();
        tokenRepository.setDataSource(this.dataSource);
        return tokenRepository;
    }

}
