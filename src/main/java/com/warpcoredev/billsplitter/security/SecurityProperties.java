package com.warpcoredev.billsplitter.security;

import lombok.Data;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties (prefix = "service.security.remember-me")
public class SecurityProperties {

    private String key;

}
