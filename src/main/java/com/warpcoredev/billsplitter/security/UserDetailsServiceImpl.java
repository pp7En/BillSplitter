package com.warpcoredev.billsplitter.security;

import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.data.repository.UserRepository;

import java.util.Collections;
import java.util.Optional;
import lombok.RequiredArgsConstructor;

import org.springframework.context.annotation.Primary;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Primary
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
        final Optional<User> optUser = this.userRepository.optFindByEmail(email);
        if (optUser.isEmpty()) {
            throw new UsernameNotFoundException("No optUser present with email: " + email);
        } else {
            return new org.springframework.security.core.userdetails.User(
                    optUser.get().getEmail(),
                    optUser.get().getHashedPassword(),
                    Collections.singletonList(new SimpleGrantedAuthority("ROLE_" + optUser.get().getRole())));
        }
    }
}
