package com.warpcoredev.billsplitter.utils;

import jakarta.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Value ("${service.url}")
    private String serviceUrl;

    private static String staticServiceUrl;

    @PostConstruct
    public void init() {
        staticServiceUrl = this.serviceUrl;
    }

    public static String getServiceUrl() {
        return staticServiceUrl;
    }
}
