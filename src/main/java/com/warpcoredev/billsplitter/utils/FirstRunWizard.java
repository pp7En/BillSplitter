package com.warpcoredev.billsplitter.utils;

import com.warpcoredev.billsplitter.data.Role;
import com.warpcoredev.billsplitter.data.Status;
import com.warpcoredev.billsplitter.data.entity.Setting;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.data.service.SettingService;
import com.warpcoredev.billsplitter.data.service.UserService;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import static com.warpcoredev.billsplitter.utils.SharedConsts.FIRST_RUN_KEY;
import static com.warpcoredev.billsplitter.utils.SharedConsts.FIRST_RUN_VALUE_PREFIX;

@Service
@Slf4j
@AllArgsConstructor
public class FirstRunWizard implements Serializable {

    private final SettingService settingService;
    private final UserService userService;

    private static final String ALLOWED_CHARACTERS = "abcdefghijklmnopqrstuvwxyz0123456789";

    public void checkFirstRun() throws NoSuchAlgorithmException {

        final String value = this.settingService.getValueByKey(FIRST_RUN_KEY);
        if (!value.isEmpty()) {
            return;
        }

        final String randomPassword = this.generateRandomString(30);
        final String randomEmail = this.generateRandomString(8) + "@example.com";

        final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        final String hashed = bCryptPasswordEncoder.encode(randomPassword);


        final User user = User.builder()
                .email(randomEmail)
                .name("Admin")
                .enabled(Status.ENABLED)
                .role(Role.ADMIN)
                .notifications(Status.DISABLED)
                .hashedPassword(hashed)
                .build();

        this.userService.saveEntity(user);

        this.settingService.saveValue(Setting.builder()
                .name(FIRST_RUN_KEY)
                .value(FIRST_RUN_VALUE_PREFIX + HelperClass.getUnixtime())
                .build());

        log.info("************************************************");
        log.info("FIRST RUN");
        log.info("E-Mail:   {}", randomEmail);
        log.info("Password: {}", randomPassword);
        log.info("************************************************");
    }

    public String generateRandomString(final int length) throws NoSuchAlgorithmException {
        final StringBuilder stringBuilder = new StringBuilder();
        final Random random = SecureRandom.getInstanceStrong();

        for (int i = 0; i < length; i++) {
            final int randomIndex = random.nextInt(ALLOWED_CHARACTERS.length());
            final char randomChar = ALLOWED_CHARACTERS.charAt(randomIndex);
            stringBuilder.append(randomChar);
        }

        return stringBuilder.toString();
    }
}
