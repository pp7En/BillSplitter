package com.warpcoredev.billsplitter.utils;

import com.vaadin.flow.router.OptionalParameter;
import com.warpcoredev.billsplitter.data.entity.User;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Arrays;
import java.util.Base64;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;

import static com.warpcoredev.billsplitter.utils.SharedConsts.GET_PARAM_CODE;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GET_PARAM_EMAIL;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GET_PARAM_GROUP;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GET_PARAM_TOKEN;

@Slf4j
public final class HelperClass {

    public static final long DIVIDE_1000L = 1000L;

    private static final String SALTCHARS = "abcdefghijklmnopqrstuvwxyz1234567890";
    private static final String EQUAL_SIGN = "=";
    private static final String AMPERSAND = "&";

    private HelperClass() {
        // No instance
    }

    public static String getRandomCode(final int size) throws NoSuchAlgorithmException {

        final StringBuilder salt = new StringBuilder();
        final Random r = SecureRandom.getInstanceStrong();
        while (salt.length() < size) {
            final int index = r.nextInt(SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();
    }

    public static long getUnixtime() {
        return System.currentTimeMillis() / DIVIDE_1000L;
    }

    /**
     * Example input: str = "Z3JvdXA9MTImdG9rZW49YWJj".
     */
    public static String base64decode(final String base64string) {
        return new String(Base64.getDecoder().decode(base64string));
    }

    /**
     * Example input: str = "group=12&token=abc".
     */
    public static String base64encode(final String plaintext) {
        return Base64.getEncoder().encodeToString(plaintext.getBytes(StandardCharsets.UTF_8));
    }

    public static String createEncodedLoginString(final Long groupId, final String strGroupCode) {
        return base64encode(GET_PARAM_GROUP
                + EQUAL_SIGN
                + groupId
                + AMPERSAND
                + GET_PARAM_TOKEN
                + EQUAL_SIGN
                + strGroupCode);
    }

    public static String createEncodedRestorePasswordLink(final User user) {
        return base64encode(GET_PARAM_EMAIL
                + EQUAL_SIGN
                + user.getEmail()
                + AMPERSAND
                + GET_PARAM_CODE
                + EQUAL_SIGN
                + user.getValidationCode());
    }

    public static Map<String, String> extractParameters(final String decodedString) {
        return Arrays
                .stream(decodedString.split(AMPERSAND))
                .map(s -> s.split(EQUAL_SIGN))
                .collect(Collectors.toMap(e -> e[0], e -> e[1]));
    }

    public static String getPageTitle(final String page, @OptionalParameter final Long parameter) {
        if (parameter == null) {
            return SharedConsts.TITLE_BILLSPLITTER
                    + SharedConsts.DELIMITER
                    + page;

        }
        return SharedConsts.TITLE_BILLSPLITTER
                + SharedConsts.DELIMITER
                + page
                + parameter;
    }

    public static String getURL() {
        return AppConfig.getServiceUrl();
    }

    public static DecimalFormat currencyFormat(final String currencyFormat,
                                               final String decimalSeparator,
                                               final String groupingSeparator) {

        final DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(decimalSeparator.charAt(0));
        symbols.setGroupingSeparator(groupingSeparator.charAt(0));

        return new DecimalFormat(currencyFormat, symbols);
    }

    public static DecimalFormat decimalFormat(final String decimalSeparator,
                                               final String groupingSeparator) {

        final DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(decimalSeparator.charAt(0));
        symbols.setGroupingSeparator(groupingSeparator.charAt(0));

        return new DecimalFormat("#0.00", symbols);
    }


    @SuppressWarnings("checkstyle:RedundantModifier")
    public static String getBuildTime() {
        final String fileName = "build";

        try (final InputStream inputStream = HelperClass.class.getClassLoader().getResourceAsStream(fileName)) {
            if (inputStream != null) {
                final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                final String line = reader.readLine();
                if (line != null) {
                    return line;
                }
            }
        } catch (final IOException ex) {
            return "dev";
        }
        return "dev";
    }
}
