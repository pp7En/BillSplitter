package com.warpcoredev.billsplitter.utils;

import java.time.format.DateTimeFormatter;

public final class SharedConsts {

    private SharedConsts() {
        // no instance
    }

    public static final String TITLE_BILLSPLITTER = "BillSplitter";
    public static final String DELIMITER = " | ";
    public static final String VAADIN_BUTTON = "vaadin-button";
    public static final String SPACE = " ";
    public static final String EMPTY_SPACE = "";
    public static final String COLON = ":";
    public static final String DASH = "-";
    public static final String UNDERSCORE = "_";
    public static final String COLON_SPACE = COLON + SPACE;
    public static final String HTML_LINE_BREAK = "<br>";
    public static final String LINE_BREAK = "\n";
    public static final String CURRENCY_FORMAT = "#,##0.00";
    public static final int ONE_DAY_IN_SECONDS = 24 * 60 * 60;
    public static final long VALIDATION_CODE_VALID = 1L;
    public static final double CURRENCY_FIELDS_MIN = 0.01d;
    public static final String FIRST_RUN_KEY = "FIRST_RUN";
    public static final String FIRST_RUN_VALUE_PREFIX = "COMPLETE: ";

    public static final String MODE_PRODUCTION = "prod";

    public static final String EVENT_TYPE_CLICK = "click";
    public static final String DRAWER_OPENED_CHANGED = "drawer-opened-changed";

    public static final String PUBLIC_REGISTRATION = "PUBLIC_REGISTRATION";
    public static final String PREFIX_LAST_GROUP = "LAST_GROUP_FOR_USERID_";

    public static final String GET_PARAM_GROUP = "group";
    public static final String GET_PARAM_TOKEN = "token";
    public static final String GET_PARAM_EMAIL = "email";
    public static final String GET_PARAM_CODE = "code";

    public static final String JS_AUTOLOGIN_USERNAME = "this.$.vaadinLoginForm.$.vaadinLoginUsername.value = $0;";
    public static final String JS_AUTOLOGIN_PASSWORD = "this.$.vaadinLoginForm.$.vaadinLoginPassword.value = $0;";
    public static final String JS_AUTOLOGIN_CLICK = "document.querySelector('vaadin-button[slot=\"submit\"]').click();";
    public static final String JS_SCROLL_TO_TOP =
              "document.body.scrollTop = 0; document.documentElement.scrollTop = 0;";
    public static final String ID_DOWNLOAD_PDF_ANCHOR = "download-pdf-anchor";
    public static final String JS_CLICK_DOWNLOAD_PDF =
            "document.getElementById('" + ID_DOWNLOAD_PDF_ANCHOR + "').click();";
    public static final String ERROR_WHILE_FETCHING_DATA = "An error occurred while fetching the data: ";

    // LOCATIONS ------------------------------------------------------------------------------------------------------
    private static final String SLASH = "/";

    public static final String URL_LOGIN = "login";
    public static final String URL_LOGIN2 = SLASH + URL_LOGIN;
    public static final String URL_AUTOLOGIN = "/login#";
    public static final String URL_GROUPS = "groups";
    public static final String URL_GROUPS2 = SLASH + URL_GROUPS + SLASH;
    public static final String URL_SETTINGS = "settings";
    public static final String URL_LOGOUT = "logout";
    public static final String URL_ADD_GROUP = "add";
    public static final String URL_FORGOT_PASSWORD = "restore";

    // SETTINGS -------------------------------------------------------------------------------------------------------
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM");
    public static final String DD_MM_YYYY = "dd.MM.yyyy";
    public static final String DD_MM_YY = "dd.MM.yy";
    public static final DateTimeFormatter DATE_FORMATTER_HUMAN_READABLE = DateTimeFormatter.ofPattern(DD_MM_YYYY);
    public static final DateTimeFormatter DATE_FORMATTER_HUMAN_READABLE_SHORT = DateTimeFormatter.ofPattern(DD_MM_YY);
    public static final String REGEX_GROUP_NAME = "^[\\w ]+$";
    public static final String REGEX_PUBLIC_LINK = ".*\\/login#[a-zA-Z0-9=]{20,}$";
    public static final String REGEX_EMAIL = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$";
    public static final long RANGE_STATISTICS_IN_MONTHS = 12L;
    public static final int MIN_LENGTH_GROUP_NAME = 6;
    public static final int MIN_LENGTH_JOIN_GROUP = 10;
    public static final int MIN_LENGTH_USERNAME = 3;
    public static final int MIN_LENGTH_PASSWORD = 8;
    public static final char DECIMAL_SEPARATOR = ',';

    public static final int MAX_LOGIN_ATTEMPT = 10;

    public static final int LIMIT_TIME_RANGE_BRUTE_FORCE = 60;

    // time range to reduce or delete old entries (in minutes)
    public static final int LIMIT_TIME_RANGE_CLEAN_UP = 360;

    // IDs ------------------------------------------------------------------------------------------------------------
    public static final String HEADER_GROUP_NAME = "header-group-name";
    public static final String TAB_CREATE_GROUP = "tab-create-group";
    public static final String TAB_CREATE_GROUP_TXT_GROUP_NAME = TAB_CREATE_GROUP + "-txt-group-name";
    public static final String TAB_CREATE_GROUP_SUBMIT = TAB_CREATE_GROUP + "-submit";
    public static final String PAYMENT_GRID = "payment-grid";
    public static final String BILL_DESCRIPTION = "bill-description";
    public static final String BILL_TOTAL_PAYABLE = "bill-total-payable";
    public static final String BILL_USER_WHO_PAID = "bill-user-who-paid";
    public static final String BILL_PAYMENT_GRID = "bill-payment-grid";
    public static final String BILL_PAYMENT_GRID_CHECKBOX = BILL_PAYMENT_GRID + "-checkbox";
    public static final String BILL_SUBMIT_BTN = "bill-submit-btn";
    public static final String BILL_CALCULATOR_BTN = "bill-open-calculator-btn";
    public static final String BILL_CALCULATOR_RESULT_TEXTFIELD = "bill-calculator-result-textfield";
    public static final String GROUP_OPTIONS_OPEN_BTN = "group-options-open-btn";
    public static final String GROUP_OPTIONS_GUEST_USER_BTN = "group-options-btn-guest-user";
    public static final String GROUP_OPTIONS_GUEST_USER_TXT = "group-options-txt-guest-user";
    public static final String GROUP_OPTIONS_GUEST_USER_SUBMIT = "group-options-submit-guest-user";
    public static final String GROUP_OPTIONS_SETTLEMENT_TABLE_BTN = "group-options-settlement-table-btn";
    public static final String GROUP_OPTIONS_MEMBER_TABLE_BTN = "group-options-member-table-btn";
    public static final String GROUP_OPTIONS_SHARE_GROUP_BTN = "group-options-share-group-btn";
    public static final String GROUP_OPTIONS_SHARE_GROUP_CBX = "group-options-share-group-cbx";
    public static final String GROUP_OPTIONS_SHARE_GROUP_SUBMIT = "group-options-share-group-submit";
    public static final String GROUP_OPTIONS_SHARE_GROUP_LINK = "group-options-share-group-link";
    public static final String GROUP_OPTIONS_LEAVE_GROUP_BTN = "group-options-leave-group-btn";
    public static final String GROUP_JOIN_REQUEST_ELEMENT = "group-join-request-element";
    public static final String GROUP_OPTIONS_HISTORY_BTN = "group-options-history-btn";
    public static final String GROUP_OPTIONS_STATISTIC_BTN = "group-options-statistic-btn";

    public static final String HISTORY_DIALOG_SEARCH_FIELD = "history-dialog-search-field";
    public static final String STATISTIC_DIALOG_ID = "statistic-dialog-id";

    public static final String TOGGLE_BTN_TRANSACTION = "toggle-btn-transaction";
    public static final String TOGGLE_BTN_EXPENSE = "toggle-btn-expense";

    public static final String SINGLE_PAYMENT_TOTAL_ID = "single-payment-total-id";
    public static final String SINGLE_PAYMENT_FROM_ID = "single-payment-from-user-id";
    public static final String SINGLE_PAYMENT_TO_ID = "single-payment-to-user-id";
    public static final String SINGLE_PAYMENT_SAVE_BTN_ID = "single-payment-save-btn-id";

    public static final String TABS_CREATE_AND_JOIN_GROUP = "tabs-create-and-join-group";
    public static final String TAB_JOIN_GROUP = "tab-join-group";
    public static final String TAB_JOIN_GROUP_TXT_GROUP_CODE = TAB_JOIN_GROUP + "-txt-group-code";
    public static final String TAB_JOIN_GROUP_SUBMIT = TAB_JOIN_GROUP + "-submit";
    public static final String VIEW_ADD_GROUP_ID = "view-add-group";
    public static final String VIEW_LEAVE_GROUP_BTN = "view-leave-group-btn";
    public static final String VIEW_LEAVE_GROUP_SELECT_NEW_ADMIN = "view-leave-group-select-new-admin";
    public static final String DLG_DELETE_ACCOUNT_ID = "dlg-delete-account";
    public static final String DLG_DELETE_ACCOUNT_BTN_APPLY = DLG_DELETE_ACCOUNT_ID + "-apply";
    public static final String DLG_DELETE_ACCOUNT_PASSWORD_FIELD = DLG_DELETE_ACCOUNT_ID + "-password";

    public static final String SETTING_HEADER = "settings-header";
    public static final String SETTING_CBX_NOTIFICATIONS = "settings-cbx-notifications";
    public static final String SETTING_TXT_USERNAME = "settings-txt-username";
    public static final String SETTING_TXT_EMAIL_ADDRESS = "settings-txt-email-address";
    public static final String SETTING_APPLY_BTN = "settings-apply-btn";
    public static final String SETTING_CHANGE_PASSWORD_BTN = "settings-change-password-btn";
    public static final String SETTING_DELETE_ACCOUNT_BTN = "settings-delete-account-btn";

    public static final String RESTORE_VL_ID = "restore-account-area";
    public static final String REGISTER_VL_ID = "register-account-area";

    public static final String RESTORE_TAB_ID = "restore-account-tab";
    public static final String REGISTER_TAB_ID = "register-account-tab";

    public static final String REGISTER_NAME_ID = "register-account-name-text-field";
    public static final String REGISTER_EMAIL_ID = "register-account-email-text-field";
    public static final String REGISTER_PASSWORD1_ID = "register-account-password1-text-field";
    public static final String REGISTER_PASSWORD2_ID = "register-account-password2-text-field";
    public static final String REGISTER_BUTTON_ID = "register-account-button";

    public static final String RESTORE_EMAIL_ID = "restore-account-email-text-field";
    public static final String RESTORE_BUTTON_ID = "restore-account-button";
    public static final String RESTORE_NEW_PASSWORD_PASSWORD1_ID = "restore-account-password1-text-field";
    public static final String RESTORE_NEW_PASSWORD_PASSWORD2_ID = "restore-account-password2-text-field";
    public static final String RESTORE_NEW_PASSWORD_BUTTON_ID = "restore-account-new-password-button";

    public static final String PLACEHOLDER_DATE = "%DATE%";
    public static final String PLACEHOLDER_PDF_PREFIX = "%PREFIX%";
    public static final String PLACEHOLDER_GROUP = "%GROUP%";

    public static final String BUTTON_DOWNLOAD_PDF = "button-download-pdf";

    public static final String AWAITING_GROUP_JOIN_REQUEST_VL = "awaiting-group-join-request-vl";
    public static final String REJECTED_GROUP_JOIN_REQUEST_VL = "rejected-group-join-request-vl";
}
