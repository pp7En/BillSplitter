package com.warpcoredev.billsplitter.utils;

@SuppressWarnings("checkstyle:InterfaceIsType")
public final class SharedStyles {

    private SharedStyles() {
        // no instance
    }

    public static final String APPLICATION_HEADER_TITLE_BOX = "application-header-title-box";
    public static final String APPLICATION_HEADER_TITLE = "application-header-title";
    public static final String TEXT_ALIGN_LEFT = "text-align-left";
    public static final String TEXT_ALIGN_CENTER = "text-align-center";
    public static final String TEXT_ALIGN_RIGHT = "text-align-right";
    public static final String ALIGN_ITEMS_END = "align-items-end";

    public static final String TOGGLE_ATTR = "aria-label";
    public static final String TOGGLE_VALUE = "Menu toggle";
    public static final String TOGGLE_MENU_BARS = "toggle-menu-bars";
    public static final String BAR_1 = "bar1";
    public static final String BAR_2 = "bar2";
    public static final String BAR_3 = "bar3";
    public static final String CHANGE = "change";

    public static final String GROUP_MAIN_TOGGLE_BTN_GRP = "group-main-toggle-btn-grp";

    public static final String LAYOUT_LOGIN_FORM = "custom-login-form";
    public static final String LAYOUT_CONTENT_PADDING = "custom-layout-content";
    public static final String LAYOUT_HEADER = "custom-layout-header";
    public static final String LAYOUT_LINK = "custom-layout-link";

    public static final String TEXT_SHADOW = "text-shadow";
    public static final String TEXT_COLOR_WHITE = "text-color-white";
    public static final String CONTENT_MAX_WIDTH = "content-max-width";
    public static final String HEIGHT_AUTO = "height-auto";

    public static final String BUTTON_PARENT_CLASS = "button-parent-class";
    public static final String BUTTON_GREEN = "button-green";
    public static final String BUTTON_RED = "button-red";
    public static final String BUTTON_BLUE = "button-blue";
    public static final String BUTTON_GROUP_OPTIONS = "button-group-options";

    public static final String FULL_WIDTH = "full-width";
    public static final String HALF_WIDTH = "half-width";

    public static final String BORDER = "border";

    public static final String FONT_XS = "font-xs";

    public static final String DIALOG_MAX_WIDTH = "dialog-max-width";

    public static final String NO_GAP = "no-gap";

    public static final String MARGIN_AUTO = "margin-auto";

    public static final String PAYMENT_BOX = "payment-box";

    public static final String LUMO_PRIMARY_COLOR = "--lumo-primary-color";

    public static final String COLOR_HEX_GREEN = "#4caf50";

    public static final String NO_PADDING = "no-padding";
    public static final String NO_MARGIN = "no-margin";

    public static final String PADDING_LEFT_FOR_ACCORDION = "padding-left-for-accordion";

    public static final String CALC_BTN_PARENT = "calc-button-parent";
    public static final String CALC_BTN_NUMBER = "calc-button-number";
    public static final String CALC_BTN_OPERATOR = "calc-button-operator";
    public static final String CALC_HL = "calc-hl";
    public static final String CALC_RESULT_TEXTFIELD = "calc-result-textfield";

    public static final String GROUP_JOIN_AREA = "group-join-area";

    public static final String STYLE_APP_NAV_ITEM_SECTION_GROUPS = "app-nav-item-section-groups";
    public static final String NOTIFICATION_BTN = "notification-button";
    public static final String NOTIFICATION_BTN_ID = "notification-button-id";
    public static final String POSITION_RELATIV = "position-relativ";
    public static final String STYLE_APP_NAV_ITEM_SECTION_SETTINGS = "app-nav-item-section-settings";
    public static final String SETTLEMENT_TABLE_LINE = "settlement-table-line";
    public static final String MEMBER_TABLE_LINE = "member-table-line";
    public static final String NO_WRAP = "no-wrap";
    public static final String FONT_STYLE_ITALIC = "font-style-italic";
    public static final String FONT_BOLD = "font-bold";
    public static final String DEFAULT_MAX_WIDTH = "default-max-width";
    public static final String LINE_BREAKS_ANYWHERE = "line-breaks-anywhere";
    public static final String BUTTON_CALC = "button-calc";
    public static final String MARGIN_TOP_10 = "margin-top-10";
    public static final String HISTORY_DETAILS = "history-details";
    public static final String NO_PADDING_TOP = "no-padding-top";
    public static final String NO_PADDING_BOTTOM = "no-padding-bottom";
    public static final String INNER_BORDER = "inner-border";
    public static final String HISTORY_GRID = "history-grid";
    public static final String SETTINGS_INFO_DIV = "settings-info-div";
}
