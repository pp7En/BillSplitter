package com.warpcoredev.billsplitter.views;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;
import com.warpcoredev.billsplitter.data.Role;
import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.data.service.GroupService;
import com.warpcoredev.billsplitter.data.service.SettingService;
import com.warpcoredev.billsplitter.security.AuthenticatedUser;
import com.warpcoredev.billsplitter.utils.SharedConsts;

import java.util.Optional;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.security.RolesAllowed;
import lombok.RequiredArgsConstructor;

@Route (value = "", layout = MainLayout.class)
@RolesAllowed ({Role.Fields.ADMIN, Role.Fields.USER, Role.Fields.GUEST, Role.Fields.FOR_PUBLIC_SHARE})
@RequiredArgsConstructor
public class InitView extends VerticalLayout implements BeforeEnterObserver {

    private final AuthenticatedUser authenticatedUser;
    private final GroupService groupService;
    private final SettingService settingService;

    private User user;

    @PostConstruct
    private void init() {
        this.user = this.authenticatedUser.get().orElseThrow();
    }

    private Group getLastSelectedGroupFromDb() {
        Group group = this.settingService.getLastSelectedGroup(this.user);

        if (group == null) {
            final Optional<Group> optGroup = this.groupService.getGroups(this.user)
                    .stream()
                    .findFirst();
            if (optGroup.isEmpty()) {
                return null;
            }
            group = optGroup.get();
            this.settingService.setLastSelectedGroup(this.user, group);
        }

        return group;
    }

    @Override
    public void beforeEnter(final BeforeEnterEvent event) {
        final Group grp = this.getLastSelectedGroupFromDb();
        if (grp == null) {
            event.forwardTo(SharedConsts.URL_ADD_GROUP);
        } else {
            event.forwardTo(SharedConsts.URL_GROUPS2 + grp.getId());
        }
    }
}
