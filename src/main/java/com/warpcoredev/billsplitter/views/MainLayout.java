package com.warpcoredev.billsplitter.views;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Header;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.sidenav.SideNav;
import com.vaadin.flow.component.sidenav.SideNavItem;
import com.vaadin.flow.dom.Style;
import com.vaadin.flow.server.auth.AccessAnnotationChecker;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.theme.lumo.LumoUtility;
import com.warpcoredev.billsplitter.components.LoadingScreen;
import com.warpcoredev.billsplitter.components.appnav.RefreshAppMenuEvent;
import com.warpcoredev.billsplitter.data.Role;
import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.service.GroupService;
import com.warpcoredev.billsplitter.security.AuthenticatedUser;
import com.warpcoredev.billsplitter.utils.HelperClass;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedConsts;
import com.warpcoredev.billsplitter.utils.SharedStyles;
import com.warpcoredev.billsplitter.views.group.GroupAddView;
import com.warpcoredev.billsplitter.views.group.GroupMainView;
import com.warpcoredev.billsplitter.views.logout.LogoutView;
import com.warpcoredev.billsplitter.views.settings.SettingsView;

import java.util.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;

import static com.warpcoredev.billsplitter.utils.SharedConsts.DRAWER_OPENED_CHANGED;
import static com.warpcoredev.billsplitter.utils.SharedConsts.EVENT_TYPE_CLICK;
import static com.warpcoredev.billsplitter.utils.SharedConsts.JS_SCROLL_TO_TOP;
import static com.warpcoredev.billsplitter.utils.SharedConsts.TITLE_BILLSPLITTER;
import static com.warpcoredev.billsplitter.utils.SharedConsts.URL_GROUPS2;
import static com.warpcoredev.billsplitter.utils.SharedStyles.BAR_1;
import static com.warpcoredev.billsplitter.utils.SharedStyles.BAR_2;
import static com.warpcoredev.billsplitter.utils.SharedStyles.BAR_3;
import static com.warpcoredev.billsplitter.utils.SharedStyles.CHANGE;
import static com.warpcoredev.billsplitter.utils.SharedStyles.TOGGLE_ATTR;
import static com.warpcoredev.billsplitter.utils.SharedStyles.TOGGLE_MENU_BARS;
import static com.warpcoredev.billsplitter.utils.SharedStyles.TOGGLE_VALUE;

/**
 * The main view is a top-level placeholder for other views.
 */
@RequiredArgsConstructor
public class MainLayout extends AppLayout {

    private final Div viewTitleBox = new Div();
    private final H2 viewTitle = new H2();
    private final SideNav nav = new SideNav();
    private final Scroller scroller = new Scroller();
    private final AccessAnnotationChecker accessChecker;
    private final LoadingScreen loadingScreen = new LoadingScreen();

    private final AuthenticatedUser authenticatedUser;
    private final GroupService groupService;

    private Div toggleBox;
    private Registration toggleClickListener;

    @PostConstruct
    private void init() {
        this.addDrawerContent();
        this.addHeaderContent();

        this.addToNavbar(this.loadingScreen);

        // close drawer initial
        this.setDrawerOpened(false);
    }

    private void addHeaderContent() {
        // put title in the middle of the page
        this.viewTitleBox.addClassName(SharedStyles.APPLICATION_HEADER_TITLE_BOX);

        this.viewTitle.addClassNames(
                SharedStyles.APPLICATION_HEADER_TITLE,
                LumoUtility.FontSize.LARGE,
                LumoUtility.Margin.NONE,
                SharedStyles.TEXT_SHADOW,
                SharedStyles.TEXT_ALIGN_CENTER,
                SharedStyles.TEXT_COLOR_WHITE);
        this.viewTitle.addClickListener(e -> UI.getCurrent().navigate(SharedConsts.URL_LOGIN));
        this.viewTitleBox.add(this.viewTitle);

        this.addToNavbar(false, this.toggleBox, this.viewTitleBox);
        this.addClassName(SharedStyles.HEIGHT_AUTO);
    }

    private void addDrawerContent() {
        final H1 appName = new H1();
        appName.addClassNames(LumoUtility.FontSize.LARGE, LumoUtility.Margin.NONE);
        final Header header = new Header(appName);

        final Div div = new Div("Build: " + HelperClass.getBuildTime());
        div.getStyle().setTextAlign(Style.TextAlign.CENTER);

        this.toggleBox = this.customDrawerToggle();

        this.addToDrawer(header, this.toggleBox, this.scroller, div);
    }

    private SideNav createNavigation() {
        this.nav.removeAll();

        if (this.accessChecker.hasAccess(GroupMainView.class)) {
            final SideNavItem sectionGroups = new SideNavItem(this.getTranslation(I18NKeys.PAGE_GROUPS));
            sectionGroups.addClassName(SharedStyles.STYLE_APP_NAV_ITEM_SECTION_GROUPS);

            this.nav.addItem(sectionGroups);

            final List<Group> groups = this.groupService.getGroups(this.authenticatedUser.get().orElseThrow());
            groups.forEach(group -> {
                final SideNavItem sideNavItem = new SideNavItem(group.getName());
                sideNavItem.setPath(URL_GROUPS2 + group.getId());
                this.nav.addItem(sideNavItem);
            });

            if (!this.authenticatedUser.get().orElseThrow().getRole().equals(Role.FOR_PUBLIC_SHARE)) {
                final SideNavItem addGroup = new SideNavItem(this.getTranslation(I18NKeys.MENU_NEW_GROUP));
                addGroup.setPath(GroupAddView.class);
                this.nav.addItem(addGroup);
            }
        }

        if (this.accessChecker.hasAccess(SettingsView.class)) {
            final SideNavItem sectionSettings = new SideNavItem(this.getTranslation(I18NKeys.PAGE_SETTINGS),
                    SettingsView.class);
            sectionSettings.addClassName(SharedStyles.STYLE_APP_NAV_ITEM_SECTION_SETTINGS);
            this.nav.addItem(sectionSettings);
        }

        if (this.accessChecker.hasAccess(LogoutView.class)) {
            final SideNavItem logout = new SideNavItem(this.getTranslation(I18NKeys.MENU_LOGOUT),
                    LogoutView.class);
            this.nav.addItem(logout);
        }

        // auto-close drawer after click
        this.nav.getItems().forEach(item -> item
                .getElement()
                .addEventListener(EVENT_TYPE_CLICK, e -> {
                    this.switchToggleButtonState();
                    this.setDrawerOpened(false);
                    this.getElement().executeJs(JS_SCROLL_TO_TOP);
                }));

        // reset toggle when closing (clicks anywhere)
        this.getElement().addEventListener(DRAWER_OPENED_CHANGED, ev -> {
            if (!this.isDrawerOpened() && this.toggleBox.hasClassName(CHANGE)) {
                this.toggleBox.removeClassName(CHANGE);
                this.getElement().executeJs(JS_SCROLL_TO_TOP);
            }
        });

        // prevent redundant assignments
        if (this.toggleClickListener != null) {
            this.toggleClickListener.remove();
        }

        // set toggle state when click on toggle
        this.toggleClickListener = this.toggleBox.addClickListener(ev -> {
            this.switchToggleButtonState();
            this.getElement().executeJs(JS_SCROLL_TO_TOP);
        });

        return this.nav;
    }

    private Div customDrawerToggle() {
        final Div div = new Div();
        div.getElement().setAttribute(TOGGLE_ATTR, TOGGLE_VALUE);
        div.addClassName(TOGGLE_MENU_BARS);

        final Div bar1 = new Div();
        bar1.addClassName(BAR_1);

        final Div bar2 = new Div();
        bar2.addClassName(BAR_2);

        final Div bar3 = new Div();
        bar3.addClassName(BAR_3);

        div.add(bar1, bar2, bar3);

        return div;
    }

    private void switchToggleButtonState() {
        if (this.toggleBox.hasClassName(CHANGE)) {
            this.toggleBox.removeClassName(CHANGE);
            this.setDrawerOpened(false);
        } else {
            this.toggleBox.addClassName(CHANGE);
            this.setDrawerOpened(true);
        }
    }

    public void refreshMenu() {
        this.scroller.setContent(this.createNavigation());
    }

    @Override
    protected void afterNavigation() {
        super.afterNavigation();
        this.viewTitle.setText(TITLE_BILLSPLITTER);
        this.scroller.setContent(this.createNavigation());
    }

    @Override
    protected void onAttach(final AttachEvent attachEvent) {
        super.onAttach(attachEvent);
        // Register to events from the event bus
        ComponentUtil.addListener(
                attachEvent.getUI(),
                RefreshAppMenuEvent.class,
                event -> {
                    if (event.getSource() != this) {
                        this.refreshMenu();
                        if (event.getGroupId() != null) {
                            UI.getCurrent().navigate(SharedConsts.URL_GROUPS2 + event.getGroupId());
                        }
                    }
                }
        );
    }
}
