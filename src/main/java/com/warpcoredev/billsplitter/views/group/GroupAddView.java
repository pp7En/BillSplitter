package com.warpcoredev.billsplitter.views.group;

import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.TabSheet;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.dom.Style;
import com.vaadin.flow.router.HasDynamicTitle;
import com.vaadin.flow.router.Route;
import com.warpcoredev.billsplitter.components.appnav.RefreshAppMenuEvent;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.BSNotification;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.NotificationType;
import com.warpcoredev.billsplitter.data.Role;
import com.warpcoredev.billsplitter.data.Status;
import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.entity.Mapping;
import com.warpcoredev.billsplitter.data.service.GroupService;
import com.warpcoredev.billsplitter.data.service.MappingService;
import com.warpcoredev.billsplitter.security.AuthenticatedUser;
import com.warpcoredev.billsplitter.utils.HelperClass;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedConsts;
import com.warpcoredev.billsplitter.utils.SharedStyles;
import com.warpcoredev.billsplitter.views.MainLayout;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.security.RolesAllowed;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import static com.warpcoredev.billsplitter.utils.SharedConsts.SPACE;
import static com.warpcoredev.billsplitter.utils.SharedConsts.TABS_CREATE_AND_JOIN_GROUP;
import static com.warpcoredev.billsplitter.utils.SharedConsts.TAB_CREATE_GROUP;
import static com.warpcoredev.billsplitter.utils.SharedConsts.TAB_CREATE_GROUP_SUBMIT;
import static com.warpcoredev.billsplitter.utils.SharedConsts.TAB_CREATE_GROUP_TXT_GROUP_NAME;
import static com.warpcoredev.billsplitter.utils.SharedConsts.TAB_JOIN_GROUP;
import static com.warpcoredev.billsplitter.utils.SharedConsts.TAB_JOIN_GROUP_SUBMIT;
import static com.warpcoredev.billsplitter.utils.SharedConsts.TAB_JOIN_GROUP_TXT_GROUP_CODE;
import static com.warpcoredev.billsplitter.utils.SharedConsts.URL_ADD_GROUP;
import static com.warpcoredev.billsplitter.utils.SharedConsts.VIEW_ADD_GROUP_ID;

@Route (value = URL_ADD_GROUP, layout = MainLayout.class)
@RolesAllowed ({Role.Fields.ADMIN, Role.Fields.USER, Role.Fields.GUEST, Role.Fields.FOR_PUBLIC_SHARE})
@Slf4j
@RequiredArgsConstructor
public class GroupAddView extends VerticalLayout implements HasDynamicTitle {

    private final AuthenticatedUser authenticatedUser;
    private final MappingService mappingService;
    private final GroupService groupService;

    private final TextField tfCreateGroup = new TextField();
    private final Button btnJoinGroup = new Button(this.getTranslation(I18NKeys.ADD_GROUP_SEND));
    private final TextField tfJoinGroup = new TextField();
    private final Button btnCreateGroup = new Button(this.getTranslation(I18NKeys.ADD_GROUP_SEND));
    private final TabSheet tabSheet = new TabSheet();

    private final String pageTitle = HelperClass.getPageTitle(
            this.getTranslation(I18NKeys.PAGE_GROUPS), null);

    @PostConstruct
    private void initUI() {
        this.tabSheet.add(this.getTranslation(I18NKeys.ADD_GROUP_GROUP_CREATE),
                new Div(this.createNewGroupArea()));

        this.tabSheet.add(this.getTranslation(I18NKeys.ADD_GROUP_JOIN_INVITE_CODE),
                new Div(this.createInviteArea()));
        this.tabSheet.setId(TABS_CREATE_AND_JOIN_GROUP);

        this.add(this.tabSheet);
        this.setSpacing(false);
        this.setSizeFull();
        this.setJustifyContentMode(JustifyContentMode.CENTER);
        this.setDefaultHorizontalComponentAlignment(Alignment.STRETCH);
        this.addClassNames(SharedStyles.TEXT_ALIGN_CENTER, SharedStyles.CONTENT_MAX_WIDTH);
        this.setId(VIEW_ADD_GROUP_ID);
    }

    private Div createNewGroupArea() {
        final Div div = new Div();
        div.setId(TAB_CREATE_GROUP);
        div.getStyle().setTextAlign(Style.TextAlign.LEFT);

        final Paragraph description = new Paragraph(this.getTranslation(I18NKeys.ADD_GROUP_GROUP_DESC));

        this.tfCreateGroup.setLabel(this.getTranslation(I18NKeys.ADD_GROUP_GROUP_NAME));
        this.tfCreateGroup.setWidthFull();
        this.tfCreateGroup.setMinLength(SharedConsts.MIN_LENGTH_GROUP_NAME);
        this.tfCreateGroup.setPattern(SharedConsts.REGEX_GROUP_NAME);
        this.tfCreateGroup.setErrorMessage(String.join(
                SPACE,
                this.getTranslation(I18NKeys.MIN_LENGTH_1),
                String.valueOf(SharedConsts.MIN_LENGTH_GROUP_NAME),
                this.getTranslation(I18NKeys.MIN_LENGTH_2))
        );
        this.tfCreateGroup.setValueChangeMode(ValueChangeMode.EAGER);
        this.tfCreateGroup.addValueChangeListener(e -> this.tfCreateGroupHandler(e.getValue()));
        this.tfCreateGroup.setId(TAB_CREATE_GROUP_TXT_GROUP_NAME);

        this.btnCreateGroup.addClassNames(SharedStyles.BUTTON_GREEN, SharedStyles.BUTTON_PARENT_CLASS);
        this.btnCreateGroup.addClickListener(e -> this.createMappingCreateGroup(this.tfCreateGroup.getValue()));
        this.btnCreateGroup.setEnabled(false);
        this.btnCreateGroup.setId(TAB_CREATE_GROUP_SUBMIT);

        div.add(description, this.tfCreateGroup, this.btnCreateGroup);

        return div;
    }

    private Div createInviteArea() {
        final Div div = new Div();
        div.setId(TAB_JOIN_GROUP);
        div.getStyle().setTextAlign(Style.TextAlign.LEFT);

        final Paragraph description = new Paragraph(this.getTranslation(I18NKeys.ADD_GROUP_JOIN_DESC));

        this.tfJoinGroup.setLabel(this.getTranslation(I18NKeys.ADD_GROUP_JOIN_INVITE_CODE));
        this.tfJoinGroup.setWidthFull();
        this.tfJoinGroup.setMinLength(SharedConsts.MIN_LENGTH_JOIN_GROUP);
        this.tfJoinGroup.setPattern(SharedConsts.REGEX_GROUP_NAME);
        this.tfJoinGroup.setErrorMessage(String.join(
                SPACE,
                this.getTranslation(I18NKeys.MIN_LENGTH_1),
                String.valueOf(SharedConsts.MIN_LENGTH_JOIN_GROUP),
                this.getTranslation(I18NKeys.MIN_LENGTH_2))
        );
        this.tfJoinGroup.setValueChangeMode(ValueChangeMode.EAGER);
        this.tfJoinGroup.addValueChangeListener(e -> this.tfJoinGroupHandler(e.getValue()));
        this.tfJoinGroup.setId(TAB_JOIN_GROUP_TXT_GROUP_CODE);

        this.btnJoinGroup.addClassNames(SharedStyles.BUTTON_GREEN, SharedStyles.BUTTON_PARENT_CLASS);
        this.btnJoinGroup.addClickListener(e -> this.createMappingJoinGroup(this.tfJoinGroup.getValue()));
        this.btnJoinGroup.setEnabled(false);
        this.btnJoinGroup.setId(TAB_JOIN_GROUP_SUBMIT);

        div.add(description, this.tfJoinGroup, this.btnJoinGroup);

        return div;
    }

    private void createMappingJoinGroup(final String value) {
        final Group groupByInviteCode = this.groupService.findGroupByInviteCode(value);

        if (groupByInviteCode == null) {
            this.tfJoinGroup.setHelperText(this.getTranslation(I18NKeys.ADD_GROUP_JOIN_INVITE_CODE_INVALID));
            return;
        }

        final Mapping mapping = Mapping.builder()
                .uid(this.authenticatedUser.get().orElseThrow())
                .gid(groupByInviteCode)
                .isAdmin(false)
                .status(Status.REQUESTED)
                .build();

        this.mappingService.createMappingEntryIfNotExist(mapping);
        this.tfJoinGroup.clear();
        this.tfJoinGroup.setInvalid(false);
        this.btnJoinGroup.setEnabled(false);

        BSNotification.show(
                this.getTranslation(I18NKeys.ADD_GROUP_JOIN_SUCCESS),
                NotificationType.SUCCESS);
    }

    private void createMappingCreateGroup(final String groupName) {
        final Group group = Group.builder()
                .name(groupName)
                .owner(this.authenticatedUser.get().orElseThrow())
                .build();

        final Group saveAndReturnGroup = this.groupService.saveAndReturnGroup(group);

        final Mapping mapping = Mapping.builder()
                .uid(this.authenticatedUser.get().orElseThrow())
                .gid(saveAndReturnGroup)
                .isAdmin(false)
                .status(Status.CONFIRMED)
                .build();

        this.mappingService.createMappingEntryIfNotExist(mapping);

        ComponentUtil.fireEvent(UI.getCurrent(), new RefreshAppMenuEvent(this, false, saveAndReturnGroup.getId()));
    }

    private void tfCreateGroupHandler(final String value) {
        if (value.length() < SharedConsts.MIN_LENGTH_GROUP_NAME) {
            this.tfCreateGroup.setInvalid(true);
            this.btnCreateGroup.setEnabled(false);
            return;
        }
        this.tfCreateGroup.setInvalid(false);
        this.btnCreateGroup.setEnabled(true);
    }

    private void tfJoinGroupHandler(final String value) {
        if (value.length() < SharedConsts.MIN_LENGTH_JOIN_GROUP) {
            this.tfJoinGroup.setInvalid(true);
            this.btnJoinGroup.setEnabled(false);
            return;
        }
        this.tfJoinGroup.setInvalid(false);
        this.btnJoinGroup.setEnabled(true);
    }

    @Override
    public String getPageTitle() {
        return this.pageTitle;
    }
}
