package com.warpcoredev.billsplitter.views.group;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.dom.Style;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasDynamicTitle;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import com.warpcoredev.billsplitter.components.groupcomponents.BillForm;
import com.warpcoredev.billsplitter.components.groupcomponents.GroupJoinArea;
import com.warpcoredev.billsplitter.components.groupcomponents.GroupMenuArea;
import com.warpcoredev.billsplitter.components.groupcomponents.SinglePaymentArea;
import com.warpcoredev.billsplitter.components.groupcomponents.dialog.DlgNotification;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.BSNotification;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.NotificationType;
import com.warpcoredev.billsplitter.data.Role;
import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.entity.Mapping;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.data.service.BillService;
import com.warpcoredev.billsplitter.data.service.GroupService;
import com.warpcoredev.billsplitter.data.service.MappingService;
import com.warpcoredev.billsplitter.data.service.PaymentService;
import com.warpcoredev.billsplitter.data.service.PdfService;
import com.warpcoredev.billsplitter.data.service.SettingService;
import com.warpcoredev.billsplitter.data.service.UserService;
import com.warpcoredev.billsplitter.security.AuthenticatedUser;
import com.warpcoredev.billsplitter.utils.HelperClass;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedConsts;
import com.warpcoredev.billsplitter.utils.SharedStyles;
import com.warpcoredev.billsplitter.views.MainLayout;

import java.util.List;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.security.RolesAllowed;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import static com.warpcoredev.billsplitter.utils.SharedConsts.HEADER_GROUP_NAME;
import static com.warpcoredev.billsplitter.utils.SharedConsts.JS_SCROLL_TO_TOP;
import static com.warpcoredev.billsplitter.utils.SharedConsts.TOGGLE_BTN_EXPENSE;
import static com.warpcoredev.billsplitter.utils.SharedConsts.TOGGLE_BTN_TRANSACTION;
import static com.warpcoredev.billsplitter.utils.SharedConsts.URL_GROUPS2;

@Route (value = SharedConsts.URL_GROUPS + "/:groups?", layout = MainLayout.class)
@RolesAllowed ({Role.Fields.ADMIN, Role.Fields.USER, Role.Fields.GUEST, Role.Fields.FOR_PUBLIC_SHARE})
@Slf4j
@RequiredArgsConstructor
public class GroupMainView extends VerticalLayout implements HasDynamicTitle, HasUrlParameter<String> {

    // SERVICES -------------------------------------------------------------------------------------------------------
    private final AuthenticatedUser authenticatedUser;
    private final GroupService groupService;
    private final BillService billService;
    private final MappingService mappingService;
    private final UserService userService;
    private final PaymentService paymentService;
    private final SettingService settingService;
    private final PdfService pdfService;

    // ELEMENTS -------------------------------------------------------------------------------------------------------
    private final VerticalLayout verticalLayoutTop = new VerticalLayout();

    private final VerticalLayout verticalLayoutBillForm = new VerticalLayout();
    private final VerticalLayout verticalLayoutSinglePayment = new VerticalLayout();
    private final VerticalLayout verticalLayoutNoAccess = new VerticalLayout();

    private final Div awaitingGroupJoinDiv = new Div();
    private final Div rejectedGroupJoinDiv = new Div();

    private final H3 groupName = new H3();
    private final HorizontalLayout selectArea = new HorizontalLayout();

    private final Button btnExpense = this.createToggleButton(
            this.getTranslation(I18NKeys.BILL_TOGGLE_BTN_EXPENSE), true);
    private final Button btnTransaction = this.createToggleButton(
            this.getTranslation(I18NKeys.BILL_TOGGLE_BTN_TRANSACTION), false);

    private final HorizontalLayout hlBillButtons = new HorizontalLayout();
    private final BillForm billForm = new BillForm();
    private final SinglePaymentArea singlePaymentArea = new SinglePaymentArea();

    private Group selectedGroup;
    private GroupJoinArea groupJoinArea;
    private GroupMenuArea groupMenuArea;

    // OTHER ----------------------------------------------------------------------------------------------------------
    private String pageTitle;
    private User user;
    private List<Mapping> rejectedGroupJoinRequests;
    private List<Mapping> awaitingGroupJoinRequests;
    private boolean isGuestUser;

    @PostConstruct
    private void initUI() {
        this.user = this.authenticatedUser.get().orElseThrow();
        // ------------------------------------------------------------------------------------------------------------
        this.groupName.setId(HEADER_GROUP_NAME);
        this.groupName.addClassNames(SharedStyles.FULL_WIDTH);

        this.selectArea.add(this.groupName);
        this.isGuestUser = this.isGuestUser();

        if (this.checkNotifications()) {
            this.selectArea.add(this.getNotificationBtn());
        }

        this.selectArea.setWidthFull();
        this.selectArea.addClassName(SharedStyles.POSITION_RELATIV);
        this.selectArea.setAlignItems(Alignment.END);
        // ------------------------------------------------------------------------------------------------------------
        this.groupMenuArea = GroupMenuArea.builder()
                .billService(this.billService)
                .groupService(this.groupService)
                .paymentService(this.paymentService)
                .userService(this.userService)
                .mappingService(this.mappingService)
                .pdfService(this.pdfService)
                .selectedGroup(this.selectedGroup)
                .user(this.user)
                .build();
        this.verticalLayoutTop.setWidthFull();
        // ------------------------------------------------------------------------------------------------------------
        this.groupJoinArea = new GroupJoinArea(this.mappingService, this.groupService);
        this.groupJoinArea.setSelectedGroup(this.selectedGroup);

        this.verticalLayoutTop.add(
                this.selectArea,
                this.groupMenuArea.initUI(this.isGuestUser),
                this.groupJoinArea.initUI()
        );

        this.add(this.awaitingGroupJoinDiv);
        this.add(this.rejectedGroupJoinDiv);

        this.add(this.verticalLayoutTop);
        // ------------------------------------------------------------------------------------------------------------
        this.hlBillButtons.setSpacing(false);
        this.hlBillButtons.add(this.createToggleButtons());
        this.add(this.hlBillButtons);
        // ------------------------------------------------------------------------------------------------------------
        this.billForm.getSaveBtn().addClickListener(e -> {
            this.paymentService.savePayments(
                    this.billForm.getListOfPayments(),
                    this.user
            );
            BSNotification.show(this.getTranslation(I18NKeys.SAVED_SUCCESSFULLY), NotificationType.SUCCESS);
            this.clearFields();
            this.billForm.setValues(
                    this.groupService.getGroupMember(this.selectedGroup),
                    this.billService.getMostCommonDescriptions(
                            this.selectedGroup,
                            this.getTranslation(I18NKeys.BALANCING_PAYMENT)));
            this.getElement().executeJs(JS_SCROLL_TO_TOP);
        });

        this.verticalLayoutBillForm.add(this.billForm.initUI());
        this.verticalLayoutBillForm.getChildren().forEach(comp -> comp.addClassName(SharedStyles.FULL_WIDTH));
        this.verticalLayoutBillForm.setWidthFull();

        final Paragraph paragraphNoGroup = new Paragraph(this.getTranslation(I18NKeys.MESSAGE_GROUP_NOT_FOUND));
        paragraphNoGroup.setWidthFull();
        paragraphNoGroup.getStyle().setTextAlign(Style.TextAlign.CENTER);

        this.verticalLayoutNoAccess.add(paragraphNoGroup);

        this.singlePaymentArea.setBillService(this.billService);
        this.singlePaymentArea.setPaymentService(this.paymentService);
        this.verticalLayoutSinglePayment.add(this.singlePaymentArea);

        this.add(
                this.verticalLayoutBillForm,
                this.verticalLayoutSinglePayment,
                this.verticalLayoutNoAccess
        );

        this.setSpacing(false);
        this.setSizeFull();
        this.setJustifyContentMode(JustifyContentMode.CENTER);
        this.setDefaultHorizontalComponentAlignment(Alignment.STRETCH);
        this.addClassNames(SharedStyles.TEXT_ALIGN_CENTER, SharedStyles.CONTENT_MAX_WIDTH);
    }


    private Button getNotificationBtn() {
        final Button btnNotification = new Button(new Icon(VaadinIcon.INFO));
        btnNotification.addThemeVariants(ButtonVariant.LUMO_ICON);
        btnNotification.setId(SharedStyles.NOTIFICATION_BTN_ID);
        btnNotification.addClassNames(
                SharedStyles.NOTIFICATION_BTN,
                SharedStyles.POSITION_RELATIV,
                SharedStyles.BUTTON_BLUE,
                SharedStyles.BUTTON_PARENT_CLASS);
        btnNotification.addClickListener(e -> {
            final DlgNotification dlgNotification = new DlgNotification(
                    this.mappingService,
                    this.awaitingGroupJoinRequests,
                    this.rejectedGroupJoinRequests);
            dlgNotification.initUI();
        });
        return btnNotification;
    }


    private boolean checkNotifications() {
        if (this.isGuestUser()) {
            return false;
        }
        this.awaitingGroupJoinRequests = this.mappingService.getAwaitingGroupJoinRequests(this.user);
        this.rejectedGroupJoinRequests = this.mappingService.getRejectedGroupJoinRequests(this.user);

        return !this.awaitingGroupJoinRequests.isEmpty() || !this.rejectedGroupJoinRequests.isEmpty();
    }

    private boolean isGuestUser() {
        return switch (this.user.getRole()) {
            case USER, ADMIN -> false;
            default -> true;
        };
    }


    private void setValues(final Group group) {
        this.selectedGroup = group;

        this.settingService.setLastSelectedGroup(this.user, this.selectedGroup);
        this.groupName.setText(this.selectedGroup.getName());

        this.groupMenuArea.setGroup(this.selectedGroup);
        final boolean isGroupAdmin = this.selectedGroup.getOwner().equals(this.user);
        final boolean isAuthenticatedUser = this.user.getRole().equals(Role.USER)
                || this.user.getRole().equals(Role.ADMIN);

        final List<User> groupMembers = this.groupService.getGroupMember(this.selectedGroup);
        this.groupMenuArea.setUser(this.user);
        this.groupMenuArea.setSelectedGroup(this.selectedGroup);
        this.groupMenuArea.setGroupMembers(groupMembers);
        this.groupMenuArea.setGroupAdmin(isGroupAdmin);
        this.groupMenuArea.setAuthenticatedUser(isAuthenticatedUser);
        this.groupMenuArea.closeMenu();

        this.billForm.setUser(this.user);
        this.billForm.setGroup(this.selectedGroup);
        this.billForm.setValues(
                groupMembers,
                this.billService.getMostCommonDescriptions(
                        this.selectedGroup,
                        this.getTranslation(I18NKeys.BALANCING_PAYMENT)));

        this.groupJoinArea.setSelectedGroup(this.selectedGroup);
        this.groupJoinArea.setValues();
        if (!this.isGuestUser) {
            this.awaitingGroupJoinDiv.setVisible(!this.awaitingGroupJoinRequests.isEmpty());
            this.rejectedGroupJoinDiv.setVisible(!this.rejectedGroupJoinRequests.isEmpty());
        }

        this.toggleLayouts(
                this.verticalLayoutBillForm,
                this.verticalLayoutSinglePayment,
                this.btnExpense,
                this.btnTransaction);
        this.singlePaymentArea.setValues(this.selectedGroup, groupMembers);
    }


    private VerticalLayout createToggleButtons() {
        // Initial visibility settings
        this.verticalLayoutBillForm.setVisible(true);
        this.verticalLayoutSinglePayment.setVisible(false);

        this.btnExpense.setId(TOGGLE_BTN_EXPENSE);
        this.btnExpense.addClickListener(event ->
                this.toggleLayouts(
                        this.verticalLayoutBillForm,
                        this.verticalLayoutSinglePayment,
                        this.btnExpense,
                        this.btnTransaction));

        this.btnTransaction.setId(TOGGLE_BTN_TRANSACTION);
        this.btnTransaction.addClickListener(event -> this.toggleLayouts(
                this.verticalLayoutSinglePayment,
                this.verticalLayoutBillForm,
                this.btnTransaction,
                this.btnExpense));

        final HorizontalLayout buttonLayout = new HorizontalLayout(this.btnExpense, this.btnTransaction);
        buttonLayout.setWidthFull();
        buttonLayout.setFlexGrow(1, this.btnExpense, this.btnTransaction);
        buttonLayout.setSpacing(false);

        final VerticalLayout verticalLayout = new VerticalLayout(buttonLayout);
        verticalLayout.getStyle().setPaddingTop("0");
        verticalLayout.getStyle().setPaddingBottom("0");

        return verticalLayout;
    }

    private Button createToggleButton(final String label, final boolean isActive) {
        final Button button = new Button(label);
        button.setWidth("50%");
        button.addClassName(SharedStyles.GROUP_MAIN_TOGGLE_BTN_GRP);

        if (isActive) {
            button.addClassName(SharedStyles.BUTTON_GREEN);
        }

        return button;
    }

    private void toggleLayouts(final VerticalLayout toShow,
                               final VerticalLayout toHide,
                               final Button pressedButton,
                               final Button otherButton) {
        toShow.setVisible(true);
        toHide.setVisible(false);

        otherButton.removeClassName(SharedStyles.BUTTON_GREEN);
        pressedButton.addClassName(SharedStyles.BUTTON_GREEN);
    }


    private void clearFields() {
        this.billForm.clearFields();
    }


    @Override
    public String getPageTitle() {
        return this.pageTitle;
    }


    @Override
    public void setParameter(final BeforeEvent event, final String parameter) {
        this.pageTitle = HelperClass.getPageTitle(
                this.getTranslation(I18NKeys.PAGE_GROUPS), null);

        if (parameter != null && parameter.matches("\\d+")) {
            this.groupService.getGroupById(this.user, Long.parseLong(parameter))
                    .ifPresentOrElse(group -> {
                        this.verticalLayoutTop.setVisible(true);
                        this.verticalLayoutBillForm.setVisible(true);
                        this.verticalLayoutNoAccess.setVisible(false);
                        this.setValues(group);
                    }, () -> {
                        this.verticalLayoutTop.setVisible(false);
                        this.hlBillButtons.setVisible(false);
                        this.verticalLayoutBillForm.setVisible(false);
                        this.verticalLayoutSinglePayment.setVisible(false);
                        this.verticalLayoutNoAccess.setVisible(true);
                    });
        } else {
            this.selectedGroup = this.groupService.getLastGroup(this.user);
            event.forwardTo(URL_GROUPS2 + this.selectedGroup.getId());
        }
    }
}
