package com.warpcoredev.billsplitter.views.login;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.login.LoginI18n;
import com.vaadin.flow.component.login.LoginOverlay;
import com.vaadin.flow.router.AccessDeniedException;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasDynamicTitle;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.internal.RouteUtil;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.auth.AccessDeniedErrorRouter;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import com.warpcoredev.billsplitter.components.LoadingScreen;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.BSNotification;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.NotificationType;
import com.warpcoredev.billsplitter.data.Status;
import com.warpcoredev.billsplitter.data.dto.GuestUserDto;
import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.data.service.LoginAttemptService;
import com.warpcoredev.billsplitter.data.service.SettingService;
import com.warpcoredev.billsplitter.data.service.UserService;
import com.warpcoredev.billsplitter.security.AuthenticatedUser;
import com.warpcoredev.billsplitter.utils.FirstRunWizard;
import com.warpcoredev.billsplitter.utils.HelperClass;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedConsts;
import com.warpcoredev.billsplitter.utils.SharedStyles;
import com.warpcoredev.billsplitter.views.MainLayout;

import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import static com.warpcoredev.billsplitter.utils.SharedConsts.GET_PARAM_CODE;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GET_PARAM_EMAIL;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GET_PARAM_GROUP;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GET_PARAM_TOKEN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.JS_AUTOLOGIN_CLICK;
import static com.warpcoredev.billsplitter.utils.SharedConsts.JS_AUTOLOGIN_PASSWORD;
import static com.warpcoredev.billsplitter.utils.SharedConsts.JS_AUTOLOGIN_USERNAME;
import static com.warpcoredev.billsplitter.utils.SharedConsts.URL_GROUPS2;


@AnonymousAllowed
@Route(value = SharedConsts.URL_LOGIN, layout = MainLayout.class)
@Slf4j
@AccessDeniedErrorRouter
@RequiredArgsConstructor
public class LoginView extends LoginOverlay implements BeforeEnterObserver, HasDynamicTitle, HasUrlParameter<String> {

    // SERVICES -------------------------------------------------------------------------------------------------------
    private final AuthenticatedUser authenticatedUser;
    private final UserService userService;
    private final SettingService settingService;
    private final FirstRunWizard firstRunWizard;
    private final transient LoginAttemptService loginAttemptService;

    // ELEMENTS -------------------------------------------------------------------------------------------------------
    private final LoadingScreen loadingScreen = new LoadingScreen();

    // OTHER ----------------------------------------------------------------------------------------------------------
    private final transient HttpServletRequest request;
    private String pageTitle;

    @PostConstruct
    private void init() throws NoSuchAlgorithmException {
        this.firstRunWizard.checkFirstRun();

        this.setAction(RouteUtil.getRoutePath(VaadinService.getCurrent().getContext(), this.getClass()));

        final LoginI18n i18n = LoginI18n.createDefault();
        i18n.setHeader(new LoginI18n.Header());
        i18n.getHeader().setTitle(SharedConsts.TITLE_BILLSPLITTER);

        i18n.getForm().setTitle(SharedConsts.TITLE_BILLSPLITTER);
        i18n.getForm().setUsername(this.getTranslation(I18NKeys.EMAIL));
        i18n.getForm().setPassword(this.getTranslation(I18NKeys.LOGIN_PASSWORD));
        i18n.getForm().setSubmit(this.getTranslation(I18NKeys.LOGIN_BUTTON));
        i18n.getForm().setForgotPassword(this.getTranslation(I18NKeys.LOGIN_RESTORE_PASSWORD_OR_CREATE_ACCOUNT));

        // disable helper text for email and password textfield
        i18n.getErrorMessage().setUsername(null);
        i18n.getErrorMessage().setPassword(null);

        i18n.getErrorMessage().setTitle(this.getTranslation(I18NKeys.LOGIN_ERROR_MESSAGE_TITLE));
        i18n.getErrorMessage().setMessage(this.getTranslation(I18NKeys.LOGIN_ERROR_MESSAGE_DESC));

        i18n.setAdditionalInformation(null);

        this.setI18n(i18n);
        this.addClassName(SharedStyles.LAYOUT_LOGIN_FORM);

        this.addForgotPasswordListener(e -> UI.getCurrent().navigate(SharedConsts.URL_FORGOT_PASSWORD));

        final boolean isBlocked = this.loginAttemptService.isBlocked(this.request);
        if (isBlocked) {
            throw new AccessDeniedException();
        }
        this.getFooter().add(this.loadingScreen);
        this.getElement().setAttribute("no-autofocus", "");
        this.setOpened(true);
    }

    @Override
    public void beforeEnter(final BeforeEnterEvent event) {
        final Optional<User> user = this.authenticatedUser.get();

        final Map<String, List<String>> parameters = event.getLocation().getQueryParameters().getParameters();

        if (user.isPresent()) {
            final Group group = this.settingService.getLastSelectedGroup(user.get());
            // Already logged in
            this.setOpened(false);
            event.forwardTo(URL_GROUPS2 + group);
        }

        if (parameters.containsKey("q")) {
            final Optional<String> validationQuery = parameters.get("q").stream().findFirst();
            if (validationQuery.isPresent()) {
                this.getValidateValidationCode(validationQuery.get());
            } else {
                this.printNotification(true);
            }

        }

        // Check anchor parameter to handle auto-login
        UI.getCurrent().getPage().fetchCurrentURL(e -> {
            if (e.getRef() != null) {
                this.handleAutoLogin(e.getRef());
            }
        });


        this.setError(parameters.containsKey("error"));
    }

    private void getValidateValidationCode(final String validationQuery) {

        final String decodedString = new String(Base64.getDecoder().decode(validationQuery));

        final Map<String, String> collect = Arrays
                .stream(decodedString.split("&"))
                .map(s -> s.split("="))
                .collect(Collectors.toMap(e -> e[0], e -> e[1]));

        final String email = collect.get(GET_PARAM_EMAIL);
        final String code = collect.get(GET_PARAM_CODE);

        final Optional<User> optUser = this.userService.validateValidationCode(email, code);

        if (optUser.isPresent()) {
            final User user = optUser.get();
            user.setValidationCode(null);
            user.setValidationCodeValidUntil(null);
            user.setEnabled(Status.ENABLED);
            this.userService.saveEntity(user);

            this.printNotification(false);
        } else {
            this.printNotification(true);
        }
    }

    private void printNotification(final boolean isError) {
        if (isError) {
            BSNotification.show(
                    this.getTranslation(I18NKeys.REGISTER_INCOMPLETE),
                    NotificationType.ERROR);
        } else {
            BSNotification.show(
                    this.getTranslation(I18NKeys.REGISTER_COMPLETE),
                    NotificationType.SUCCESS);
            UI.getCurrent().navigate(LoginView.class);
        }
    }

    private void handleAutoLogin(final String anchor) {
        // e.g. Z3JvdXA9MTImdG9rZW49YWJj
        final String decodedString = new String(Base64.getDecoder().decode(anchor));

        // e.g. group=12&token=abc
        final Map<String, String> collect = HelperClass.extractParameters(decodedString);

        final String group = collect.get(GET_PARAM_GROUP);
        final String token = collect.get(GET_PARAM_TOKEN);

        if (group == null || token == null) {
            log.warn("Group or token is null.");
            return;
        }

        final Optional<GuestUserDto> optGuestUser = this.userService.existGuestAccount(group, token);
        if (optGuestUser.isEmpty()) {
            log.warn("Missing guest user. (group={})", group);
            BSNotification.show(
                    this.getTranslation(I18NKeys.ADD_GROUP_JOIN_INVITE_CODE_INVALID),
                    NotificationType.ERROR);
            return;
        }

        this.getElement().executeJs(JS_AUTOLOGIN_USERNAME, optGuestUser.get().user().getEmail());
        this.getElement().executeJs(JS_AUTOLOGIN_PASSWORD, optGuestUser.get().group().getCode());
        this.getElement().executeJs(JS_AUTOLOGIN_CLICK);
    }

    @Override
    public String getPageTitle() {
        return this.pageTitle;
    }

    @Override
    public void setParameter(final BeforeEvent event, @OptionalParameter final String parameter) {
        this.pageTitle = this.getTranslation(I18NKeys.PAGE_LOGIN);
    }
}
