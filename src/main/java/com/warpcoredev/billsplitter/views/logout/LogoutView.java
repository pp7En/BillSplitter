package com.warpcoredev.billsplitter.views.logout;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.HasDynamicTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import com.warpcoredev.billsplitter.security.AuthenticatedUser;
import com.warpcoredev.billsplitter.utils.SharedConsts;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;

@AnonymousAllowed
@Route(value = SharedConsts.URL_LOGOUT)
@RequiredArgsConstructor
public class LogoutView extends VerticalLayout implements HasDynamicTitle {

    // SERVICES -------------------------------------------------------------------------------------------------------
    private final AuthenticatedUser authenticatedUser;

    // OTHER ----------------------------------------------------------------------------------------------------------
    private String pageTitle;

    @PostConstruct
    private void init() {
        this.authenticatedUser.logout();
    }


    @Override
    public String getPageTitle() {
        return this.pageTitle;
    }

}
