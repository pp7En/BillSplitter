package com.warpcoredev.billsplitter.views.restore;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.TabSheet;
import com.vaadin.flow.component.tabs.TabSheetVariant;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationResult;
import com.vaadin.flow.data.binder.Validator;
import com.vaadin.flow.data.validator.EmailValidator;
import com.vaadin.flow.data.validator.StringLengthValidator;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasDynamicTitle;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import com.warpcoredev.billsplitter.components.groupcomponents.dialog.DlgMailSuccessful;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.BSNotification;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.NotificationType;
import com.warpcoredev.billsplitter.data.Role;
import com.warpcoredev.billsplitter.data.Status;
import com.warpcoredev.billsplitter.data.dto.UserRegistrationModel;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.data.service.MailService;
import com.warpcoredev.billsplitter.data.service.SettingService;
import com.warpcoredev.billsplitter.data.service.UserService;
import com.warpcoredev.billsplitter.utils.HelperClass;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedConsts;
import com.warpcoredev.billsplitter.utils.SharedStyles;

import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static com.warpcoredev.billsplitter.utils.SharedConsts.GET_PARAM_CODE;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GET_PARAM_EMAIL;
import static com.warpcoredev.billsplitter.utils.SharedConsts.MIN_LENGTH_PASSWORD;
import static com.warpcoredev.billsplitter.utils.SharedConsts.MIN_LENGTH_USERNAME;
import static com.warpcoredev.billsplitter.utils.SharedConsts.REGEX_EMAIL;
import static com.warpcoredev.billsplitter.utils.SharedConsts.REGISTER_BUTTON_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.REGISTER_EMAIL_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.REGISTER_NAME_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.REGISTER_PASSWORD1_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.REGISTER_PASSWORD2_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.REGISTER_TAB_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.RESTORE_BUTTON_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.RESTORE_EMAIL_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.RESTORE_NEW_PASSWORD_BUTTON_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.RESTORE_NEW_PASSWORD_PASSWORD1_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.RESTORE_NEW_PASSWORD_PASSWORD2_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.RESTORE_TAB_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.VALIDATION_CODE_VALID;
import static com.warpcoredev.billsplitter.utils.SharedStyles.BUTTON_GREEN;

@AnonymousAllowed
@Route (value = SharedConsts.URL_FORGOT_PASSWORD)
@Slf4j
@RequiredArgsConstructor
public class RestorePassword extends VerticalLayout implements BeforeEnterObserver, HasDynamicTitle,
        HasUrlParameter<Long> {

    private final UserService userService;
    private final MailService mailService;
    private final SettingService settingService;

    private final H1 header = new H1(this.getTranslation(I18NKeys.APP_TITLE));
    private final Paragraph infobox = new Paragraph(
            this.getTranslation(I18NKeys.REGISTER_PUBLIC_REGISTRATIONS_DISABLED));
    private final DlgMailSuccessful dlgMailSuccessful = new DlgMailSuccessful();
    private final TabSheet tabSheet = new TabSheet();
    private final Anchor backToLogin = new Anchor(
            SharedConsts.URL_LOGIN,
            this.getTranslation(I18NKeys.RESTORE_PASSWORD_ANCHOR));
    private final Binder<UserRegistrationModel> binder = new Binder<>(UserRegistrationModel.class);
    private final UserRegistrationModel userModel = new UserRegistrationModel();
    private final Button button = new Button(this.getTranslation(I18NKeys.RESTORE_PASSWORD_BUTTON));

    private String pageTitle;

    @PostConstruct
    private void initUI() {
        final VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addClassNames(
                SharedStyles.DEFAULT_MAX_WIDTH,
                SharedStyles.MARGIN_AUTO);

        this.header.addClassName(SharedStyles.LAYOUT_HEADER);
        verticalLayout.add(this.header);

        final VerticalLayout registerArea = this.createRegisterArea();
        registerArea.setWidthFull();
        registerArea.setId(SharedConsts.REGISTER_VL_ID);
        registerArea.addClassName(SharedStyles.DEFAULT_MAX_WIDTH);

        this.tabSheet.add(this.getTranslation(I18NKeys.REGISTER_HEADER),
                registerArea);

        final VerticalLayout restoreArea = this.createRestoreArea();
        restoreArea.setWidthFull();
        restoreArea.setId(SharedConsts.RESTORE_VL_ID);
        restoreArea.addClassName(SharedStyles.DEFAULT_MAX_WIDTH);

        this.tabSheet.add(this.getTranslation(I18NKeys.RESTORE_PASSWORD_HEADER),
                restoreArea);

        this.tabSheet.setWidthFull();
        this.tabSheet.addThemeVariants(TabSheetVariant.LUMO_TABS_SMALL);

        this.tabSheet.getTabAt(0).setId(REGISTER_TAB_ID);
        this.tabSheet.getTabAt(1).setId(RESTORE_TAB_ID);

        this.backToLogin.addClassNames(
                SharedStyles.MARGIN_AUTO,
                SharedStyles.LAYOUT_LINK);

        verticalLayout.add(this.tabSheet, this.backToLogin);
        verticalLayout.addClassName("register-and-restore-background-vertical-layout");

        this.add(verticalLayout);
        this.addClassName("register-and-restore-background");
    }

    private VerticalLayout createRegisterArea() {
        final VerticalLayout vl = new VerticalLayout();
        vl.setWidthFull();

        if (!this.settingService.publicRegistrationsEnabled()) {
            vl.add(this.infobox);
            return vl;
        }

        final TextField name = new TextField(this.getTranslation(I18NKeys.REGISTER_NAME));
        name.setId(REGISTER_NAME_ID);

        final TextField mail = new TextField(this.getTranslation(I18NKeys.REGISTER_MAIL));
        mail.setId(REGISTER_EMAIL_ID);

        final PasswordField passwordField1 = new PasswordField(this.getTranslation(I18NKeys.REGISTER_PASSWORD_FIRST));
        passwordField1.setId(REGISTER_PASSWORD1_ID);

        final PasswordField passwordField2 = new PasswordField(this.getTranslation(I18NKeys.REGISTER_PASSWORD_SECOND));
        passwordField2.setId(REGISTER_PASSWORD2_ID);

        final Button registerBtn = new Button(this.getTranslation(I18NKeys.REGISTER_BUTTON));
        registerBtn.setId(REGISTER_BUTTON_ID);

        name.setWidthFull();
        name.setRequired(true);

        mail.setWidthFull();
        mail.setRequired(true);
        mail.setPattern(REGEX_EMAIL);

        passwordField1.setWidthFull();
        passwordField1.setRequired(true);

        passwordField2.setWidthFull();
        passwordField2.setRequired(true);

        registerBtn.addClassNames(SharedStyles.BUTTON_GREEN, SharedStyles.BUTTON_PARENT_CLASS);
        registerBtn.addClickListener(event -> {
            if (this.binder.validate().isOk()) {
                this.createNewUser();
            } else {
                BSNotification.show(this.getTranslation(I18NKeys.REGISTER_ERROR), NotificationType.ERROR);
            }
        });

        this.binder.forField(name)
                .asRequired()
                .withValidator(new StringLengthValidator(this.getTranslation(I18NKeys.NAME_TO_SHORT),
                                                         MIN_LENGTH_USERNAME,
                                                         Integer.MAX_VALUE))
                .bind(UserRegistrationModel::getName, UserRegistrationModel::setName);

        this.binder.forField(mail)
                .asRequired()
                .withValidator(new EmailValidator(this.getTranslation(I18NKeys.MAIL_INVALID)))
                .bind(UserRegistrationModel::getEmail, UserRegistrationModel::setEmail);

        this.binder.forField(passwordField1)
                .asRequired()
                .withValidator(new StringLengthValidator(this.getTranslation(I18NKeys.REGISTER_PASSWORD_NOT_EQUALS),
                                                         MIN_LENGTH_PASSWORD,
                                                         Integer.MAX_VALUE))
                .bind(UserRegistrationModel::getPassword, UserRegistrationModel::setPassword);

        this.binder.forField(passwordField2)
                .asRequired()
                .withValidator(new StringLengthValidator(this.getTranslation(I18NKeys.REGISTER_PASSWORD_NOT_EQUALS),
                                                         MIN_LENGTH_PASSWORD,
                                                         Integer.MAX_VALUE))
                .withValidator((Validator<String>) (value1, context) -> {
                    if (value1.equals(passwordField1.getValue())) {
                        return ValidationResult.ok();
                    } else {
                        return ValidationResult.error(this.getTranslation(I18NKeys.SETTINGS_PASSWORD_DO_NOT_MATCH));
                    }
                })
                .bind(UserRegistrationModel::getPasswordConfirmation, UserRegistrationModel::setPasswordConfirmation);
        this.binder.setBean(this.userModel);

        vl.add(name,
               mail,
               passwordField1,
               passwordField2,
               registerBtn);

        return vl;
    }

    private VerticalLayout createRestoreArea() {
        final VerticalLayout vl = new VerticalLayout();

        final Paragraph description = new Paragraph(this.getTranslation(I18NKeys.RESTORE_PASSWORD_DESC));

        final TextField email = new TextField(this.getTranslation(I18NKeys.EMAIL));
        email.setErrorMessage(this.getTranslation(I18NKeys.MAIL_INVALID));
        email.setWidthFull();
        email.setId(RESTORE_EMAIL_ID);
        email.setValueChangeMode(ValueChangeMode.EAGER);
        email.setPattern(REGEX_EMAIL);
        email.addValueChangeListener(e ->
                this.button.setEnabled(e.getValue().matches(REGEX_EMAIL)));
        email.setRequired(true);

        this.button.setId(RESTORE_BUTTON_ID);
        this.button.addClassNames(BUTTON_GREEN);
        this.button.setEnabled(false);
        this.button.addClickListener(e -> {
            try {
                this.handlePasswordRestore(email.getValue());
            } catch (final NoSuchAlgorithmException ex) {
                BSNotification.show(ex.getMessage(), NotificationType.ERROR);
            }
        });

        vl.add(description, email, this.button);

        return vl;
    }

    private void handlePasswordRestore(final String email) throws NoSuchAlgorithmException {
        final Optional<User> optUser = this.userService.optUserRestorePassword(email);

        optUser.ifPresent(this.mailService::sendPasswordResetMail);

        BSNotification.show(this.getTranslation(I18NKeys.DONE), NotificationType.SUCCESS);
    }

    private void createNewUser() {
        if (this.userService.emailAlreadyInUse(this.userModel.getEmail())) {
            BSNotification.show(
                    this.getTranslation(I18NKeys.MAIL_ALREADY_IN_USE),
                    NotificationType.ERROR);
            return;
        }

        final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        final String hashedPassword = bCryptPasswordEncoder.encode(this.userModel.getPassword());

        final User user = User.builder()
                .name(this.userModel.getName())
                .email(this.userModel.getEmail())
                .hashedPassword(hashedPassword)
                .enabled(Status.DISABLED)
                .validationCode(UUID.randomUUID().toString())
                .validationCodeValidUntil(LocalDateTime.now().plusDays(VALIDATION_CODE_VALID))
                .lastLogin(HelperClass.getUnixtime())
                .notifications(Status.ENABLED)
                .role(Role.USER)
                .build();

        this.userService.saveEntity(user);
        this.sendMail(user);

        this.dlgMailSuccessful.initUI();
    }

    @Override
    public void beforeEnter(final BeforeEnterEvent event) {
        final Map<String, List<String>> parameters = event.getLocation().getQueryParameters().getParameters();

        if (parameters.containsKey("q")) {
            final Optional<String> validationQuery = parameters.get("q").stream().findFirst();
            validationQuery.ifPresent(this::validate);
        }
    }

    private void validate(final String queryParameter) {
        final String decodedString = new String(Base64.getDecoder().decode(queryParameter));

        final Map<String, String> collect = Arrays
                .stream(decodedString.split("&"))
                .map(s -> s.split("="))
                .collect(Collectors.toMap(e -> e[0], e -> e[1]));

        final String email = collect.get(GET_PARAM_EMAIL);
        final String code = collect.get(GET_PARAM_CODE);

        final Optional<User> optUser = this.userService.validateValidationCode(email, code);
        optUser.ifPresent(user -> {
            final LocalDateTime now = LocalDateTime.now();
            final LocalDateTime timestampDateTime = optUser.get().getValidationCodeValidUntil();

            if (timestampDateTime.isAfter(now)) {
                optUser.get().setValidationCodeValidUntil(null);
                optUser.get().setValidationCode(null);

                final User savedUser = this.userService.saveEntity(optUser.get());
                this.openDialog(savedUser);
            } else {
                BSNotification.show(
                        this.getTranslation(I18NKeys.RESTORE_PASSWORD_CODE_INVALID),
                        NotificationType.ERROR);
            }

        });
    }

    private void openDialog(final User user) {
        final Dialog dlg = new Dialog();
        final PasswordField newPassword1 = new PasswordField(
                this.getTranslation(I18NKeys.SETTINGS_PASSWORD_NEW));
        final PasswordField newPassword2 = new PasswordField(
                this.getTranslation(I18NKeys.SETTINGS_PASSWORD_NEW_REPEAT));
        final Button btnApply = new Button(this.getTranslation(I18NKeys.BUTTON_APPLY));
        final Button closeBtn = new Button(new Icon("lumo", "cross"), e -> dlg.close());

        newPassword1.setId(RESTORE_NEW_PASSWORD_PASSWORD1_ID);
        newPassword1.setWidthFull();

        newPassword2.setId(RESTORE_NEW_PASSWORD_PASSWORD2_ID);
        newPassword2.setWidthFull();

        dlg.add(newPassword1, newPassword2);

        btnApply.setId(RESTORE_NEW_PASSWORD_BUTTON_ID);
        btnApply.addClickListener(e -> this.checkPassword(
                user,
                newPassword1.getValue(),
                newPassword2.getValue()));

        dlg.setHeaderTitle(this.getTranslation(I18NKeys.SETTINGS_CHANGE_PASSWORD));
        closeBtn.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        dlg.getHeader().add(closeBtn);

        dlg.getFooter().add(btnApply);
        dlg.setWidthFull();
        dlg.addClassName(SharedStyles.DIALOG_MAX_WIDTH);
        dlg.open();
    }

    private void checkPassword(final User user, final String newPassword1, final String newPassword2) {
        if (!newPassword1.equals(newPassword2)) {
            BSNotification.show(
                    this.getTranslation(I18NKeys.SETTINGS_PASSWORD_DO_NOT_MATCH),
                    NotificationType.ERROR);
            return;
        }

        this.userService.setNewPassword(user, newPassword1);
        BSNotification.show(this.getTranslation(I18NKeys.DONE), NotificationType.SUCCESS);
        UI.getCurrent().navigate(SharedConsts.URL_LOGIN);
    }

    private void sendMail(final User user) {
        this.mailService.sendRegistrationMail(user);
    }


    @Override
    public String getPageTitle() {
        return this.pageTitle;
    }

    @Override
    public void setParameter(final BeforeEvent event, @OptionalParameter final Long parameter) {
        this.pageTitle = SharedConsts.TITLE_BILLSPLITTER;
    }
}
