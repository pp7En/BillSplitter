package com.warpcoredev.billsplitter.views.settings;

import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasDynamicTitle;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.Route;
import com.warpcoredev.billsplitter.components.groupcomponents.dialog.DlgChangePassword;
import com.warpcoredev.billsplitter.components.groupcomponents.dialog.DlgDeleteAccount;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.BSNotification;
import com.warpcoredev.billsplitter.components.groupcomponents.notifications.NotificationType;
import com.warpcoredev.billsplitter.data.Role;
import com.warpcoredev.billsplitter.data.Status;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.data.service.PaymentService;
import com.warpcoredev.billsplitter.data.service.SettingService;
import com.warpcoredev.billsplitter.data.service.UserService;
import com.warpcoredev.billsplitter.security.AuthenticatedUser;
import com.warpcoredev.billsplitter.utils.HelperClass;
import com.warpcoredev.billsplitter.utils.I18NKeys;
import com.warpcoredev.billsplitter.utils.SharedConsts;
import com.warpcoredev.billsplitter.utils.SharedStyles;
import com.warpcoredev.billsplitter.views.MainLayout;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.security.RolesAllowed;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import static com.warpcoredev.billsplitter.utils.SharedConsts.REGEX_EMAIL;
import static com.warpcoredev.billsplitter.utils.SharedConsts.SETTING_APPLY_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.SETTING_CBX_NOTIFICATIONS;
import static com.warpcoredev.billsplitter.utils.SharedConsts.SETTING_CHANGE_PASSWORD_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.SETTING_DELETE_ACCOUNT_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.SETTING_HEADER;
import static com.warpcoredev.billsplitter.utils.SharedConsts.SETTING_TXT_EMAIL_ADDRESS;
import static com.warpcoredev.billsplitter.utils.SharedConsts.SETTING_TXT_USERNAME;
import static com.warpcoredev.billsplitter.utils.SharedStyles.SETTINGS_INFO_DIV;

@Route (value = SharedConsts.URL_SETTINGS, layout = MainLayout.class)
@RolesAllowed ({Role.Fields.ADMIN, Role.Fields.USER})
@Slf4j
@RequiredArgsConstructor
public class SettingsView extends VerticalLayout implements HasDynamicTitle, HasUrlParameter<Long> {

    // SERVICES -------------------------------------------------------------------------------------------------------
    private final AuthenticatedUser authenticatedUser;
    private final UserService userService;
    private final SettingService settingService;
    private final PaymentService paymentService;

    // ELEMENTS -------------------------------------------------------------------------------------------------------
    private final H3 header = new H3(this.getTranslation(I18NKeys.SETTINGS_HEADER));
    private final Button btnApply = new Button(this.getTranslation(I18NKeys.BUTTON_APPLY));
    private final Button btnChangePassword = new Button(this.getTranslation(I18NKeys.SETTINGS_CHANGE_PASSWORD));
    private final Button btnDeleteAccount = new Button(this.getTranslation(I18NKeys.SETTINGS_DELETE_ACCOUNT));
    private final H5 notificationHeader = new H5(this.getTranslation(I18NKeys.SETTINGS_NOTIFICATIONS));
    private final Checkbox cbxNotifications = new Checkbox(this.getTranslation(I18NKeys.SETTINGS_NOTIFICATIONS_DESC));
    private final TextField txtUsername = new TextField(this.getTranslation(I18NKeys.SETTINGS_PROFILE_USERNAME));
    private final TextField txtMailAddress = new TextField(this.getTranslation(I18NKeys.EMAIL));
    private final Checkbox cbxPublicRegistration = new Checkbox(
            this.getTranslation(I18NKeys.SETTINGS_PUBLIC_REGISTRATIONS));

    // OTHER ----------------------------------------------------------------------------------------------------------
    private String pageTitle;
    private User user;

    @PostConstruct
    private void initUI() {
        this.user = this.authenticatedUser.get().orElseThrow();
        this.setupHeader();
        final Accordion accordion = this.createAccordion();
        this.add(accordion, this.createInfoDiv());
        this.setupLayoutProperties();
        this.setValues();
    }

    private void setupHeader() {
        this.header.setId(SETTING_HEADER);
        this.header.getStyle().setMarginTop("0");
        this.add(this.header);
    }

    private Accordion createAccordion() {
        final Accordion accordion = new Accordion();
        accordion.add(this.getTranslation(I18NKeys.SETTINGS_PROFILE), this.createProfileLayout());
        accordion.add(this.getTranslation(I18NKeys.SETTINGS_CHANGE_PASSWORD), this.createChangePasswordLayout());
        accordion.add(this.getTranslation(I18NKeys.SETTINGS_DELETE_ACCOUNT), this.createDeleteAccountLayout());
        this.fixAccordionPadding(accordion);
        return accordion;
    }

    private VerticalLayout createProfileLayout() {
        final VerticalLayout vlProfile = new VerticalLayout();
        if (this.user.getRole().equals(Role.ADMIN)) {
            vlProfile.add(this.createPublicRegistrationLayout());
        }
        vlProfile.add(this.createNotificationLayout());
        vlProfile.add(this.createNameEmailLayout());
        vlProfile.add(this.createApplyButton());
        return vlProfile;
    }

    private VerticalLayout createPublicRegistrationLayout() {
        final VerticalLayout vlPublicRegistration = new VerticalLayout();
        vlPublicRegistration.addClassNames(SharedStyles.NO_PADDING);
        final H5 publicRegistrationHeader = new H5(this.getTranslation(I18NKeys.SETTINGS_PUBLIC_REGISTRATIONS));
        publicRegistrationHeader.addClassNames(SharedStyles.NO_MARGIN);
        vlPublicRegistration.add(publicRegistrationHeader, this.cbxPublicRegistration);
        return vlPublicRegistration;
    }

    private VerticalLayout createNotificationLayout() {
        final VerticalLayout vlNotifications = new VerticalLayout();
        vlNotifications.addClassNames(SharedStyles.NO_PADDING);
        this.cbxNotifications.setId(SETTING_CBX_NOTIFICATIONS);
        if (!this.user.getRole().equals(Role.ADMIN)) {
            this.notificationHeader.addClassNames(SharedStyles.NO_MARGIN);
        }
        vlNotifications.add(this.notificationHeader, this.cbxNotifications);
        return vlNotifications;
    }

    private VerticalLayout createNameEmailLayout() {
        final VerticalLayout vlNameEmail = new VerticalLayout();
        vlNameEmail.addClassNames(SharedStyles.NO_PADDING);
        this.txtUsername.setWidthFull();
        this.txtUsername.setId(SETTING_TXT_USERNAME);
        this.txtMailAddress.setWidthFull();
        this.txtMailAddress.setId(SETTING_TXT_EMAIL_ADDRESS);
        this.txtMailAddress.setPattern(REGEX_EMAIL);
        vlNameEmail.add(this.txtUsername, this.txtMailAddress);
        return vlNameEmail;
    }

    private Button createApplyButton() {
        this.btnApply.setId(SETTING_APPLY_BTN);
        this.btnApply.addClassNames(SharedStyles.BUTTON_GREEN, SharedStyles.BUTTON_PARENT_CLASS);
        this.btnApply.addClickListener(e -> this.saveSettings());
        return this.btnApply;
    }

    private VerticalLayout createChangePasswordLayout() {
        final VerticalLayout vlChangePassword = new VerticalLayout();
        this.btnChangePassword.setId(SETTING_CHANGE_PASSWORD_BTN);
        final DlgChangePassword dlgChangePassword = new DlgChangePassword(this.userService, this.user);
        this.btnChangePassword.addClassNames(SharedStyles.BUTTON_GREEN, SharedStyles.BUTTON_PARENT_CLASS);
        this.btnChangePassword.addClickListener(e -> dlgChangePassword.initUI());
        vlChangePassword.add(this.btnChangePassword);
        return vlChangePassword;
    }

    private VerticalLayout createDeleteAccountLayout() {
        final VerticalLayout vlDeleteAccount = new VerticalLayout();
        this.btnDeleteAccount.setId(SETTING_DELETE_ACCOUNT_BTN);
        final DlgDeleteAccount dlgDeleteAccount = new DlgDeleteAccount(
                this.userService,
                this.user,
                this.paymentService);
        this.btnDeleteAccount.addClassNames(SharedStyles.BUTTON_PARENT_CLASS, SharedStyles.BUTTON_RED);
        this.btnDeleteAccount.addClickListener(e -> dlgDeleteAccount.initUI());
        vlDeleteAccount.add(this.btnDeleteAccount);
        return vlDeleteAccount;
    }

    private void fixAccordionPadding(final Accordion accordion) {
        accordion.getChildren().forEach(elem -> elem.getChildren().forEach(el -> {
            if (el instanceof Div) {
                el.addClassName(SharedStyles.PADDING_LEFT_FOR_ACCORDION);
            }
        }));
    }

    private Div createInfoDiv() {
        final Div infoDiv = new Div();
        infoDiv.addClassName(SETTINGS_INFO_DIV);
        final Image codeberg = new Image("images/codeberg-logo.png", "Codeberg");
        codeberg.addClickListener(e -> this.openNewTab("https://codeberg.org/pp7En/BillSplitter"));
        final Image buyMeCoffee = new Image("images/buy-me-a-coffee.png", "Buy me a coffee");
        buyMeCoffee.addClickListener(e -> this.openNewTab("https://www.buymeacoffee.com/warpcoredev"));
        infoDiv.add(codeberg, buyMeCoffee);
        return infoDiv;
    }

    private void setupLayoutProperties() {
        this.setSpacing(false);
        this.setSizeFull();
        this.setJustifyContentMode(JustifyContentMode.CENTER);
        this.setDefaultHorizontalComponentAlignment(Alignment.STRETCH);
        this.addClassNames(SharedStyles.TEXT_ALIGN_CENTER, SharedStyles.CONTENT_MAX_WIDTH);
    }


    public void setValues() {
        if (this.user.getRole().equals(Role.ADMIN)) {
            this.cbxPublicRegistration.setValue(this.settingService.publicRegistrationsEnabled());
        }

        this.cbxNotifications.setValue(this.user.getNotifications().equals(Status.ENABLED));

        this.txtUsername.setValue(this.user.getName());
        this.txtMailAddress.setValue(this.user.getEmail());
    }

    private void saveSettings() {
        // GLOBAL
        if (this.user.getRole().equals(Role.ADMIN)) {
            this.settingService.setPublicRegistrations(this.cbxPublicRegistration.getValue());
        }

        // USER
        final Boolean notifications = this.cbxNotifications.getValue();
        this.user.setNotifications(notifications ? Status.ENABLED : Status.DISABLED);

        if (this.emailAlreadyInUse()) {
            BSNotification.show(
                    this.getTranslation(I18NKeys.MAIL_ALREADY_IN_USE),
                    NotificationType.ERROR);
            return;
        }

        this.user.setName(this.txtUsername.getValue());
        this.user.setEmail(this.txtMailAddress.getValue());
        this.userService.saveEntity(this.user);

        BSNotification.show(
                this.getTranslation(I18NKeys.NOTIFICATIONS_CHANGES_SAVED),
                NotificationType.SUCCESS);
    }

    private boolean emailAlreadyInUse() {
        if (this.txtMailAddress.getValue().equals(this.user.getEmail())) {
            // No changes
            return false;
        }
        // check if email address already exist in database
        return this.userService.emailAlreadyInUse(this.txtMailAddress.getValue());
    }

    private void openNewTab(final String url) {
        this.getUI().ifPresent(ui -> ui.getPage().executeJs("window.open($0, '_blank');", url));
    }

    @Override
    public String getPageTitle() {
        return this.pageTitle;
    }

    @Override
    public void setParameter(final BeforeEvent event, @OptionalParameter final Long parameter) {
        this.pageTitle = HelperClass.getPageTitle(
                this.getTranslation(I18NKeys.PAGE_SETTINGS), parameter);
    }

}
