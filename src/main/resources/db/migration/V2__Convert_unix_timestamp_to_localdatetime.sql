-- 1. Backup the current column into a temporary column
ALTER TABLE users ADD COLUMN validation_code_valid_until_backup BIGINT;

UPDATE users
SET validation_code_valid_until_backup = validation_code_valid_until;

-- 2. Change the data type of the column
ALTER TABLE users
ALTER COLUMN validation_code_valid_until DROP DEFAULT;

ALTER TABLE users
ALTER COLUMN validation_code_valid_until TYPE TIMESTAMP USING TO_TIMESTAMP(validation_code_valid_until / 1000);

-- 3. Remove the temporary backup column
ALTER TABLE users DROP COLUMN validation_code_valid_until_backup;
