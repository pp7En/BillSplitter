ALTER TABLE bills ALTER COLUMN total_payable
SET DATA TYPE numeric(38,2);

ALTER TABLE payments ALTER COLUMN balance
SET DATA TYPE numeric(38,2);

ALTER TABLE payments ALTER COLUMN have_to_pay
SET DATA TYPE numeric(38,2);

ALTER TABLE payments ALTER COLUMN paid
SET DATA TYPE numeric(38,2);