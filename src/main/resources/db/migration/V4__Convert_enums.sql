-- mapping
ALTER TABLE mapping ADD COLUMN status_temp varchar(255);
UPDATE mapping
SET status_temp = CASE
    WHEN status = 0 THEN 'DISABLED'
    WHEN status = 1 THEN 'ENABLED'
    WHEN status = 2 THEN 'REQUESTED'
    WHEN status = 3 THEN 'CONFIRMED'
    WHEN status = 4 THEN 'REJECTED'
    ELSE NULL
END;
ALTER TABLE mapping DROP COLUMN status;
ALTER TABLE mapping RENAME COLUMN status_temp TO status;

-- payments
ALTER TABLE payments ADD COLUMN status_temp varchar(255);
UPDATE payments
SET status_temp = CASE
    WHEN status = 0 THEN 'NO_PAYMENT'
    WHEN status = 1 THEN 'REQUEST_SEND'
    WHEN status = 2 THEN 'REJECTED'
    WHEN status = 3 THEN 'CONFIRMED'
    ELSE NULL
END;
ALTER TABLE payments DROP COLUMN status;
ALTER TABLE payments RENAME COLUMN status_temp TO status;

-- users
ALTER TABLE users ADD COLUMN enabled_temp varchar(255);
UPDATE users
SET enabled_temp = CASE
    WHEN enabled = 0 THEN 'DISABLED'
    WHEN enabled = 1 THEN 'ENABLED'
    WHEN enabled = 2 THEN 'REQUESTED'
    WHEN enabled = 3 THEN 'CONFIRMED'
    WHEN enabled = 4 THEN 'REJECTED'
    ELSE NULL
END;
ALTER TABLE users DROP COLUMN enabled;
ALTER TABLE users RENAME COLUMN enabled_temp TO enabled;
--
ALTER TABLE users ADD COLUMN notifications_temp varchar(255);
UPDATE users
SET notifications_temp = CASE
    WHEN notifications = 0 THEN 'DISABLED'
    WHEN notifications = 1 THEN 'ENABLED'
    WHEN notifications = 2 THEN 'REQUESTED'
    WHEN notifications = 3 THEN 'CONFIRMED'
    WHEN notifications = 4 THEN 'REJECTED'
    ELSE NULL
END;
ALTER TABLE users DROP COLUMN notifications;
ALTER TABLE users RENAME COLUMN notifications_temp TO notifications;
--
ALTER TABLE users ADD COLUMN role_temp varchar(255);
UPDATE users
SET role_temp = CASE
    WHEN role = 1 THEN 'UNKNOWN'
    WHEN role = 2 THEN 'USER'
    WHEN role = 3 THEN 'GUEST'
    WHEN role = 4 THEN 'FOR_PUBLIC_SHARE'
    WHEN role = 5 THEN 'USER_WITHOUT_LOGIN'
    WHEN role = 99 THEN 'ADMIN'
    ELSE NULL
END;
ALTER TABLE users DROP COLUMN role;
ALTER TABLE users RENAME COLUMN role_temp TO role;