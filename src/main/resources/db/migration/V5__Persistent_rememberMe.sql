CREATE TABLE public.persistent_logins (
    username VARCHAR(255) NOT NULL,
    series VARCHAR(100) PRIMARY KEY,
    token VARCHAR(100) NOT NULL,
    last_used TIMESTAMP NOT NULL
);
