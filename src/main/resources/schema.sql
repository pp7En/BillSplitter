CREATE TABLE public.login_attempt (
    id bigserial NOT NULL,
    counter int4 NULL,
    datetime timestamp(6) NULL,
    "object" varchar(255) NULL,
    CONSTRAINT login_attempt_pkey PRIMARY KEY (id)
);

CREATE TABLE public.settings (
    id bigserial NOT NULL,
    "name" varchar(255) NOT NULL,
    value varchar(255) NOT NULL,
    CONSTRAINT settings_pkey PRIMARY KEY (id)
);

CREATE TABLE public.users (
    id bigserial NOT NULL,
    email varchar(255) NULL,
    enabled int2 NULL,
    pass_hash varchar(255) NULL,
    last_login int8 NULL,
    "name" varchar(255) NOT NULL,
    notifications int2 NULL,
    "role" int4 NOT NULL,
    validation_code varchar(255) NULL,
    validation_code_valid_until int8 NULL,
    CONSTRAINT users_enabled_check CHECK (((enabled >= 0) AND (enabled <= 4))),
    CONSTRAINT users_notifications_check CHECK (((notifications >= 0) AND (notifications <= 4))),
    CONSTRAINT users_pkey PRIMARY KEY (id)
);

CREATE TABLE public."groups" (
    id bigserial NOT NULL,
    code varchar(255) NULL,
    "name" varchar(255) NOT NULL,
    owner_id int8 NOT NULL,
    CONSTRAINT groups_pkey PRIMARY KEY (id),
    CONSTRAINT fkke9gpecgx7u1oef8lsd9tax3c FOREIGN KEY (owner_id) REFERENCES public.users(id)
);

CREATE TABLE public."mapping" (
    id bigserial NOT NULL,
    is_admin bool NOT NULL,
    status int2 NOT NULL,
    gid int8 NOT NULL,
    uid int8 NOT NULL,
    CONSTRAINT mapping_pkey PRIMARY KEY (id),
    CONSTRAINT mapping_status_check CHECK (((status >= 0) AND (status <= 4))),
    CONSTRAINT fk3ndbcnu2vpcwg9rv2aoy1lxcd FOREIGN KEY (gid) REFERENCES public."groups"(id),
    CONSTRAINT fkeyqr4b5mdv523imkqwsxpp3is FOREIGN KEY (uid) REFERENCES public.users(id)
);

CREATE TABLE public.bills (
    id bigserial NOT NULL,
    description varchar(255) NOT NULL,
    paid_date date NOT NULL,
    total_payable float8 NOT NULL,
    collector_id int8 NOT NULL,
    group_id int8 NOT NULL,
    CONSTRAINT bills_pkey PRIMARY KEY (id),
    CONSTRAINT fk2rc1cgs2sk4hioe0901bpkp46 FOREIGN KEY (collector_id) REFERENCES public.users(id),
    CONSTRAINT fkn7mvt7ci567ds40sx8a0ddy5y FOREIGN KEY (group_id) REFERENCES public."groups"(id)
);

CREATE TABLE public.payments (
    id bigserial NOT NULL,
    balance float8 NOT NULL,
    have_to_pay float8 NOT NULL,
    paid float8 NOT NULL,
    paid_day date NOT NULL,
    status int2 NOT NULL,
    bill_id int8 NULL,
    group_id int8 NOT NULL,
    user_id int8 NOT NULL,
    CONSTRAINT payments_pkey PRIMARY KEY (id),
    CONSTRAINT payments_status_check CHECK (((status >= 0) AND (status <= 3))),
    CONSTRAINT fk9565r6579khpdjxnyla0l2ycd FOREIGN KEY (bill_id) REFERENCES public.bills(id),
    CONSTRAINT fkj94hgy9v5fw1munb90tar2eje FOREIGN KEY (user_id) REFERENCES public.users(id),
    CONSTRAINT fkjeqnjw00iwcmqvpra9wjmicap FOREIGN KEY (group_id) REFERENCES public."groups"(id)
);