package com.warpcoredev.billsplitter;

import com.warpcoredev.billsplitter.components.groupcomponents.Calculator;
import com.warpcoredev.billsplitter.data.Role;
import com.warpcoredev.billsplitter.data.Status;
import com.warpcoredev.billsplitter.data.dto.HistoryDto;
import com.warpcoredev.billsplitter.data.dto.SettlementDto;
import com.warpcoredev.billsplitter.data.entity.Bill;
import com.warpcoredev.billsplitter.data.entity.Group;
import com.warpcoredev.billsplitter.data.entity.LoginAttempt;
import com.warpcoredev.billsplitter.data.entity.Mapping;
import com.warpcoredev.billsplitter.data.entity.Payment;
import com.warpcoredev.billsplitter.data.entity.User;
import com.warpcoredev.billsplitter.data.service.BillService;
import com.warpcoredev.billsplitter.data.service.GroupService;
import com.warpcoredev.billsplitter.data.service.LoginAttemptService;
import com.warpcoredev.billsplitter.data.service.MappingService;
import com.warpcoredev.billsplitter.data.service.PaymentService;
import com.warpcoredev.billsplitter.data.service.SettingService;
import com.warpcoredev.billsplitter.data.service.UserService;
import com.warpcoredev.billsplitter.utils.FirstRunWizard;
import com.warpcoredev.billsplitter.utils.HelperClass;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.BrowserWebDriverContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;
import org.testcontainers.utility.MountableFile;

import static com.warpcoredev.billsplitter.SharedConsts.AMOUNT_0_00;
import static com.warpcoredev.billsplitter.SharedConsts.AMOUNT_150_00;
import static com.warpcoredev.billsplitter.SharedConsts.AMOUNT_2_85;
import static com.warpcoredev.billsplitter.SharedConsts.AMOUNT_50_00;
import static com.warpcoredev.billsplitter.SharedConsts.AMOUNT_5_70;
import static com.warpcoredev.billsplitter.SharedConsts.AMOUNT_5_71;
import static com.warpcoredev.billsplitter.SharedConsts.DUMP_FILE_PATH_CONTAINER;
import static com.warpcoredev.billsplitter.SharedConsts.DUMP_FILE_PATH_HOST;
import static com.warpcoredev.billsplitter.SharedConsts.FIREFOX_DOCKER_IMAGE;
import static com.warpcoredev.billsplitter.SharedConsts.FIREFOX_OPTIONS;
import static com.warpcoredev.billsplitter.SharedConsts.GROUP_1;
import static com.warpcoredev.billsplitter.SharedConsts.GROUP_2;
import static com.warpcoredev.billsplitter.SharedConsts.GROUP_2_ID;
import static com.warpcoredev.billsplitter.SharedConsts.GROUP_3;
import static com.warpcoredev.billsplitter.SharedConsts.GROUP_CODE;
import static com.warpcoredev.billsplitter.SharedConsts.NETWORK;
import static com.warpcoredev.billsplitter.SharedConsts.PASSWORD_LONG;
import static com.warpcoredev.billsplitter.SharedConsts.PASSWORD_SHORT;
import static com.warpcoredev.billsplitter.SharedConsts.POSTGRES_DATABASE_NAME;
import static com.warpcoredev.billsplitter.SharedConsts.POSTGRES_DOCKER_IMAGE;
import static com.warpcoredev.billsplitter.SharedConsts.REGISTER_ELEMENTS_ID;
import static com.warpcoredev.billsplitter.SharedConsts.SLEEP;
import static com.warpcoredev.billsplitter.SharedConsts.SOURCE;
import static com.warpcoredev.billsplitter.SharedConsts.SQL_INSERT_DATA;
import static com.warpcoredev.billsplitter.SharedConsts.SQL_TRUNCATE_DATA;
import static com.warpcoredev.billsplitter.SharedConsts.TARGET;
import static com.warpcoredev.billsplitter.SharedConsts.TIMEOUT;
import static com.warpcoredev.billsplitter.SharedConsts.USER_1;
import static com.warpcoredev.billsplitter.SharedConsts.USER_1_EMAIL;
import static com.warpcoredev.billsplitter.SharedConsts.USER_1_NEW_EMAIL;
import static com.warpcoredev.billsplitter.SharedConsts.USER_1_NEW_NAME;
import static com.warpcoredev.billsplitter.SharedConsts.USER_2;
import static com.warpcoredev.billsplitter.SharedConsts.USER_2_EMAIL;
import static com.warpcoredev.billsplitter.SharedConsts.USER_3;
import static com.warpcoredev.billsplitter.SharedConsts.USER_3_EMAIL;
import static com.warpcoredev.billsplitter.SharedConsts.USER_4_EMAIL;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_CALCULATOR_RESULT_TEXTFIELD;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_CREATE_BILL_DESCRIPTION;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_CREATE_BILL_TOTAL;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_CREATE_GRP_NAME;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_CREATE_GUEST_USER;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_DLG_DELETE_ACCOUNT_PASSWORD;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_GROUP_OPTIONS_SHARE_GROUP_CBX;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_JOIN_GROUP_ACCEPT;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_JOIN_GROUP_REJECT;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_JOIN_GRP_CODE;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_JOIN_GRP_TAB;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_LOGIN_PASSWORD;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_LOGIN_SUBMIT_BTN;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_LOGIN_USER_NAME;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_NEW_GROUP_ADMIN;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_NEW_GROUP_ADMIN_FIRST_USER;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_REGISTER_EMAIL;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_REGISTER_NAME;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_REGISTER_PASSWORD_1;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_REGISTER_PASSWORD_2;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_RESTORE_EMAIL;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_RESTORE_NEW_PASSWORD1;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_RESTORE_NEW_PASSWORD2;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_SETTING_EMAIL_ADDRESS;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_SETTING_USERNAME;
import static com.warpcoredev.billsplitter.SharedConsts.XPATH_SINGLE_PAYMENT_TOTAL;
import static com.warpcoredev.billsplitter.utils.SharedConsts.AWAITING_GROUP_JOIN_REQUEST_VL;
import static com.warpcoredev.billsplitter.utils.SharedConsts.BILL_CALCULATOR_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.BILL_PAYMENT_GRID_CHECKBOX;
import static com.warpcoredev.billsplitter.utils.SharedConsts.BILL_SUBMIT_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.BUTTON_DOWNLOAD_PDF;
import static com.warpcoredev.billsplitter.utils.SharedConsts.DLG_DELETE_ACCOUNT_BTN_APPLY;
import static com.warpcoredev.billsplitter.utils.SharedConsts.FIRST_RUN_KEY;
import static com.warpcoredev.billsplitter.utils.SharedConsts.FIRST_RUN_VALUE_PREFIX;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GET_PARAM_GROUP;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GET_PARAM_TOKEN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_GUEST_USER_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_GUEST_USER_SUBMIT;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_HISTORY_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_LEAVE_GROUP_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_MEMBER_TABLE_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_OPEN_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_SETTLEMENT_TABLE_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_SHARE_GROUP_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_SHARE_GROUP_LINK;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_SHARE_GROUP_SUBMIT;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_STATISTIC_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.HEADER_GROUP_NAME;
import static com.warpcoredev.billsplitter.utils.SharedConsts.HISTORY_DIALOG_SEARCH_FIELD;
import static com.warpcoredev.billsplitter.utils.SharedConsts.PAYMENT_GRID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.REGEX_PUBLIC_LINK;
import static com.warpcoredev.billsplitter.utils.SharedConsts.REGISTER_BUTTON_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.REGISTER_TAB_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.REGISTER_VL_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.REJECTED_GROUP_JOIN_REQUEST_VL;
import static com.warpcoredev.billsplitter.utils.SharedConsts.RESTORE_BUTTON_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.RESTORE_NEW_PASSWORD_BUTTON_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.RESTORE_NEW_PASSWORD_PASSWORD1_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.RESTORE_TAB_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.SETTING_APPLY_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.SETTING_CBX_NOTIFICATIONS;
import static com.warpcoredev.billsplitter.utils.SharedConsts.SETTING_DELETE_ACCOUNT_BTN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.SETTING_HEADER;
import static com.warpcoredev.billsplitter.utils.SharedConsts.SINGLE_PAYMENT_FROM_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.SINGLE_PAYMENT_SAVE_BTN_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.SINGLE_PAYMENT_TO_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.STATISTIC_DIALOG_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.TAB_CREATE_GROUP;
import static com.warpcoredev.billsplitter.utils.SharedConsts.TAB_CREATE_GROUP_SUBMIT;
import static com.warpcoredev.billsplitter.utils.SharedConsts.TAB_JOIN_GROUP_SUBMIT;
import static com.warpcoredev.billsplitter.utils.SharedConsts.TOGGLE_BTN_TRANSACTION;
import static com.warpcoredev.billsplitter.utils.SharedConsts.URL_AUTOLOGIN;
import static com.warpcoredev.billsplitter.utils.SharedConsts.URL_FORGOT_PASSWORD;
import static com.warpcoredev.billsplitter.utils.SharedConsts.VIEW_LEAVE_GROUP_BTN;
import static com.warpcoredev.billsplitter.utils.SharedStyles.MEMBER_TABLE_LINE;
import static com.warpcoredev.billsplitter.utils.SharedStyles.NOTIFICATION_BTN_ID;
import static com.warpcoredev.billsplitter.utils.SharedStyles.SETTLEMENT_TABLE_LINE;
import static java.time.Duration.ofSeconds;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.openqa.selenium.support.ui.ExpectedConditions.titleIs;

@Testcontainers
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration (initializers = Initializer.class)
@Slf4j
public class BillSplitterTest {

    @Container
    @SuppressWarnings ("resource")
    public static PostgreSQLContainer<?> postgreContainer = new PostgreSQLContainer<>(
            DockerImageName.parse(POSTGRES_DOCKER_IMAGE))
            .withDatabaseName(POSTGRES_DATABASE_NAME)
            .withNetwork(NETWORK);

    @Container
    @SuppressWarnings ("resource")
    public static BrowserWebDriverContainer<?> firefoxContainer = new BrowserWebDriverContainer<>(FIREFOX_DOCKER_IMAGE)
            .withCapabilities(FIREFOX_OPTIONS)
            .withAccessToHost(true)
            .withNetwork(NETWORK);

    @Autowired
    private UserService userService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private MappingService mappingService;

    @Autowired
    private SettingService settingService;

    @Autowired
    private BillService billService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private LoginAttemptService loginAttemptService;

    @Autowired
    private FirstRunWizard firstRunWizard;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static WebDriver driver;

    private static final String[] PSQL_CMD_IMPORT_STRUCTURE = {"psql",
            "-U", postgreContainer.getUsername(),
            "-d", postgreContainer.getDatabaseName(),
            "-f", DUMP_FILE_PATH_CONTAINER};

    @DynamicPropertySource
    public static void setupProperties(final DynamicPropertyRegistry prop) {
        prop.add("spring.datasource.url", postgreContainer::getJdbcUrl);
        prop.add("spring.datasource.username", postgreContainer::getUsername);
        prop.add("spring.datasource.password", postgreContainer::getPassword);
        prop.add("spring.jpa.hibernate.ddl-auto", () -> "none");
        prop.add("service.envName", () -> "test");
        prop.add("spring.flyway.enabled", () -> false);
        prop.add("vaadin.pushMode", () -> "disabled");
    }

    @BeforeAll
    static void initBrowser() throws IOException, InterruptedException {
        final MountableFile file = MountableFile.forHostPath(DUMP_FILE_PATH_HOST);

        // Copy the database structure file from the host to the container
        postgreContainer.copyFileToContainer(file, DUMP_FILE_PATH_CONTAINER);
        final org.testcontainers.containers.Container.ExecResult result = postgreContainer.execInContainer(PSQL_CMD_IMPORT_STRUCTURE);
        if (result.getExitCode() == 0) {
            log.info("Database structure successfully imported.");
        } else {
            log.error("Error while importing the database structure. Exit-Code: {}", result.getExitCode());
            log.error("Error message: {}", result.getStderr());
        }

        // Disable firefox log
        System.setProperty("webdriver.firefox.silentOutput", "true");
        System.setProperty("webdriver.firefox.logfile", "/dev/null");

        driver = new RemoteWebDriver(
                firefoxContainer.getSeleniumAddress(),
                FIREFOX_OPTIONS);
        driver.manage().timeouts().implicitlyWait(ofSeconds(10));
        driver.manage().window().maximize();
    }

    @BeforeEach
    void setUp(final TestInfo testInfo) throws SQLException {
        log.info("Starting test: {}", testInfo.getDisplayName());
        final Connection connection = getDatabaseConnection();
        try (final Statement statement = connection.createStatement()) {
            statement.execute(SQL_TRUNCATE_DATA);
            statement.execute(SQL_INSERT_DATA);
            log.info("Database data successfully imported.");
        }
    }

    @AfterEach
    void tearDown(final TestInfo testInfo) {
        log.info("Finished test: {}", testInfo.getDisplayName());
        log.info("-".repeat(50));
    }

    @AfterAll
    static void close() {
        driver.close();
    }

    @Test
    @DisplayName ("check login: wrong password")
    void test002() {
        driver.get(Initializer.getBaseUrl());

        new WebDriverWait(driver, ofSeconds(TIMEOUT), ofSeconds(SLEEP))
                .until(titleIs("Login"));

        driver.findElement(By.xpath(XPATH_LOGIN_USER_NAME))
                .sendKeys(USER_1_EMAIL);

        driver.findElement(By.xpath(XPATH_LOGIN_PASSWORD))
                .sendKeys("wrong-password");

        driver.findElement(By.xpath(XPATH_LOGIN_SUBMIT_BTN)).click();

        assertDoesNotThrow(() -> new WebDriverWait(driver, ofSeconds(TIMEOUT), ofSeconds(SLEEP))
                .until(e -> Objects.requireNonNull(e.getCurrentUrl()).endsWith("login?error")));
    }

    @Test
    @DisplayName ("check login: correct password")
    void test003() {
        loginUser(USER_1_EMAIL);

        assertDoesNotThrow(() -> new WebDriverWait(driver, ofSeconds(TIMEOUT), ofSeconds(SLEEP))
                .until(e -> e.findElement(By.id(HEADER_GROUP_NAME)).isDisplayed()));
    }

    @Test
    @DisplayName("add group")
    void test004()  {
        loginUser(USER_1_EMAIL);
        driver.get(Initializer.getBaseUrl() + "add");

        webDriverWaitElementDisplayed(By.id(TAB_CREATE_GROUP));

        driver.findElement(By.xpath(XPATH_CREATE_GRP_NAME)).sendKeys(GROUP_2);
        driver.findElement(By.id(TAB_CREATE_GROUP_SUBMIT)).click();

        final User user = this.userService.findUserByMailAddress(USER_1_EMAIL);
        final List<Group> groups = this.groupService.getGroups(user);

        assertEquals(2, groups.size());
    }

    @Test
    @DisplayName("add bill (paid by all)")
    void test005() {
        loginUser(USER_1_EMAIL);

        final Group group = this.groupService.getGroups(this.userService.findUserByMailAddress(USER_1_EMAIL)).getFirst();
        driver.get(Initializer.getBaseUrl() + "groups/" + group.getId());

        final List<User> groupMember = this.groupService.getGroupMember(group);
        final User lastGroupMember = groupMember.getLast();
        lastGroupMember.setNotifications(Status.ENABLED);
        this.userService.saveEntity(lastGroupMember);

        webDriverWaitElementDisplayed(By.id(HEADER_GROUP_NAME));

        final List<WebElement> elements = driver.findElements(By.className(PAYMENT_GRID));

        final int countGroupMembers = groupMember.size();
        assertEquals(countGroupMembers, elements.size());

        this.createBill("Test Bill", AMOUNT_150_00, List.of());
        final List<SettlementDto> settlementData = this.groupService.getSettlementData(group);

        SettlementDto settlementDto;

        settlementDto = settlementData.get(0);
        assertEquals(USER_2, settlementDto.getFrom().getName());
        assertEquals(USER_1, settlementDto.getTo().getName());
        assertEquals(AMOUNT_50_00, settlementDto.getMoney());

        settlementDto = settlementData.get(1);
        assertEquals(USER_3, settlementDto.getFrom().getName());
        assertEquals(USER_1, settlementDto.getTo().getName());
        assertEquals(AMOUNT_50_00, settlementDto.getMoney());
    }

    @Test
    @DisplayName("add bill (not paid by all)")
    void test006() {
        final Group group = this.loginAndOpenFirstGroup();

        final List<WebElement> elements = driver.findElements(By.className(PAYMENT_GRID));

        final int countGroupMembers = this.groupService.getGroupMember(group).size();

        assertEquals(countGroupMembers, elements.size());

        this.createBill("Test Bill 1", AMOUNT_150_00, List.of());
        this.createBill("Test Bill 2", AMOUNT_150_00, List.of(1));

        final List<SettlementDto> listSettlementDto = this.groupService.getSettlementData(group);

        // The second bill affects only User1 and User3. User3 already has to pay 50 EUR,
        // so now 75 EUR are on top and total 125 EUR. User2 stays at 50 EUR as before.
        SettlementDto settlementDto;

        settlementDto = listSettlementDto.get(0);
        assertEquals(USER_2, settlementDto.getFrom().getName());
        assertEquals(USER_1, settlementDto.getTo().getName());
        assertEquals(50.0, settlementDto.getMoney().doubleValue());

        settlementDto = listSettlementDto.get(1);
        assertEquals(USER_3, settlementDto.getFrom().getName());
        assertEquals(USER_1, settlementDto.getTo().getName());
        assertEquals(125.0, settlementDto.getMoney().doubleValue());
    }

    @Test
    @DisplayName("create guests user")
    void test007() {
        final Group group = this.loginAndOpenFirstGroup();

        final int countGroupMembersBefore = this.groupService.getGroupMember(group).size();

        driver.findElement(By.id(GROUP_OPTIONS_OPEN_BTN)).click();
        driver.findElement(By.id(GROUP_OPTIONS_GUEST_USER_BTN)).click();
        driver.findElement(By.xpath(XPATH_CREATE_GUEST_USER)).sendKeys("guestuser1");
        driver.findElement(By.id(GROUP_OPTIONS_GUEST_USER_SUBMIT)).click();

        // Check database
        final int countGroupMembersAfter = this.groupService.getGroupMember(group).size();
        assertEquals(countGroupMembersBefore + 1, countGroupMembersAfter);

        // Check frontend
        webDriverWaitElementDisplayed(By.id(HEADER_GROUP_NAME));
        final int elements = driver.findElements(By.className(PAYMENT_GRID)).size();

        assertEquals(countGroupMembersAfter, elements);
    }

    @Test
    @DisplayName("settlement table")
    void test008() {
        final Group group = this.loginAndOpenFirstGroup();

        this.createBill("Test Bill 1", AMOUNT_150_00, List.of());
        this.createBill("Test Bill 2", AMOUNT_150_00, List.of(1));

        webDriverWaitElementClickable(By.id(GROUP_OPTIONS_OPEN_BTN));

        driver.findElement(By.id(GROUP_OPTIONS_OPEN_BTN)).click();
        driver.findElement(By.id(GROUP_OPTIONS_SETTLEMENT_TABLE_BTN)).click();

        final List<SettlementDto> settlementDataBefore = this.groupService.getSettlementData(group);

        driver.findElements(By.className(SETTLEMENT_TABLE_LINE)).getFirst()
                .findElement(By.tagName("vaadin-button"))
                .click();

        final List<SettlementDto> settlementDataAfter = this.groupService.getSettlementData(group);

        assertNotEquals(settlementDataBefore.size(), settlementDataAfter.size());

        final boolean paid = settlementDataBefore.stream()
                .filter(dto -> dto.getFrom().getName().equals(USER_2))
                .allMatch(dto -> settlementDataAfter.stream()
                        .noneMatch(afterDto -> afterDto.getFrom().getName().equals(USER_2)));
        assertTrue(paid);

        final boolean notPaid = settlementDataBefore.stream()
                .filter(dto -> dto.getFrom().getName().equals(USER_3))
                .allMatch(dto -> settlementDataAfter.stream()
                        .anyMatch(afterDto -> afterDto.getFrom().getName().equals(USER_3)));
        assertTrue(notPaid);
    }

    @Test
    @DisplayName("public link for group")
    void test009() {
        this.loginAndOpenFirstGroup();

        webDriverWaitElementClickable(By.id(GROUP_OPTIONS_OPEN_BTN));

        driver.findElement(By.id(GROUP_OPTIONS_OPEN_BTN)).click();
        driver.findElement(By.id(GROUP_OPTIONS_SHARE_GROUP_BTN)).click();

        driver.findElement(By.xpath(XPATH_GROUP_OPTIONS_SHARE_GROUP_CBX)).click();
        driver.findElement(By.id(GROUP_OPTIONS_SHARE_GROUP_SUBMIT)).click();

        webDriverWaitElementDisplayed(By.id(GROUP_OPTIONS_SHARE_GROUP_LINK));

        final String link = driver.findElement(By.id(GROUP_OPTIONS_SHARE_GROUP_LINK)).getText();
        assertTrue(link.matches(REGEX_PUBLIC_LINK));

        final String base64 = link.split("#")[1];
        final String decodedString = HelperClass.base64decode(base64);

        final Map<String, String> collect = HelperClass.extractParameters(decodedString);
        final long group = Long.parseLong(collect.get(GET_PARAM_GROUP));
        final String token = collect.get(GET_PARAM_TOKEN);

        assertEquals(token, this.groupService.getGroupById(group).orElseThrow().getCode());
    }

    @Test
    @DisplayName("group join requests")
    void test010() {
        loginUser(USER_4_EMAIL);
        final Group group = this.groupService.getAllGroups().getFirst();
        group.setCode(GROUP_CODE);
        this.groupService.saveEntity(group);

        driver.get(Initializer.getBaseUrl() + "add");

        webDriverWaitElementClickable(By.xpath(XPATH_JOIN_GRP_TAB));

        driver.findElement(By.xpath(XPATH_JOIN_GRP_TAB)).click();

        webDriverWaitElementDisplayed(By.xpath(XPATH_JOIN_GRP_TAB));

        driver.findElement(By.xpath(XPATH_JOIN_GRP_CODE)).sendKeys(GROUP_CODE);
        driver.findElement(By.id(TAB_JOIN_GROUP_SUBMIT)).click();

        final User user = this.userService.findUserByMailAddress(USER_4_EMAIL);
        final List<Mapping> awaitingGroupJoinRequests = this.mappingService.getAwaitingGroupJoinRequests(user);
        assertEquals(1, awaitingGroupJoinRequests.size());
    }

    @Test
    @DisplayName("accept group join request")
    void test011() {
        final Group group = this.groupService.getGroups(this.userService.findUserByMailAddress(USER_1_EMAIL)).getFirst();
        this.mappingService.saveEntity(Mapping.builder()
                .gid(group)
                .uid(this.userService.findUserByMailAddress(USER_4_EMAIL))
                .isAdmin(false)
                .status(Status.REQUESTED)
                .build());

        loginUser(USER_1_EMAIL);
        driver.get(Initializer.getBaseUrl() + "groups/" + group.getId());

        final int countGroupMembersBefore = this.groupService.getGroupMember(group).size();

        webDriverWaitElementClickable(By.xpath(XPATH_JOIN_GROUP_ACCEPT));

        driver.findElement(By.xpath(XPATH_JOIN_GROUP_ACCEPT)).click();

        final int countGroupMembersAfter = this.groupService.getGroupMember(group).size();

        assertEquals(
                countGroupMembersBefore + 1,
                countGroupMembersAfter);
    }

    @Test
    @DisplayName("reject group join request")
    void test012() {
        loginUser(USER_1_EMAIL);
        final Group group = this.groupService.getGroups(this.userService.findUserByMailAddress(USER_1_EMAIL)).getFirst();
        driver.get(Initializer.getBaseUrl() + "groups/" + group.getId());

        final Mapping mapping = Mapping.builder()
                .uid(this.userService.findUserByMailAddress(USER_3_EMAIL))
                .gid(group)
                .isAdmin(false)
                .status(Status.REQUESTED)
                .build();
        this.mappingService.saveEntity(mapping);

        final int countGroupMembersBefore = this.groupService.getGroupMember(group).size();

        webDriverWaitElementClickable(By.xpath(XPATH_JOIN_GROUP_REJECT));

        driver.findElement(By.xpath(XPATH_JOIN_GROUP_REJECT)).click();

        final int countGroupMembersAfter = this.groupService.getGroupMember(group).size();

        assertEquals(
                countGroupMembersBefore,
                countGroupMembersAfter);
    }

    @Test
    @DisplayName("leave group")
    void test013() {
        final Group group = this.groupService.findGroupsByOwner(USER_4_EMAIL).getFirst();
        final User user1 = this.userService.findUserByMailAddress(USER_1_EMAIL);
        group.setOwner(user1);
        this.groupService.saveEntity(group);
        final Mapping mapping = Mapping.builder()
                .uid(user1)
                .gid(group)
                .isAdmin(true)
                .status(Status.CONFIRMED)
                .build();
        this.mappingService.saveEntity(mapping);

        loginUser(USER_1_EMAIL);
        driver.get(Initializer.getBaseUrl() + "groups/" + group.getId());

        final User user = this.userService.findUserByMailAddress(USER_1_EMAIL);
        final int countGroupsBefore = this.groupService.getGroups(user).size();

        webDriverWaitElementClickable(By.id(GROUP_OPTIONS_OPEN_BTN));

        driver.findElement(By.id(GROUP_OPTIONS_OPEN_BTN)).click();
        driver.findElement(By.id(GROUP_OPTIONS_LEAVE_GROUP_BTN)).click();

        webDriverWaitElementDisplayed(By.id(GROUP_OPTIONS_OPEN_BTN));

        driver.findElement(By.xpath(XPATH_NEW_GROUP_ADMIN)).click();

        webDriverWaitElementDisplayed(By.xpath(XPATH_NEW_GROUP_ADMIN));

        driver.findElement(By.xpath(XPATH_NEW_GROUP_ADMIN_FIRST_USER)).click();

        driver.findElement(By.id(VIEW_LEAVE_GROUP_BTN)).click();

        loginUser(USER_1_EMAIL);
        driver.get(Initializer.getBaseUrl() + "groups/" + GROUP_2_ID);

        final int countGroupsAfter = this.groupService.getGroups(user).size();

        assertEquals(
                countGroupsBefore - 1,
                countGroupsAfter);
    }

    @Test
    @DisplayName("change notifications")
    void test014() {
        loginUser(USER_1_EMAIL);
        driver.get(Initializer.getBaseUrl() + "settings");

        webDriverWaitElementDisplayed(By.id(SETTING_HEADER));

        final Status status1 = this.userService.findUserByMailAddress(USER_1_EMAIL).getNotifications();
        assertEquals(Status.DISABLED, status1);

        driver.findElement(By.id(SETTING_CBX_NOTIFICATIONS)).click();
        driver.findElement(By.id(SETTING_APPLY_BTN)).click();

        final Status status2 = this.userService.findUserByMailAddress(USER_1_EMAIL).getNotifications();
        assertEquals(Status.ENABLED, status2);
    }

    @Test
    @DisplayName("change email")
    void test015() {
        loginUser(USER_1_EMAIL);
        driver.get(Initializer.getBaseUrl() + "settings");

        webDriverWaitElementDisplayed(By.id(SETTING_HEADER));

        final String email1 = this.userService.findUserByMailAddress(USER_1_EMAIL).getEmail();
        assertEquals(USER_1_EMAIL, email1);

        driver.findElement(By.xpath(XPATH_SETTING_EMAIL_ADDRESS)).clear();
        driver.findElement(By.xpath(XPATH_SETTING_EMAIL_ADDRESS)).sendKeys(USER_1_NEW_EMAIL);
        driver.findElement(By.id(SETTING_APPLY_BTN)).click();

        final String email2 = this.userService.findUserByMailAddress(USER_1_NEW_EMAIL).getEmail();
        assertEquals(USER_1_NEW_EMAIL, email2);
    }

    @Test
    @DisplayName("change name")
    void test016() {
        loginUser(USER_1_EMAIL);
        driver.get(Initializer.getBaseUrl() + "settings");

        webDriverWaitElementDisplayed(By.id(SETTING_HEADER));

        final String name1 = this.userService.findUserByMailAddress(USER_1_EMAIL).getName();
        assertEquals(USER_1, name1);

        driver.findElement(By.xpath(XPATH_SETTING_USERNAME)).clear();
        driver.findElement(By.xpath(XPATH_SETTING_USERNAME)).sendKeys(USER_1_NEW_NAME);
        driver.findElement(By.id(SETTING_APPLY_BTN)).click();

        final String name2 = this.userService.findUserByMailAddress(USER_1_EMAIL).getName();
        assertEquals(USER_1_NEW_NAME, name2);
    }

    @Test
    @DisplayName ("check brute force: first try")
    void test017() {
        this.loginAttemptService.cleanTable();

        loginUser("wrong-user-name");

        final List<LoginAttempt> loginAttempts = this.loginAttemptService.getAllLoginAttempts();
        assertEquals(1, loginAttempts.size());
        assertEquals(1, loginAttempts.getFirst().getCounter());
    }

    @Test
    @DisplayName ("check brute force: increase counter")
    void test018() {
        this.loginAttemptService.cleanTable();
        final int tryCounter = 5;

        for (int i = 0; i < tryCounter; i++) {
            loginUser("wrong-user-name");
            new WebDriverWait(driver, ofSeconds(TIMEOUT), ofSeconds(SLEEP)).until(titleIs("Login"));
        }

        final List<LoginAttempt> loginAttempts = this.loginAttemptService.getAllLoginAttempts();
        assertEquals(tryCounter, loginAttempts.getFirst().getCounter());
    }

    @Test
    @DisplayName ("check brute force: blocked")
    void test019() {
        this.loginAttemptService.cleanTable();
        final int tryCounter = 10;

        for (int i = 0; i < tryCounter; i++) {
            loginUser("wrong-user-name");
        }
        final List<LoginAttempt> loginAttempts = this.loginAttemptService.getAllLoginAttempts();

        assertEquals(tryCounter, loginAttempts.getFirst().getCounter());

        driver.manage().deleteAllCookies();
        driver.get(Initializer.getBaseUrl());

        assertThrows(
                NoSuchElementException.class,
                () -> driver.findElement(By.xpath(XPATH_LOGIN_USER_NAME)));
    }

    @Test
    @DisplayName ("delete account: account not balanced")
    void test020() {
        loginUser(USER_1_EMAIL);
        final Group group = this.groupService.getGroups(this.userService.findUserByMailAddress(USER_1_EMAIL)).getFirst();
        driver.get(Initializer.getBaseUrl() + "groups/" + group.getId());

        this.createBill("Test", AMOUNT_150_00, List.of());

        driver.get(Initializer.getBaseUrl() + "settings");

        webDriverWaitElementDisplayed(By.id(SETTING_HEADER));

        final WebElement deleteAccountAccordionTab = this.getDeleteAccountAccordionTab();
        deleteAccountAccordionTab.click();

        driver.findElement(By.id(SETTING_DELETE_ACCOUNT_BTN)).click();

        assertThrows(
                NoSuchElementException.class,
                () -> driver.findElement(By.id(DLG_DELETE_ACCOUNT_BTN_APPLY)));
    }

    @Test
    @DisplayName ("open history dialog")
    void test021() {
        this.loginAndOpenFirstGroup();

        driver.findElement(By.id(GROUP_OPTIONS_OPEN_BTN)).click();
        driver.findElement(By.id(GROUP_OPTIONS_HISTORY_BTN)).click();

        assertDoesNotThrow(() -> driver.findElement(By.id(HISTORY_DIALOG_SEARCH_FIELD)).isDisplayed());
    }

    @Test
    @DisplayName ("open statistic dialog")
    void test022() {
        this.loginAndOpenFirstGroup();

        driver.findElement(By.id(GROUP_OPTIONS_OPEN_BTN)).click();
        driver.findElement(By.id(GROUP_OPTIONS_STATISTIC_BTN)).click();

        assertDoesNotThrow(() -> driver.findElement(By.id(STATISTIC_DIALOG_ID)).isDisplayed());
    }

    @Test
    @DisplayName ("delete account: account balanced")
    void test023() {
        loginUser(USER_1_EMAIL);
        driver.get(Initializer.getBaseUrl() + "settings");

        webDriverWaitElementDisplayed(By.id(SETTING_HEADER));

        final WebElement deleteAccountAccordionTab = this.getDeleteAccountAccordionTab();
        deleteAccountAccordionTab.click();

        driver.findElement(By.id(SETTING_DELETE_ACCOUNT_BTN)).click();

        assertTrue(driver.findElement(By.id(DLG_DELETE_ACCOUNT_BTN_APPLY)).isDisplayed());

        driver.findElement(By.xpath(XPATH_DLG_DELETE_ACCOUNT_PASSWORD))
                .sendKeys(PASSWORD_SHORT);

        driver.findElement(By.id(DLG_DELETE_ACCOUNT_BTN_APPLY)).click();

        assertThrows(
                java.util.NoSuchElementException.class,
                () -> this.userService.findUserByMailAddress(USER_1_EMAIL)
        );
    }

    @Test
    @DisplayName("add bill and check rounding of the amounts (odd)")
    void test024() {
        final Group group = this.groupService.getGroups(this.userService.findUserByMailAddress(USER_1_EMAIL)).getFirst();
        loginUser(USER_1_EMAIL);

        driver.get(Initializer.getBaseUrl() + "groups/" + group.getId());

        webDriverWaitElementDisplayed(By.id(HEADER_GROUP_NAME));

        final List<WebElement> elements = driver.findElements(By.className(PAYMENT_GRID));

        final int countGroupMembers = this.groupService.getGroupMember(group).size();

        assertEquals(countGroupMembers, elements.size());

        final String billDescription = "Test Bill 3";
        this.createBill(billDescription, AMOUNT_5_71, List.of(1));

        final Bill bill = this.billService.getBillsByGroup(group)
                .stream()
                .filter(b -> billDescription.equals(b.getDescription()))
                .findFirst()
                .orElseThrow(() ->
                        new NoSuchElementException("No bill found with description '" + billDescription + "'"));

        assertEquals(AMOUNT_5_70, bill.getTotalPayable());
        bill.getPayments().forEach(payment -> assertEquals(AMOUNT_2_85, payment.getHaveToPay()));

        final Payment payment1 = this.findPaymentByUser(bill, USER_1);
        assertEquals(AMOUNT_2_85, payment1.getHaveToPay());
        assertEquals(AMOUNT_5_70, payment1.getPaid());
        assertEquals(AMOUNT_2_85, payment1.getBalance());

        final Payment payment2 = this.findPaymentByUser(bill, USER_3);
        assertEquals(AMOUNT_2_85, payment2.getHaveToPay());
        assertEquals(AMOUNT_0_00, payment2.getPaid());
        assertEquals(AMOUNT_2_85.negate(), payment2.getBalance());
    }

    @Test
    @DisplayName("add bill and check rounding of the amounts (even)")
    void test025() {
        final Group group = this.loginAndOpenFirstGroup();

        final List<WebElement> elements = driver.findElements(By.className(PAYMENT_GRID));

        final int countGroupMembers = this.groupService.getGroupMember(group).size();

        assertEquals(countGroupMembers, elements.size());

        final String billDescription = "Test Bill 4";
        this.createBill(billDescription, AMOUNT_5_71, List.of(0, 1));

        final Bill bill = this.billService.getBillsByGroup(group)
                .stream()
                .filter(b -> billDescription.equals(b.getDescription()))
                .findFirst()
                .orElseThrow(() ->
                        new NoSuchElementException("No bill found with description '" + billDescription + "'"));

        assertEquals(AMOUNT_5_71, bill.getTotalPayable());

        final Payment payment1 = this.findPaymentByUser(bill, USER_1);
        assertEquals(AMOUNT_0_00, payment1.getHaveToPay());
        assertEquals(AMOUNT_5_71, payment1.getPaid());
        assertEquals(AMOUNT_5_71, payment1.getBalance());

        final Payment payment2 = this.findPaymentByUser(bill, USER_3);
        assertEquals(AMOUNT_5_71, payment2.getHaveToPay());
        assertEquals(AMOUNT_0_00, payment2.getPaid());
        assertEquals(AMOUNT_5_71.negate(), payment2.getBalance());
    }

    @Test
    @DisplayName ("registration: successful")
    void test026() {
        final String name = "JohnDoe";
        final String email = "john-doe@example.com";

        boolean enabled = this.settingService.publicRegistrationsEnabled();
        assertTrue(enabled);

        driver.get(Initializer.getBaseUrl() + URL_FORGOT_PASSWORD);

        webDriverWaitElementDisplayed(By.id(REGISTER_TAB_ID));

        webDriverWaitElementDisplayed(By.id(REGISTER_VL_ID));

        for (final String elem : REGISTER_ELEMENTS_ID) {
            assertDoesNotThrow(() -> driver.findElement(By.id(elem)).isDisplayed());
        }

        driver.findElement(By.xpath(XPATH_REGISTER_NAME)).sendKeys(name);
        driver.findElement(By.xpath(XPATH_REGISTER_EMAIL)).sendKeys(email);
        driver.findElement(By.xpath(XPATH_REGISTER_PASSWORD_1)).sendKeys(PASSWORD_LONG);
        driver.findElement(By.xpath(XPATH_REGISTER_PASSWORD_2)).sendKeys(PASSWORD_LONG);
        driver.findElement(By.id(REGISTER_BUTTON_ID)).click();

        final User user = this.userService.findUserByMailAddress(email);
        assertNotNull(user);
        assertEquals(name, user.getName());
        assertFalse(user.getHashedPassword().isEmpty());
        assertFalse(user.getValidationCode().isEmpty());
        assertEquals(Status.DISABLED, user.getEnabled());
        assertEquals(Status.ENABLED, user.getNotifications());
        assertEquals(Role.USER, user.getRole());

        final LocalDateTime now = LocalDateTime.now();
        final LocalDateTime lowerBound = now.plus(Duration.ofHours(23));
        final LocalDateTime upperBound = now.plus(Duration.ofHours(25));
        assertTrue(user.getValidationCodeValidUntil().isAfter(lowerBound),
                "validUntil should be after 23 hours from now");
        assertTrue(user.getValidationCodeValidUntil().isBefore(upperBound),
                "validUntil should be before 25 hours from now");

        this.settingService.setPublicRegistrations(false);

        enabled = this.settingService.publicRegistrationsEnabled();
        assertFalse(enabled);
    }

    @Test
    @DisplayName ("registration: disabled")
    void test027() {
        boolean enabled = this.settingService.publicRegistrationsEnabled();
        assertTrue(enabled);

        this.settingService.setPublicRegistrations(false);

        enabled = this.settingService.publicRegistrationsEnabled();
        assertFalse(enabled);

        driver.get(Initializer.getBaseUrl() + URL_FORGOT_PASSWORD);

        webDriverWaitElementDisplayed(By.id(REGISTER_TAB_ID));
        webDriverWaitElementDisplayed(By.id(REGISTER_VL_ID));

        for (final String elem : REGISTER_ELEMENTS_ID) {
            assertThrows(
                    NoSuchElementException.class,
                    () -> driver.findElement(By.id(elem)));
        }
    }

    @Test
    @DisplayName ("restore password")
    void test028() {
        driver.get(Initializer.getBaseUrl() + URL_FORGOT_PASSWORD);

        webDriverWaitElementDisplayed(By.id(REGISTER_TAB_ID));

        final User user = this.userService.findUserByMailAddress(USER_1_EMAIL);
        final String passwordHashBefore = user.getHashedPassword();

        driver.findElement(By.id(RESTORE_TAB_ID)).click();
        driver.findElement(By.xpath(XPATH_RESTORE_EMAIL)).sendKeys(USER_1_EMAIL);

        new WebDriverWait(driver, ofSeconds(TIMEOUT), ofSeconds(SLEEP))
                .until(e -> e.findElement(By.id(RESTORE_BUTTON_ID)).isEnabled());

        driver.findElement(By.id(RESTORE_BUTTON_ID)).click();

        // Verify that password hash has not yet changed
        assertEquals(
                passwordHashBefore,
                this.userService.findUserByMailAddress(USER_1_EMAIL).getHashedPassword());

        final String base64encode = HelperClass.createEncodedRestorePasswordLink(this.userService.findUserByMailAddress(USER_1_EMAIL));
        final String fullRestoreUrl = Initializer.getBaseUrl() + URL_FORGOT_PASSWORD + "?q=" + base64encode;
        driver.manage().deleteAllCookies();
        driver.get(fullRestoreUrl);

        webDriverWaitElementDisplayed(By.id(RESTORE_NEW_PASSWORD_PASSWORD1_ID));

        final String newPassword = PASSWORD_SHORT + "-newPassword";
        driver.findElement(By.xpath(XPATH_RESTORE_NEW_PASSWORD1)).sendKeys(newPassword);
        driver.findElement(By.xpath(XPATH_RESTORE_NEW_PASSWORD2)).sendKeys(newPassword);
        driver.findElement(By.id(RESTORE_NEW_PASSWORD_BUTTON_ID)).click();

        // Verify that password hash has changed
        final User changedUser = this.userService.findUserByMailAddress(USER_1_EMAIL);
        assertNotEquals(
                passwordHashBefore,
                changedUser.getHashedPassword());

        // Verify that validation code and 'validUntil' is empty
        assertNull(changedUser.getValidationCode());
        assertNull(changedUser.getValidationCodeValidUntil());
    }

    @Test
    @DisplayName ("first run wizard")
    void test029() throws SQLException, NoSuchAlgorithmException {
        assertFalse(this.settingService.getValueByKey(FIRST_RUN_KEY).isEmpty());

        final Connection connection = getDatabaseConnection();
        try (final Statement statement = connection.createStatement()) {
            statement.execute(SQL_TRUNCATE_DATA);
            log.info("Database truncated");
        }

        assertTrue(this.settingService.getValueByKey(FIRST_RUN_KEY).isEmpty());

        this.firstRunWizard.checkFirstRun();

        final String value = this.settingService.getValueByKey(FIRST_RUN_KEY);
        assertFalse(value.isEmpty());
        assertTrue(value.startsWith(FIRST_RUN_VALUE_PREFIX));

        for (int i = 0; i < 50; i++) {
            assertEquals(
                    i,
                    this.firstRunWizard.generateRandomString(i).length());
        }

        final Set<String> generatedStrings = new HashSet<>();
        final int repetitions = 100;
        for (int i = 0; i < repetitions; i++) {
            final String randomString = this.firstRunWizard.generateRandomString(30);
            assertFalse(generatedStrings.contains(randomString), "Duplicate string found: " + randomString);
            generatedStrings.add(randomString);
        }
        assertEquals(repetitions, generatedStrings.size(), "Some generated strings were duplicates.");
    }

    @Test
    @DisplayName("group member table")
    void test030() {
        this.loginAndOpenFirstGroup();

        this.createBill("Test Bill 1", AMOUNT_150_00, List.of());
        this.createBill("Test Bill 2", AMOUNT_150_00, List.of(1));

        webDriverWaitElementClickable(By.id(GROUP_OPTIONS_OPEN_BTN));

        driver.findElement(By.id(GROUP_OPTIONS_OPEN_BTN)).click();
        driver.findElement(By.id(GROUP_OPTIONS_MEMBER_TABLE_BTN)).click();

        final List<WebElement> elements = driver.findElements(By.className(MEMBER_TABLE_LINE));
        assertEquals(3, elements.size());

        WebElement user = elements.get(0);
        List<WebElement> paragraphs = user.findElements(By.tagName("p"));
        assertEquals(USER_1, paragraphs.get(0).getText());
        assertEquals("175.00 €", paragraphs.get(1).getText());

        user = elements.get(1);
        paragraphs = user.findElements(By.tagName("p"));
        assertEquals(USER_2, paragraphs.get(0).getText());
        assertEquals("-50.00 €", paragraphs.get(1).getText());

        user = elements.get(2);
        paragraphs = user.findElements(By.tagName("p"));
        assertEquals(USER_3, paragraphs.get(0).getText());
        assertEquals("-125.00 €", paragraphs.get(1).getText());
    }

    @Test
    @DisplayName("single payment")
    void test031() {
        loginUser(USER_1_EMAIL);
        final Group group = this.groupService.getGroups(this.userService.findUserByMailAddress(USER_1_EMAIL)).getFirst();
        final List<SettlementDto> before = this.groupService.getSettlementData(group);
        final String money = "150";

        driver.get(Initializer.getBaseUrl() + "groups/" + group.getId());

        webDriverWaitElementDisplayed(By.id(HEADER_GROUP_NAME));

        driver.findElement(By.id(TOGGLE_BTN_TRANSACTION)).click();
        driver.findElement(By.xpath(XPATH_SINGLE_PAYMENT_TOTAL)).sendKeys(money);

        driver.findElement(By.id(SINGLE_PAYMENT_FROM_ID)).click();
        driver.findElement(By.xpath("//vaadin-select-overlay//vaadin-select-item[1]")).click();

        driver.findElement(By.id(SINGLE_PAYMENT_TO_ID)).click();
        driver.findElement(By.xpath("//vaadin-select-overlay//vaadin-select-item[2]")).click();

        driver.findElement(By.id(SINGLE_PAYMENT_SAVE_BTN_ID)).click();

        final List<SettlementDto> after = this.groupService.getSettlementData(group);

        assertEquals(0, before.size());

        assertEquals(1, after.size());
        assertEquals(USER_2_EMAIL, after.getFirst().getFrom().getEmail());
        assertEquals(USER_1_EMAIL, after.getFirst().getTo().getEmail());

        final BigDecimal expected = new BigDecimal(money);
        assertEquals(0, expected.compareTo(after.getFirst().getMoney()));
    }

    @Test
    @DisplayName("test services")
    void test032() {
        final Group group = this.loginAndOpenFirstGroup();

        this.createBill("Test Bill 1", AMOUNT_150_00, List.of());
        this.createBill("Test Bill 2", AMOUNT_150_00, List.of(1));

        final List<HistoryDto> historyDataBefore = this.groupService.getHistoryData(group, "Test", 0, 100);
        assertEquals(2, historyDataBefore.size());
        this.billService.deleteEntity(historyDataBefore);
        final List<HistoryDto> historyDataAfter = this.groupService.getHistoryData(group, "Test", 0, 100);
        assertEquals(0, historyDataAfter.size());
    }

    @Test
    @DisplayName("pdf service")
    void test033() throws InterruptedException {
        this.loginAndOpenFirstGroup();

        this.createBill("Test Bill 1", AMOUNT_150_00, List.of());
        this.createBill("Test Bill 2", AMOUNT_150_00, List.of(1));

        driver.findElement(By.id(GROUP_OPTIONS_OPEN_BTN)).click();
        driver.findElement(By.id(GROUP_OPTIONS_HISTORY_BTN)).click();

        driver.findElement(By.id(BUTTON_DOWNLOAD_PDF)).click();

        Thread.sleep(5_000L);

        firefoxContainer.copyFileFromContainer(SOURCE, TARGET);
        final File pdfFile = new File(TARGET);

        assertTrue(
                pdfFile.exists(),
                "No PDF file found in the directory.");

        final boolean validFileSize = pdfFile.length() > 1024;
        pdfFile.delete();

        assertTrue(
                validFileSize,
                "The PDF file found is smaller than 1 KB: " + pdfFile.getName());
    }

    @Test
    @DisplayName("logout")
    void test034() throws InterruptedException {
        this.loginAndOpenFirstGroup();

        driver.get(Initializer.getBaseUrl() + "logout");
        Thread.sleep(2_000L);

        assertDoesNotThrow(() -> new WebDriverWait(driver, ofSeconds(TIMEOUT), ofSeconds(SLEEP))
                .until(titleIs("Login")));
    }

    @Test
    @DisplayName("group join rejected")
    void test035() {
        final User user1 = this.userService.findUserByMailAddress(USER_1_EMAIL);
        final User user4 = this.userService.findUserByMailAddress(USER_4_EMAIL);

        final Group group1 = this.groupService.getGroups(user1)
                .stream()
                .filter(grp -> grp.getName().equals(GROUP_1))
                .findFirst()
                .orElseThrow();
        final Group group2 = this.groupService.getGroups(user4)
                .stream()
                .filter(grp -> grp.getName().equals(GROUP_2))
                .findFirst()
                .orElseThrow();

        // test first reject
        this.mappingService.saveEntity(Mapping.builder()
                .gid(group1)
                .uid(user4)
                .isAdmin(false)
                .status(Status.REJECTED)
                .build());

        loginUser(USER_4_EMAIL);
        driver.get(Initializer.getBaseUrl() + "groups/" + group2.getId());

        webDriverWaitElementDisplayed(By.id(HEADER_GROUP_NAME));

        webDriverWaitElementDisplayed(By.id(NOTIFICATION_BTN_ID));

        driver.findElement(By.id(NOTIFICATION_BTN_ID)).click();

        assertEquals(
                1,
                driver.findElements(By.className(REJECTED_GROUP_JOIN_REQUEST_VL)).size());

        // test second reject
        final Group group3 = this.groupService.saveEntity(Group.builder()
                .name(GROUP_3)
                .owner(user1)
                .build());
        this.mappingService.saveEntity(Mapping.builder()
                .gid(group3)
                .uid(user4)
                .isAdmin(false)
                .status(Status.REJECTED)
                .build());
        loginUser(USER_4_EMAIL);
        driver.get(Initializer.getBaseUrl() + "groups/" + group2.getId());

        webDriverWaitElementDisplayed(By.id(HEADER_GROUP_NAME));

        driver.findElement(By.id(NOTIFICATION_BTN_ID)).click();
        assertEquals(
                2,
                driver.findElements(By.className(REJECTED_GROUP_JOIN_REQUEST_VL)).size());
    }

    @Test
    @DisplayName("group join awaiting")
    void test036() {
        final User user1 = this.userService.findUserByMailAddress(USER_1_EMAIL);
        final User user4 = this.userService.findUserByMailAddress(USER_4_EMAIL);

        final Group group1 = this.groupService.getGroups(user1)
                .stream()
                .filter(grp -> grp.getName().equals(GROUP_1))
                .findFirst()
                .orElseThrow();
        final Group group2 = this.groupService.getGroups(user4)
                .stream()
                .filter(grp -> grp.getName().equals(GROUP_2))
                .findFirst()
                .orElseThrow();

        // test first awaiting request
        this.mappingService.saveEntity(Mapping.builder()
                .gid(group1)
                .uid(user4)
                .isAdmin(false)
                .status(Status.REQUESTED)
                .build());

        loginUser(USER_4_EMAIL);
        driver.get(Initializer.getBaseUrl() + "groups/" + group2.getId());

        webDriverWaitElementDisplayed(By.id(HEADER_GROUP_NAME));

        driver.findElement(By.id(NOTIFICATION_BTN_ID)).click();

        assertEquals(
                1,
                driver.findElements(By.className(AWAITING_GROUP_JOIN_REQUEST_VL)).size());

        // test second awaiting request
        final Group group3 = this.groupService.saveEntity(Group.builder()
                .name(GROUP_3)
                .owner(user1)
                .build());
        this.mappingService.saveEntity(Mapping.builder()
                .gid(group3)
                .uid(user4)
                .isAdmin(false)
                .status(Status.REQUESTED)
                .build());
        loginUser(USER_4_EMAIL);
        driver.get(Initializer.getBaseUrl() + "groups/" + group2.getId());

        webDriverWaitElementDisplayed(By.id(HEADER_GROUP_NAME));

        driver.findElement(By.id(NOTIFICATION_BTN_ID)).click();
        assertEquals(
                2,
                driver.findElements(By.className(AWAITING_GROUP_JOIN_REQUEST_VL)).size());
    }

    @Test
    @DisplayName("test calculator")
    void test037() {
        this.loginAndOpenFirstGroup();

        driver.findElement(
                By.id(BILL_CALCULATOR_BTN)).click();

        this.pressCalculatorKeys("1+3*3=");
        assertTrue(webDriverWaitElementGetTextfieldValue(By.xpath(XPATH_CALCULATOR_RESULT_TEXTFIELD), "10.00"));

        this.pressCalculatorKeys("3.1+4.2=");
        assertTrue(webDriverWaitElementGetTextfieldValue(By.xpath(XPATH_CALCULATOR_RESULT_TEXTFIELD), "7.30"));

        this.pressCalculatorKeys("8-5=");
        assertTrue(webDriverWaitElementGetTextfieldValue(By.xpath(XPATH_CALCULATOR_RESULT_TEXTFIELD), "3.00"));

        this.pressCalculatorKeys("6/3=");
        assertTrue(webDriverWaitElementGetTextfieldValue(By.xpath(XPATH_CALCULATOR_RESULT_TEXTFIELD), "2.00"));

        this.pressCalculatorKeys("1.1+0.......1=");
        assertTrue(webDriverWaitElementGetTextfieldValue(By.xpath(XPATH_CALCULATOR_RESULT_TEXTFIELD), "1.20"));

        this.pressCalculatorKeys("10--------5=");
        assertTrue(webDriverWaitElementGetTextfieldValue(By.xpath(XPATH_CALCULATOR_RESULT_TEXTFIELD), "5.00"));
    }

    @Test
    @DisplayName("guest login")
    void test038() throws NoSuchAlgorithmException {
        final Group group = this.groupService.getGroups(this.userService.findUserByMailAddress(USER_1_EMAIL)).getFirst();
        final String randomCode = HelperClass.getRandomCode(32);
        group.setCode(randomCode);
        this.groupService.saveEntity(group);

        final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        final String encode = bCryptPasswordEncoder.encode(randomCode);

        final User guestUser = this.userService.findGuestUser(group);
        guestUser.setEnabled(Status.CONFIRMED);
        guestUser.setHashedPassword(encode);
        this.userService.saveEntity(guestUser);

        this.mappingService.createMappingEntryIfNotExist(Mapping.builder()
                .uid(guestUser)
                .gid(group)
                .isAdmin(false)
                .status(Status.CONFIRMED)
                .build());

        final String encodedString = HelperClass.createEncodedLoginString(group.getId(), randomCode);
        final String url = Initializer.getBaseUrl() + URL_AUTOLOGIN + encodedString;

        driver.manage().deleteAllCookies();
        driver.get(url);

        assertDoesNotThrow(() -> webDriverWaitElementDisplayed(By.id(HEADER_GROUP_NAME)));
    }

    private static void webDriverWaitElementDisplayed(final By element) {
        new WebDriverWait(driver, ofSeconds(TIMEOUT), ofSeconds(SLEEP))
                .until(e -> e.findElement(element).isDisplayed());
    }

    private static void webDriverWaitElementClickable(final By element) {
        new WebDriverWait(driver, ofSeconds(TIMEOUT), ofSeconds(SLEEP))
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    private static boolean webDriverWaitElementGetTextfieldValue(final By element, final String value) {
        return new WebDriverWait(driver, ofSeconds(TIMEOUT), ofSeconds(SLEEP))
                .until(e -> Objects.equals(e.findElement(element).getDomProperty("value"), value));
    }

    private Group loginAndOpenFirstGroup() {
        loginUser(USER_1_EMAIL);
        final Group group = this.groupService.getGroups(this.userService.findUserByMailAddress(USER_1_EMAIL)).getFirst();
        driver.get(Initializer.getBaseUrl() + "groups/" + group.getId());

        webDriverWaitElementDisplayed(By.id(HEADER_GROUP_NAME));

        return group;
    }

    private void pressCalculatorKey(final String key) {
        driver.findElement(
                By.id(Calculator.generateButtonId(key))).click();
    }

    private void pressCalculatorKeys(final String input) {
        final String str = input
                .replace("*", Calculator.KEY_MULTIPLY)
                .replace("/", Calculator.KEY_DIVIDE)
                .replace("+", Calculator.KEY_ADD)
                .replace("-", Calculator.KEY_SUBTRACT)
                .replace("=", Calculator.KEY_EQUALS)
                .replace(".", Calculator.KEY_POINT)
                .replace(",", Calculator.KEY_COMMA);

        this.pressCalculatorKey(Calculator.KEY_CLEAR);
        for (final char key : str.toCharArray()) {
            this.pressCalculatorKey(String.valueOf(key));
        }
    }

    private static Connection getDatabaseConnection() throws SQLException {
        return DriverManager.getConnection(
                postgreContainer.getJdbcUrl(),
                postgreContainer.getUsername(),
                postgreContainer.getPassword()
        );
    }

    private static void loginUser(final String userEmailAddress) {
        driver.manage().deleteAllCookies();
        driver.get(Initializer.getBaseUrl());

        new WebDriverWait(driver, ofSeconds(TIMEOUT), ofSeconds(SLEEP)).until(titleIs("Login"));

        webDriverWaitElementClickable(By.xpath(XPATH_LOGIN_SUBMIT_BTN));

        driver.findElement(By.xpath(XPATH_LOGIN_USER_NAME)).sendKeys(userEmailAddress);
        driver.findElement(By.xpath(XPATH_LOGIN_PASSWORD)).sendKeys(PASSWORD_SHORT);
        driver.findElement(By.xpath(XPATH_LOGIN_SUBMIT_BTN)).click();
    }

    private void createBill(final String desc, final BigDecimal billTotal, final List<Integer> disableCheckboxId) {
        webDriverWaitElementDisplayed(By.xpath(XPATH_CREATE_BILL_DESCRIPTION));
        driver.findElement(By.xpath(XPATH_CREATE_BILL_DESCRIPTION)).sendKeys(desc);
        driver.findElement(By.xpath(XPATH_CREATE_BILL_TOTAL)).clear();
        driver.findElement(By.xpath(XPATH_CREATE_BILL_TOTAL)).sendKeys(String.valueOf(billTotal));
        if (!disableCheckboxId.isEmpty()) {
            disableCheckboxId.forEach(chx ->
                    driver.findElements(By.className(BILL_PAYMENT_GRID_CHECKBOX)).get(chx).click());
        }
        ((JavascriptExecutor) driver)
                .executeScript("window.scrollTo(0, document.body.scrollHeight)");
        webDriverWaitElementClickable(By.id(BILL_SUBMIT_BTN));
        driver.findElement(By.id(BILL_SUBMIT_BTN)).click();
    }

    private WebElement getDeleteAccountAccordionTab() {
        final WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        final WebElement accordion = wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("vaadin-accordion")));
        return accordion.findElements(By.tagName("vaadin-accordion-panel")).get(2);
    }

    private Payment findPaymentByUser(final Bill bill, final String userName) {
        return bill.getPayments().stream()
                .filter(payment -> payment.getUser().getName().equals(userName))
                .findFirst()
                .orElseThrow();
    }
}
