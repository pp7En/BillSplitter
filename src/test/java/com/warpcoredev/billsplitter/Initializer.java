package com.warpcoredev.billsplitter;

import lombok.Getter;

import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;

public class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Getter
    private static String baseUrl;

    @Override
    public void initialize(final ConfigurableApplicationContext applicationContext) {
        applicationContext.addApplicationListener(
                (ApplicationListener<WebServerInitializedEvent>) event -> {
                    org.testcontainers.Testcontainers.exposeHostPorts(event.getWebServer().getPort());
                    baseUrl = "http://host.testcontainers.internal:" + event.getWebServer().getPort() + "/";
                }
        );
    }
}
