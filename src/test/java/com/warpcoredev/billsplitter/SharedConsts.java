package com.warpcoredev.billsplitter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.List;

import org.openqa.selenium.firefox.FirefoxOptions;
import org.testcontainers.containers.Network;

import static com.warpcoredev.billsplitter.utils.SharedConsts.BILL_CALCULATOR_RESULT_TEXTFIELD;
import static com.warpcoredev.billsplitter.utils.SharedConsts.BILL_DESCRIPTION;
import static com.warpcoredev.billsplitter.utils.SharedConsts.BILL_TOTAL_PAYABLE;
import static com.warpcoredev.billsplitter.utils.SharedConsts.DATE_FORMATTER_HUMAN_READABLE;
import static com.warpcoredev.billsplitter.utils.SharedConsts.DLG_DELETE_ACCOUNT_PASSWORD_FIELD;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_JOIN_REQUEST_ELEMENT;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_GUEST_USER_TXT;
import static com.warpcoredev.billsplitter.utils.SharedConsts.GROUP_OPTIONS_SHARE_GROUP_CBX;
import static com.warpcoredev.billsplitter.utils.SharedConsts.REGISTER_BUTTON_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.REGISTER_EMAIL_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.REGISTER_NAME_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.REGISTER_PASSWORD1_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.REGISTER_PASSWORD2_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.RESTORE_EMAIL_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.RESTORE_NEW_PASSWORD_PASSWORD1_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.RESTORE_NEW_PASSWORD_PASSWORD2_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.SETTING_TXT_EMAIL_ADDRESS;
import static com.warpcoredev.billsplitter.utils.SharedConsts.SETTING_TXT_USERNAME;
import static com.warpcoredev.billsplitter.utils.SharedConsts.SINGLE_PAYMENT_TOTAL_ID;
import static com.warpcoredev.billsplitter.utils.SharedConsts.TABS_CREATE_AND_JOIN_GROUP;
import static com.warpcoredev.billsplitter.utils.SharedConsts.TAB_CREATE_GROUP_TXT_GROUP_NAME;
import static com.warpcoredev.billsplitter.utils.SharedConsts.TAB_JOIN_GROUP_TXT_GROUP_CODE;
import static com.warpcoredev.billsplitter.utils.SharedConsts.VIEW_LEAVE_GROUP_SELECT_NEW_ADMIN;

public interface SharedConsts {

    Long TIMEOUT = 30L;
    Long SLEEP = 3L;

    String EMAIL_DOMAIN = "@example.com";
    String PASSWORD_SHORT = "123456";
    String PASSWORD_LONG = "123456789";

    String USER_1 = "user1";
    String USER_1_NEW_NAME = USER_1 + "-new";
    String USER_1_EMAIL = USER_1 + EMAIL_DOMAIN;
    String USER_1_NEW_EMAIL = USER_1 + "-new" + EMAIL_DOMAIN;

    String USER_2 = "user2";
    String USER_2_EMAIL = USER_2 + EMAIL_DOMAIN;

    String USER_3 = "user3";
    String USER_3_EMAIL = USER_3 + EMAIL_DOMAIN;

    String USER_4 = "user4";
    String USER_4_EMAIL = USER_4 + EMAIL_DOMAIN;

    String GROUP_1 = "group1";
    String GROUP_2 = "group2";
    String GROUP_3 = "group3";
    long GROUP_2_ID = 2;

    String GROUP_CODE = "5ad3xqljbmi5h118iqouqu253udohapm";

    BigDecimal AMOUNT_5_71 = scaled(BigDecimal.valueOf(5.71));
    BigDecimal AMOUNT_5_70 = scaled(BigDecimal.valueOf(5.70));
    BigDecimal AMOUNT_2_85 = scaled(BigDecimal.valueOf(2.85));
    BigDecimal AMOUNT_0_00 = scaled(BigDecimal.valueOf(0.00));
    BigDecimal AMOUNT_50_00 = scaled(BigDecimal.valueOf(50.00));
    BigDecimal AMOUNT_150_00 = scaled(BigDecimal.valueOf(150.00));
    static BigDecimal scaled(final BigDecimal value) {
        return value.setScale(2, RoundingMode.UNNECESSARY);
    }

    String FILENAME = String.join("_",
            "Billing", GROUP_1, LocalDateTime.now().format(DATE_FORMATTER_HUMAN_READABLE)) + ".pdf";

    String SOURCE = "/home/seluser/Downloads/" + FILENAME;
    String TARGET = "/tmp/" + FILENAME;

    // Testcontainers
    Network NETWORK = Network.SHARED;
    FirefoxOptions FIREFOX_OPTIONS = new FirefoxOptions();
    String FIREFOX_DOCKER_IMAGE = "selenium/standalone-firefox:latest";
    String POSTGRES_DOCKER_IMAGE = "postgres:alpine";
    String POSTGRES_DATABASE_NAME = "postgres";
    String DUMP_FILE_PATH_CONTAINER = "/tmp/db_dump.sql";
    String DUMP_FILE_PATH_HOST = "./db_dump.sql";

    String SQL_TRUNCATE_DATA = """
            TRUNCATE TABLE
                public.bills,
                public.user_groups,
                public.login_attempt,
                public.mapping,
                public.payments,
                public.persistent_logins,
                public.settings,
                public.users
            CASCADE;
            """;
    String SQL_INSERT_DATA = """
            INSERT INTO public.users (email, enabled, pass_hash, last_login, name, notifications, role, validation_code, validation_code_valid_until) VALUES
            ('user1@example.com', 'ENABLED', '$2a$10$x0tKRgHriUGyKyDI12JZYOS.lr4qMteG0/IRdbD.oS0dTHx9mju2i', 0, 'user1', 'DISABLED', 'ADMIN', NULL, NULL),
            ('user2@example.com', 'ENABLED', '$2a$10$x0tKRgHriUGyKyDI12JZYOS.lr4qMteG0/IRdbD.oS0dTHx9mju2i', 0, 'user2', 'DISABLED', 'USER', NULL, NULL),
            ('user3@example.com', 'ENABLED', '$2a$10$x0tKRgHriUGyKyDI12JZYOS.lr4qMteG0/IRdbD.oS0dTHx9mju2i', 0, 'user3', 'DISABLED', 'USER', NULL, NULL),
            ('user4@example.com', 'ENABLED', '$2a$10$x0tKRgHriUGyKyDI12JZYOS.lr4qMteG0/IRdbD.oS0dTHx9mju2i', 0, 'user4', 'DISABLED', 'USER', NULL, NULL),
            ('uv5wg1a2@example.com', 'ENABLED', '$2a$10$Wd70syTXZtiA/MGVDLNEM.YVAnHkJQGH4qlnkbAKbRj05ceey9xLa', 0, 'Admin', 'DISABLED', 'ADMIN', NULL, NULL);
            
            INSERT INTO public.user_groups (code, name, owner_id) VALUES
            (NULL, 'group1', (SELECT id FROM public.users WHERE email = 'user1@example.com')),
            (NULL, 'group2', (SELECT id FROM public.users WHERE email = 'user4@example.com'));
            
            INSERT INTO public.mapping (is_admin, status, gid, uid) VALUES
            (TRUE, 'CONFIRMED',
                (SELECT id FROM public.user_groups WHERE name = 'group1'),
                (SELECT id FROM public.users WHERE email = 'user1@example.com')),
            (FALSE, 'CONFIRMED',
                (SELECT id FROM public.user_groups WHERE name = 'group1'),
                (SELECT id FROM public.users WHERE email = 'user2@example.com')),
            (FALSE, 'CONFIRMED',
                (SELECT id FROM public.user_groups WHERE name = 'group1'),
                (SELECT id FROM public.users WHERE email = 'user3@example.com')),
            (TRUE, 'CONFIRMED',
                (SELECT id FROM public.user_groups WHERE name = 'group2'),
                (SELECT id FROM public.users WHERE email = 'user4@example.com'));
            
            INSERT INTO public.settings (name, value) VALUES
            ('PUBLIC_REGISTRATION', '1'),
            ('FIRST_RUN', 'COMPLETE: 1725987729');
            """;

    // XPath and IDs
    String XPATH_LOGIN_USER_NAME = "//*[@id='vaadinLoginUsername']/input";
    String XPATH_LOGIN_PASSWORD = "//*[@id='vaadinLoginPassword']/input";
    String XPATH_LOGIN_SUBMIT_BTN = "/html/body/vaadin-login-overlay-wrapper/vaadin-login-form/vaadin-login-form-wrapper/vaadin-button[1]";
    String XPATH_CREATE_GRP_NAME = "//vaadin-text-field[@id='" + TAB_CREATE_GROUP_TXT_GROUP_NAME + "']//input[1]";
    String XPATH_CREATE_BILL_DESCRIPTION = "//vaadin-combo-box[@id='" + BILL_DESCRIPTION + "']//input[1]";
    String XPATH_CREATE_BILL_TOTAL = "//vaadin-big-decimal-field[@id='" + BILL_TOTAL_PAYABLE + "']//input[1]";
    String XPATH_CREATE_GUEST_USER = "//vaadin-text-field[@id='" + GROUP_OPTIONS_GUEST_USER_TXT + "']//input[1]";
    String XPATH_JOIN_GRP_TAB = "//vaadin-tabsheet[@id='" + TABS_CREATE_AND_JOIN_GROUP + "']//vaadin-tab[2]";
    String XPATH_JOIN_GRP_CODE = "//*[@id='" + TAB_JOIN_GROUP_TXT_GROUP_CODE + "']/input";
    String XPATH_JOIN_GROUP_ACCEPT = "//vaadin-horizontal-layout[@class='" + GROUP_JOIN_REQUEST_ELEMENT + "']//vaadin-vertical-layout[2]//vaadin-button[1]";
    String XPATH_JOIN_GROUP_REJECT = "//vaadin-horizontal-layout[@class='" + GROUP_JOIN_REQUEST_ELEMENT + "']//vaadin-vertical-layout[2]//vaadin-button[2]";
    String XPATH_NEW_GROUP_ADMIN = "//*[@id='" + VIEW_LEAVE_GROUP_SELECT_NEW_ADMIN + "']//vaadin-select-value-button";
    String XPATH_NEW_GROUP_ADMIN_FIRST_USER = "//vaadin-select-item[@value='1']";
    String XPATH_SETTING_USERNAME = "//vaadin-text-field[@id='" + SETTING_TXT_USERNAME + "']//input[1]";
    String XPATH_SETTING_EMAIL_ADDRESS = "//vaadin-text-field[@id='" + SETTING_TXT_EMAIL_ADDRESS + "']//input[1]";
    String XPATH_DLG_DELETE_ACCOUNT_PASSWORD = "//vaadin-password-field[@id='" + DLG_DELETE_ACCOUNT_PASSWORD_FIELD + "']//input[1]";
    String XPATH_GROUP_OPTIONS_SHARE_GROUP_CBX = "//*[@id='" + GROUP_OPTIONS_SHARE_GROUP_CBX + "']//input[@slot='input']";

    String XPATH_REGISTER_NAME = "//vaadin-text-field[@id='" + REGISTER_NAME_ID + "']//input[1]";
    String XPATH_REGISTER_EMAIL = "//vaadin-text-field[@id='" + REGISTER_EMAIL_ID + "']//input[1]";
    String XPATH_REGISTER_PASSWORD_1 = "//vaadin-password-field[@id='" + REGISTER_PASSWORD1_ID + "']//input[1]";
    String XPATH_REGISTER_PASSWORD_2 = "//vaadin-password-field[@id='" + REGISTER_PASSWORD2_ID + "']//input[1]";

    List<String> REGISTER_ELEMENTS_ID = List.of(
            REGISTER_NAME_ID,
            REGISTER_EMAIL_ID,
            REGISTER_PASSWORD1_ID,
            REGISTER_PASSWORD2_ID,
            REGISTER_BUTTON_ID
    );

    String XPATH_RESTORE_EMAIL = "//vaadin-text-field[@id='" + RESTORE_EMAIL_ID + "']//input[1]";
    String XPATH_RESTORE_NEW_PASSWORD1 = "//vaadin-password-field[@id='" + RESTORE_NEW_PASSWORD_PASSWORD1_ID + "']//input[1]";
    String XPATH_RESTORE_NEW_PASSWORD2 = "//vaadin-password-field[@id='" + RESTORE_NEW_PASSWORD_PASSWORD2_ID + "']//input[1]";

    String XPATH_SINGLE_PAYMENT_TOTAL = "//vaadin-number-field[@id='" + SINGLE_PAYMENT_TOTAL_ID + "']//input[1]";
    String XPATH_CALCULATOR_RESULT_TEXTFIELD = "//vaadin-text-field[@id='" + BILL_CALCULATOR_RESULT_TEXTFIELD + "']//input[1]";
}
